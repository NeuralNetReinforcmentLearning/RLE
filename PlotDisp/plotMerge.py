import matplotlib.pyplot as plt
import json
import sys
import os

merged = []
i=0
for file in os.listdir("./"):
    if i%100==0:
	    print("Read "+str(i)+" files")
    i=i+1
    with open(file) as raw_points:
        points = json.load(raw_points)
        merged.extend(points)
json.dump(merged, open("merged.json", "w+"))
print("Merged to merged.json")
