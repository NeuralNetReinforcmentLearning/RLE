import matplotlib.pyplot as plt
import json
import sys
import os

samples=7
values={}
for obj in os.listdir("./"):
	if os.path.isdir(obj):
		try:
			sample=int(obj[-2])
			if sample<samples:
				name=obj[:-2]
				file=obj+"/"+obj+".json"
				with open(file) as raw_points:
					points = json.load(raw_points)
					if name in values:
						for i in range(len(points)):
							values[name][i]+=points[i]
					else:
						values[name]=points
				print("Read "+obj)
		except ValueError:
			pass
for n in values:
	for i in range(len(values[n])):
		values[n][i]=values[n][i]/samples
	json.dump(values[n], open(n+".json", "w+"))
	print("Wrote "+n)