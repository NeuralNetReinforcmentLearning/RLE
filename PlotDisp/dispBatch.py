import matplotlib.pyplot as plt
import json
import sys
import os

plt.figure(1)

f=0
for file in os.listdir("./"):
    print(file)
    with open(file) as raw_points:
        points = json.load(raw_points)
        if(f==5):
            plt.plot(points, "r-", label="Agent #"+str(f))
        else:
            plt.plot(points, "b-", label="Agent #"+str(f))
    f+=1

plt.ylabel("Reward")
plt.xlabel("Step")

plt.show()
