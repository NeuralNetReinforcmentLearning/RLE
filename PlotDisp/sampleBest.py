import matplotlib.pyplot as plt
import json
import sys
import os

samples=9
values={}
for obj in os.listdir("./"):
	if os.path.isdir(obj):
		try:
			sample=int(obj[-2])
			if sample<samples:
				name=obj[:-2]
				file=obj+"/"+obj+".json"
				with open(file) as raw_points:
					points = json.load(raw_points)
					psum = sum(points)
					values[obj]=psum
				print("Read "+obj)
		except ValueError:
			pass
for n in values:
	values[n]=values[n]/(100000)
for t in sorted(values.items(), key=lambda x: x[1]):
	print(t)