import matplotlib.pyplot as plt
import json
import sys

if len(sys.argv) < 2:
    print("Missing ChallengeName argument")
    sys.exit(0)

c_name = sys.argv[1]

r_points = []
p_points = []
with open(c_name+"R.json") as raw_points:
    r_points = json.load(raw_points)

with open(c_name+"P.json") as raw_points:
    p_points = json.load(raw_points)

plt.figure(1)
plt.plot(r_points, "r-")
plt.plot(p_points, "b-")
plt.ylabel("Reward")
plt.xlabel("Step")

plt.show()
