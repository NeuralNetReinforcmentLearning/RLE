import matplotlib.pyplot as plt
import json
import sys
import os

samples=9
values={}
for obj in os.listdir("./"):
	if os.path.isdir(obj):
		try:
			sample=int(obj[-2])
			if sample<samples:
				name=obj[:-2]
				file=obj+"/"+obj+".json"
				with open(file) as raw_points:
					points = json.load(raw_points)
					psum = sum(points)
					if name in values:
						values[name]+=psum
					else:
						values[name]=psum
				print("Read "+obj)
		except ValueError:
			pass
for n in values:
	values[n]=values[n]/(samples*100000)
	print(n+":"+str(values[n]))