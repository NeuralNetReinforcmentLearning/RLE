package layers;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import networks.ModularNetwork;
import utils.Util;

public class FullyConnectedLayer extends TrainableLayer{//Takes Any Matrix->Converts to Column Vector if necessairy
	int nodes;
	
	public RealMatrix columnInput;
	
	public FullyConnectedLayer(int nodes) {
		this.nodes=nodes;
	}

	@Override
	public void initialize(int inColumnDimension, int inRowDimention) {
		super.initialize(inColumnDimension, inRowDimention);
	}

	@Override
	public void initializeTrainableParameters() {
		weights=Util.randGaussianMatrix(ModularNetwork.GAUSS_SCALE, nodes, inCD*inRD).scalarMultiply(1/Math.sqrt(inCD*inRD));//Variance normalized
		biases=new ArrayRealVector(nodes);//All zeros
	}

	@Override
	public RealMatrix passForward(RealMatrix in) {
		this.in=in;
		if(in.getColumnDimension()>1){
			columnInput=toColumnMatrix(in);
		}else columnInput=in;
		out=weights.multiply(columnInput).add(getBiasMatrix());
		return out;
	}

	@Override
	public RealMatrix passBackward(RealMatrix in) {
		RealMatrix out=weights.transpose().multiply(in);
		if(this.in.getColumnDimension()>1){
			out=toRectMatrix(out, this.in.getColumnDimension());
		}
		return out;
	}

	@Override
	public LayerGradient calcBPOutput(RealMatrix errors) {
		RealMatrix wErrors;
		if(errors.getColumnDimension()>1)wErrors=toColumnMatrix(errors);
		else wErrors=errors;

		RealVector nablaB=wErrors.getColumnVector(0);
		RealMatrix nablaW=columnInput.transpose().preMultiply(wErrors); 
		
		return new LayerGradient(nablaW, nablaB);
	}
	
	public RealMatrix getBiasMatrix(){
		RealMatrix biasMatrix=new Array2DRowRealMatrix(biases.getDimension(), 1);
		biasMatrix.setColumnVector(0, biases);
		return biasMatrix;
	}
	
	RealMatrix toRectMatrix(RealMatrix inp, int columns){
		RealMatrix out=new Array2DRowRealMatrix(inp.getRowDimension()/columns, columns);
		for(int r=0; r<inp.getRowDimension(); r++){
			int newRow=r%out.getRowDimension(), newColumn=r/out.getRowDimension();
			out.setEntry(newRow, newColumn, inp.getEntry(r, 0));
		}
		return out;
	}
	
	RealMatrix toColumnMatrix(RealMatrix inp){
		double[] columnArray=new double[inp.getColumnDimension()*inp.getRowDimension()];
		for(int c=0; c<inp.getColumnDimension(); c++){
			int indexOffset=c*inp.getRowDimension();
			for(int r=0; r<inp.getRowDimension(); r++){
				columnArray[indexOffset+r]=inp.getEntry(r, c);
			}
		}
		RealMatrix out=new Array2DRowRealMatrix(columnArray.length, 1);
		out.setColumn(0, columnArray);
		return out;
	}

	@Override
	public int getOutColumnDimension() {
		return 1;
	}

	@Override
	public int getOutRowDimension() {
		return nodes;
	}

	@Override
	public Layer copy() {
		FullyConnectedLayer newLayer=new FullyConnectedLayer(nodes);
		newLayer.initialize(inCD, inRD);
		newLayer.biases=biases.copy();
		newLayer.weights=weights.copy();
		return newLayer;
	}

	@Override
	public String getSaveString() {
		StringBuilder out=new StringBuilder();
		out.append(nodes+"&"+inCD+"&"+inRD+"&");

		for(int j=0; j<biases.getDimension(); j++){
			if(j<biases.getDimension()-1)out.append(biases.getEntry(j)+",");
			else out.append(biases.getEntry(j)+"&");
		}
		
		for(int j=0; j<weights.getRowDimension(); j++){
			for(int k=0; k<weights.getColumnDimension(); k++){
				if(k<weights.getColumnDimension()-1)out.append(weights.getEntry(j,k)+",");
				else out.append(weights.getEntry(j,k));
			}
			if(j<weights.getRowDimension()-1)out.append(";");
			else out.append("");
		}
		return out.toString();
	}

	@Override
	public void constructFromSaveString(String str) {
		String[] parts=str.split("&");
		nodes=Integer.parseInt(parts[0]);
		inCD=Integer.parseInt(parts[1]);
		inRD=Integer.parseInt(parts[2]);
		
		String biasString=parts[3];
		String weightString=parts[4];
		
		String[] biasSplit=biasString.split(",");
		double[] biases=new double[biasSplit.length];
		for(int j=0; j<biases.length; j++)biases[j]=Double.parseDouble(biasSplit[j]);
		
		String[] weightRowSplit=weightString.split(";");
		String[][] weightSplit=new String[weightRowSplit.length][];
		for(int j=0; j<weightRowSplit.length; j++)weightSplit[j]=weightRowSplit[j].split(",");
		
		double[][] weights=new double[weightSplit.length][weightSplit[0].length];
		for(int j=0; j<weightSplit.length; j++){
			for(int k=0; k<weightSplit[j].length; k++){
				weights[j][k]=Double.parseDouble(weightSplit[j][k]);
			}
		}
		
		initialize(inCD, inRD);
		this.weights=new Array2DRowRealMatrix(weights);
		this.biases=new ArrayRealVector(biases);
	}

}
