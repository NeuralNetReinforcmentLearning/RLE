package layers;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

public class PoolingLayer extends Layer{//2x2-2 Stride max pooling layer
	int inSideLenght;
	//public PoolingLayer() {}
	
	@Override
	public void initialize(int inColumnDimension, int inRowDimention) {
		super.initialize(inColumnDimension, inRowDimention);
		inSideLenght=(int)Math.sqrt(inCD);
	}

	@Override
	public RealMatrix passForward(RealMatrix in) {
		this.in=in;
		out=new Array2DRowRealMatrix(in.getRowDimension(), in.getColumnDimension()/4);
		for(int l=0; l<in.getRowDimension(); l++){
			for(int i=0; i<out.getColumnDimension(); i++){
				int offsetX=(i*2)%inSideLenght, offsetY=((i*2)/inSideLenght)*2;//Calculating offset for filter
				double[] maxArgs=new double[4];
				for(int j=0; j<4; j++){
					int relX=j%2, relY=j/2;
					int inputIndex=(offsetY+relY)*inSideLenght+offsetX+relX;
					maxArgs[j]=in.getEntry(l, inputIndex);
				}
				out.setEntry(l, i, absMax(maxArgs));
			}
		}
		return out;
	}
	

	@Override
	public RealMatrix passBackward(RealMatrix in) {
		RealMatrix out=new Array2DRowRealMatrix(in.getRowDimension(), in.getColumnDimension()*4);
		for(int l=0; l<in.getRowDimension(); l++){
			for(int i=0; i<in.getColumnDimension(); i++){
				int offsetX=(i*2)%inSideLenght, offsetY=((i*2)/inSideLenght)*2;//Calculating offset for filter
				double[] maxArgs=new double[4];
				for(int j=0; j<4; j++){
					int relX=j%2, relY=j/2;
					int inputIndex=(offsetY+relY)*inSideLenght+offsetX+relX;
					maxArgs[j]=this.in.getEntry(l, inputIndex);
				}
				int maxIndex=absMaxIndex(maxArgs);
				int relX=maxIndex%2, relY=maxIndex/2;
				int relevantIndex=(offsetY+relY)*inSideLenght+offsetX+relX;
				out.setEntry(l, relevantIndex, in.getEntry(l, i));
			}
		}
		return out;
	}
	
	double absMax(double[] args){
		boolean neg=false;
		double max=-Double.MAX_VALUE;
		for(double d:args){
			double a=Math.abs(d);
			if(a>max){
				max=a;
				neg=d<0;
			}
		}
		if(neg)return -max;
		else return max;
	}
	
	int absMaxIndex(double[] args){
		double max=-Double.MAX_VALUE;
		int idx=-1;
		for(int i=0; i<args.length; i++){
			double a=Math.abs(args[i]);
			if(a>max){
				max=a;
				idx=i;
			}
		}
		return idx;
	}

	@Override
	public int getOutColumnDimension() {
		return inCD/4;
	}

	@Override
	public int getOutRowDimension() {
		return inRD;
	}

	@Override
	public Layer copy() {
		PoolingLayer newLayer=new PoolingLayer();
		newLayer.initialize(inCD, inRD);
		return newLayer;
	}

	@Override
	public String getSaveString() {
		return inCD+"&"+inRD;
	}

	@Override
	public void constructFromSaveString(String str) {
		String[] splited=str.split("&");
		initialize(Integer.parseInt(splited[0]), Integer.parseInt(splited[1]));
	}

}
