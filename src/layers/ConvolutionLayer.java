package layers;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import networks.ModularNetwork;
import utils.Util;

public class ConvolutionLayer extends TrainableLayer{
	int filters, size, stride, padding;
	int outLenght, rowCount, sizeSq;//SizeSquared
	int paddedLenght;
	
	RealMatrix convIn;
	
	public ConvolutionLayer(int filters, int size, int stride, int padding) {
		this.filters=filters;
		this.size=size;
		this.stride=stride;
		this.padding=padding;
	}
	
	@Override
	public void initialize(int inColumnDimension, int inRowDimention) {
		super.initialize(inColumnDimension, inRowDimention);
		sizeSq=size*size;
		outLenght=(int) Math.pow(((Math.sqrt(inCD)-size+2*padding)/stride+1), 2);
		rowCount=inRD*sizeSq;
	}
	

	@Override
	public void initializeTrainableParameters() {
		weights=Util.randGaussianMatrix(ModularNetwork.GAUSS_SCALE, filters, rowCount).scalarMultiply(1/Math.sqrt(filters*rowCount));//Maybe right?
		biases=new ArrayRealVector(filters);//Zerooo
	}
	
	public RealMatrix getBiasMatrix(){
		RealMatrix biasMatrix=new Array2DRowRealMatrix(filters, outLenght);
//		for(int r=0; r<biasMatrix.getRowDimension(); r++){
//			double[] row=new double[biasMatrix.getColumnDimension()];
//			for(int i=0; i<row.length; i++)row[i]=biases.getEntry(r);
//			biasMatrix.setRow(r, row);
//		}
		for(int c=0; c<biasMatrix.getColumnDimension(); c++){
			biasMatrix.setColumnVector(c, biases);
		}
		return biasMatrix;
	}
	
	@Override
	public RealMatrix passForward(RealMatrix in) {
		this.in=in;
		convIn=rearrange(in.copy());
		out=weights.multiply(convIn).add(getBiasMatrix());
		return out;
	}
	
	@Override
	public RealMatrix passBackward(RealMatrix in) {
		RealMatrix convM=weights.transpose().multiply(in);
		RealMatrix out=unRearrange(convM.copy());
		/*RealMatrix out=weights.transpose().multiply(convM);//Works?*/
		return out;
	}
	

	@Override
	public LayerGradient calcBPOutput(RealMatrix errors) {
		double[] biasArr=new double[filters];
		for(int f=0; f<filters; f++){
			double eSum=0;
			for(int c=0; c<outLenght; c++){
				eSum+=errors.getEntry(f, c);
			}
			biasArr[f]=eSum;
		}
		RealVector nablaB=new ArrayRealVector(biasArr);
		
		RealMatrix nablaW=convIn.transpose().preMultiply(errors);//Probably False
		
		return new LayerGradient(nablaW, nablaB);
	}
	
	public RealMatrix rearrange(RealMatrix inp){//Modifies input!!!
		for(int i=0; i<padding; i++){
			inp=zeroPad(inp);
		}
		paddedLenght=inp.getColumnDimension();
		inp=convolude(inp);
		return inp;
	}
	
	public RealMatrix unRearrange(RealMatrix inp){//Modifies input!!!
		inp=unConvolude(inp);
		for(int i=0; i<padding; i++){
			inp=unZeroPad(inp);
		}
		return inp;
	}
	
	RealMatrix zeroPad(RealMatrix inp){
		int inpSideLenght=(int)Math.sqrt(inp.getColumnDimension());
		RealMatrix out=new Array2DRowRealMatrix(inp.getRowDimension(), inp.getColumnDimension()+4*(inpSideLenght+1));
		int outSideLenght=inpSideLenght+2;
		for(int r=0; r<out.getRowDimension(); r++){
			int e=0;
			for(int i=0; i<out.getColumnDimension(); i++){
				int y=i/outSideLenght;
				int x=i%outSideLenght;
				if(y==0||y==outSideLenght-1||x==0||x==outSideLenght-1);//out.setEntry(r, i, 0);
				else{
					out.setEntry(r, i, inp.getEntry(r, e));
					e++;
				}
			}
		}
		return out;
	}
	
	RealMatrix unZeroPad(RealMatrix inp){
		int inpSideLenght=(int)Math.sqrt(inp.getColumnDimension());
		int outSideLenght=inpSideLenght-2;
		RealMatrix out=new Array2DRowRealMatrix(inp.getRowDimension(), inp.getColumnDimension()-4*(outSideLenght+1));
		for(int r=0; r<inp.getRowDimension(); r++){
			int e=0;
			for(int i=0; i<inp.getColumnDimension(); i++){
				int y=i/inpSideLenght;
				int x=i%inpSideLenght;
				if(y==0||y==inpSideLenght-1||x==0||x==inpSideLenght-1);//out.setEntry(r, i, 0);
				else{
					out.setEntry(r, e, inp.getEntry(r, i));
					e++;
				}
			}
		}
		return out;
	}
	
	public RealMatrix convolude(RealMatrix inp){//outLenght columns - rowCount rows
		int inpSideLenght=(int)Math.sqrt(inp.getColumnDimension());//Side Lenght of input image
		RealMatrix out=new Array2DRowRealMatrix(rowCount, outLenght);
		for(int i=0; i<outLenght; i++){
			int offsetX=(i*stride)%inpSideLenght, offsetY=((i*stride)/inpSideLenght)*stride;//Calculating offset for filter
			for(int j=0; j<rowCount; j++){
				int layer=j/(sizeSq);//Current layer
				int layerJ=j%(sizeSq);
				int relX=layerJ%size, relY=layerJ/size;
				int inputIndex=(offsetY+relY)*inpSideLenght+offsetX+relX;
				out.setEntry(j, i, inp.getEntry(layer, inputIndex));
			}
		}
		return out;
	}
	
	public RealMatrix unConvolude(RealMatrix inp){
		int outpSideLenght=(int)Math.sqrt(paddedLenght);//Side Lenght of input image
		RealMatrix out=new Array2DRowRealMatrix(inRD, paddedLenght);
		for(int i=0; i<outLenght; i++){
			int offsetX=(i*stride)%outpSideLenght, offsetY=((i*stride)/outpSideLenght)*stride;//Calculating offset for filter
			for(int j=0; j<rowCount; j++){
				int layer=j/(sizeSq);//Current layer
				int layerJ=j%(sizeSq);
				int relX=layerJ%size, relY=layerJ/size;
				int outputIndex=(offsetY+relY)*outpSideLenght+offsetX+relX;
				out.addToEntry(layer, outputIndex, inp.getEntry(j, i));
			}
		}
		return out;
	}

	@Override
	public int getOutColumnDimension() {
		return outLenght;
	}

	@Override
	public int getOutRowDimension() {
		return filters;
	}

	@Override
	public Layer copy() {
		ConvolutionLayer newLayer=new ConvolutionLayer(filters, size, stride, padding);
		newLayer.initialize(inCD, inRD);
		newLayer.biases=biases.copy();
		newLayer.weights=weights.copy();
		return newLayer;
	}

	@Override
	public String getSaveString() {
		StringBuilder out=new StringBuilder();
		out.append(filters+"&"+size+"&"+stride+"&"+padding+"&"+inCD+"&"+inRD+"&");

		for(int j=0; j<biases.getDimension(); j++){
			if(j<biases.getDimension()-1)out.append(biases.getEntry(j)+",");
			else out.append(biases.getEntry(j)+"&");
		}
		
		for(int j=0; j<weights.getRowDimension(); j++){
			for(int k=0; k<weights.getColumnDimension(); k++){
				if(k<weights.getColumnDimension()-1)out.append(weights.getEntry(j,k)+",");
				else out.append(weights.getEntry(j,k));
			}
			if(j<weights.getRowDimension()-1)out.append(";");
			else out.append("");
		}
		return out.toString();
	}

	@Override
	public void constructFromSaveString(String str) {
		String[] parts=str.split("&");
		filters=Integer.parseInt(parts[0]);
		size=Integer.parseInt(parts[1]);
		stride=Integer.parseInt(parts[2]);
		padding=Integer.parseInt(parts[3]);
		inCD=Integer.parseInt(parts[4]);
		inRD=Integer.parseInt(parts[5]);
		
		String biasString=parts[6];
		String weightString=parts[7];
		
		String[] biasSplit=biasString.split(",");
		double[] biases=new double[biasSplit.length];
		for(int j=0; j<biases.length; j++)biases[j]=Double.parseDouble(biasSplit[j]);
		
		String[] weightRowSplit=weightString.split(";");
		String[][] weightSplit=new String[weightRowSplit.length][];
		for(int j=0; j<weightRowSplit.length; j++)weightSplit[j]=weightRowSplit[j].split(",");
		
		double[][] weights=new double[weightSplit.length][weightSplit[0].length];
		for(int j=0; j<weightSplit.length; j++){
			for(int k=0; k<weightSplit[j].length; k++){
				weights[j][k]=Double.parseDouble(weightSplit[j][k]);
			}
		}
		
		initialize(inCD, inRD);
		this.weights=new Array2DRowRealMatrix(weights);
		this.biases=new ArrayRealVector(biases);
	}

}