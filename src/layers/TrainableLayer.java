package layers;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public abstract class TrainableLayer extends Layer{
	public RealMatrix weights;
	public RealVector biases;
	public abstract void initializeTrainableParameters();
	
	public abstract LayerGradient calcBPOutput(RealMatrix errors);
	
	public class LayerGradient{
		public RealMatrix nablaWeights;
		public RealVector nablaBiases;
		public LayerGradient(RealMatrix nablaWeights, RealVector nablaBiases) {
			this.nablaWeights=nablaWeights;
			this.nablaBiases=nablaBiases;
		}
		
		public LayerGradient add(LayerGradient other){
			return new LayerGradient(nablaWeights.add(other.nablaWeights), nablaBiases.add(other.nablaBiases));
		}
		
		public LayerGradient scale(double scalar){
			return new LayerGradient(nablaWeights.scalarMultiply(scalar), nablaBiases.mapMultiply(scalar));
		}
	}
}