package layers;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import utils.Operation;
import utils.Util;

public class ReLULayer extends Layer{
	public static final double LEAKSCALAR=0.01;

	@Override
	public RealMatrix passForward(RealMatrix in) {
		this.in=in;
		out=activate(in);
		return out;
	}
	
	@Override
	public RealMatrix passBackward(RealMatrix in) {
		return new Array2DRowRealMatrix(Util.componentwiseOperation(Operation.hadamardProductOperation, in.getData(), activatePrime(this.in).getData()));//Backprop with Hadamard
	}
	
	RealMatrix activate(RealMatrix arg){
		double[][] out=new double[arg.getRowDimension()][arg.getColumnDimension()];
		for(int i=0; i<out.length; i++){
			for(int j=0; j<out[i].length; j++){
				out[i][j]=activation(arg.getEntry(i, j));
			}
		}
		return new Array2DRowRealMatrix(out);
	}
	
	RealMatrix activatePrime(RealMatrix arg){
		double[][] out=new double[arg.getRowDimension()][arg.getColumnDimension()];
		for(int i=0; i<out.length; i++){
			for(int j=0; j<out[i].length; j++){
				out[i][j]=activationPrime(arg.getEntry(i, j));
			}
		}
		return new Array2DRowRealMatrix(out);
	}
	
	double activation(double z){//ReLU FTW!!
		return Math.max(LEAKSCALAR*z, z);
	}
	
	double activationPrime(double z){
		if(z>0)return 1;
		else return LEAKSCALAR;
	}

	@Override
	public int getOutColumnDimension() {
		return inCD;
	}
	
	@Override
	public int getOutRowDimension() {
		return inRD;
	}
	
	@Override
	public Layer copy() {
		ReLULayer newLayer=new ReLULayer();
		newLayer.initialize(inCD, inRD);
		return newLayer;
	}

	@Override
	public String getSaveString() {
		return inCD+"&"+inRD;
	}

	@Override
	public void constructFromSaveString(String str) {
		String[] splited=str.split("&");
		initialize(Integer.parseInt(splited[0]), Integer.parseInt(splited[1]));
	}
}