package layers;

import org.apache.commons.math3.linear.RealMatrix;

import interfaces.StringSaveAndLoadable;

public abstract class Layer implements StringSaveAndLoadable{
	public RealMatrix in, out;//In&Out Matrices !Not Always Set!
	public int inCD, inRD;//Input Dimensions !Not Always Set!
	
	public abstract RealMatrix passForward(RealMatrix in);
	public abstract RealMatrix passBackward(RealMatrix in);
	
	public void initialize(int inColumnDimension, int inRowDimention){
		inCD=inColumnDimension;
		inRD=inRowDimention;
	}
	public abstract Layer copy();
	
	public abstract int getOutColumnDimension();
	public abstract int getOutRowDimension();
}