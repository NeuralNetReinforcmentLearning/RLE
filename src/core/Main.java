package core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.UIManager;

import agents.ConvolutionalQNeuralNet;
import environments.SnakeEnvironment;
import gui.InformationPanel;
import utils.PerformanceWatcher;
import utils.PlotOut;

public class Main {
	public static InformationPanel panel;
	public static PerformanceWatcher performanceWatcher;
	public static BufferedImage icon;
	public static double SPS=60;
	
	public static String NNNAME="ImageConv1Prio1LR64x64x64NoAgingAPipe#0";
	
	public static final int SAMPLES=10, SAMPLE_STEPS=100001;
	public static final long TRAINING_TIME=1000*3600*12; //12h in ms
	
	public static void main(String[] args) throws IOException {
		loadIcon();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		performanceWatcher=new PerformanceWatcher(true);
		performanceWatcher.addWatchPoint("ConvLayer 2 Forward", 1);
		performanceWatcher.addWatchPoint("FFW Layer 1 Forward", 2);
		performanceWatcher.addWatchPoint("ConvLayer 2 Backwards", 3);
		performanceWatcher.addWatchPoint("FFW Layer 1 Backwards", 4);
		performanceWatcher.addWatchPoint("ConvLayer 2 Gradient", 42);
		performanceWatcher.addWatchPoint("FFW Layer 1 Gradient", 43);
		performanceWatcher.addWatchPoint("Full Reflection", 5);
		performanceWatcher.addWatchPoint("Full Step", 6);
		performanceWatcher.addWatchPoint("Update", 7);
		performanceWatcher.activate();
		
	//	NESBridge.restartEmulator();
		TaskMaster.setEnvironment(new SnakeEnvironment());

		//Idea: Stand & Die, Earlier Finish - TICK
		TaskMaster.setAgent(new ConvolutionalQNeuralNet());
		
		panel=new InformationPanel();
		
//		TaskMaster.addStepwiseTask(autoSave);
//		PlotOut.setFolder(NNNAME);
//		TaskMaster.addStepwiseTask(autoPlotOut);
		
		TaskMaster.execute();
		
		/*
		TaskMaster.addStepwiseTask(autoSave);
		TaskMaster.addStepwiseTask(autoPlotOut);

		PlotOut.setEnabled(true);
		
//		TaskMaster.setAgent(new KeyboardAgent());
//		TaskMaster.execute();
		for(int s=2; s<SAMPLES; s++){
			System.out.println("Training cycle #"+s);
			if(s!=2){
				System.out.println("Training 32N2SI...");
				NNNAME="32N2SI"+s+"#";
				PlotOut.setFolder(NNNAME);
				{
					ConvolutionalQNeuralNet a32N2SI=new ConvolutionalQNeuralNet(true);
					a32N2SI.setBrain(new ModularNetwork(a32N2SI.inRD, a32N2SI.inCD, true, new Adam(), 
							new FullyConnectedLayer(32), new ReLULayer(),
							new FullyConnectedLayer(TaskMaster.environment.getPossibleActions())));
					a32N2SI.SELECTION_INEQUALITY=2;
					TaskMaster.setAgent(a32N2SI);
					TaskMaster.setMaxSteps(SAMPLE_STEPS);
					TaskMaster.execute();
					manualAuto();
					a32N2SI.destroy();
				}
				
				System.out.println("Training 64N2SI...");
				NNNAME="64N2SI"+s+"#";
				PlotOut.setFolder(NNNAME);
				{
					ConvolutionalQNeuralNet a64N2SI=new ConvolutionalQNeuralNet(true);
					a64N2SI.setBrain(new ModularNetwork(a64N2SI.inRD, a64N2SI.inCD, true, new Adam(), 
							new FullyConnectedLayer(64), new ReLULayer(),
							new FullyConnectedLayer(TaskMaster.environment.getPossibleActions())));
					a64N2SI.SELECTION_INEQUALITY=2;
					TaskMaster.setAgent(a64N2SI);
					TaskMaster.setMaxSteps(SAMPLE_STEPS);
					TaskMaster.execute();
					manualAuto();
					a64N2SI.destroy();
				}
				
				System.out.println("Training 128N2SI...");
				NNNAME="128N2SI"+s+"#";
				PlotOut.setFolder(NNNAME);
				{
					ConvolutionalQNeuralNet a128N2SI=new ConvolutionalQNeuralNet(true);
					a128N2SI.setBrain(new ModularNetwork(a128N2SI.inRD, a128N2SI.inCD, true, new Adam(), 
							new FullyConnectedLayer(128), new ReLULayer(),
							new FullyConnectedLayer(TaskMaster.environment.getPossibleActions())));
					a128N2SI.SELECTION_INEQUALITY=2;
					TaskMaster.setAgent(a128N2SI);
					TaskMaster.setMaxSteps(SAMPLE_STEPS);
					TaskMaster.execute();
					manualAuto();
					a128N2SI.destroy();
				}
			}
			
			System.out.println("Training 64x64N2SI...");
			NNNAME="64x64N2SI"+s+"#";
			PlotOut.setFolder(NNNAME);
			{
				ConvolutionalQNeuralNet a64x64N2SI=new ConvolutionalQNeuralNet(true);
				a64x64N2SI.setBrain(new ModularNetwork(a64x64N2SI.inRD, a64x64N2SI.inCD, true, new Adam(), 
						new FullyConnectedLayer(64), new ReLULayer(),
						new FullyConnectedLayer(64), new ReLULayer(),
						new FullyConnectedLayer(TaskMaster.environment.getPossibleActions())));
				a64x64N2SI.SELECTION_INEQUALITY=2;
				TaskMaster.setAgent(a64x64N2SI);
				TaskMaster.setMaxSteps(SAMPLE_STEPS);
				TaskMaster.execute();
				manualAuto();
				a64x64N2SI.destroy();
			}
			
			System.out.println("Training 64N10SI...");
			NNNAME="64N10SI"+s+"#";
			PlotOut.setFolder(NNNAME);
			{
				ConvolutionalQNeuralNet a64N10SI=new ConvolutionalQNeuralNet(true);
				a64N10SI.setBrain(new ModularNetwork(a64N10SI.inRD, a64N10SI.inCD, true, new Adam(), 
						new FullyConnectedLayer(64), new ReLULayer(),
						new FullyConnectedLayer(TaskMaster.environment.getPossibleActions())));
				a64N10SI.SELECTION_INEQUALITY=10;
				TaskMaster.setAgent(a64N10SI);
				TaskMaster.setMaxSteps(SAMPLE_STEPS);
				TaskMaster.execute();
				manualAuto();
				a64N10SI.destroy();
			}
			
			System.out.println("Training 64N100SI...");
			NNNAME="64N100SI"+s+"#";
			PlotOut.setFolder(NNNAME);
			{
				ConvolutionalQNeuralNet a64N100SI=new ConvolutionalQNeuralNet(true);
				a64N100SI.setBrain(new ModularNetwork(a64N100SI.inRD, a64N100SI.inCD, true, new Adam(), 
						new FullyConnectedLayer(64), new ReLULayer(),
						new FullyConnectedLayer(TaskMaster.environment.getPossibleActions())));
				a64N100SI.SELECTION_INEQUALITY=100;
				TaskMaster.setAgent(a64N100SI);
				TaskMaster.setMaxSteps(SAMPLE_STEPS);
				TaskMaster.execute();
				manualAuto();
				a64N100SI.destroy();
			}
		}
		
		TaskMaster.destroy();
		TaskMaster.stop(true);
		
//		TaskMaster.setMaxSteps(SAMPLE_STEPS);
//		for(int s=0; s<SAMPLES; s++){
//			TaskMaster.setEnvironment(new SimplePongEnvironment());
//			TaskMaster.setAgent(new ConvolutionalQNeuralNet());
//			System.out.println("Executing "+(s+1)+"th sample");
//			TaskMaster.execute();
//			
//			PlotOut.writeTo(PlotOut.getPlotPoints(), s+".json");
//			System.out.println("Wrote "+(s+1)+"th sample");
//			Util.safeSleep(10);
//			PlotOut.reset();
//		}
		
		/*double[] plotPointSum=new double[SAMPLE_STEPS-1];
		for(int s=0; s<SAMPLES; s++){
			TaskMaster.setEnvironment(new SimplePongEnvironment());
			TaskMaster.setAgent(new ConvolutionalQNeuralNet());
			System.out.println("Executing "+(s+1)+"th sample");
			TaskMaster.execute();
			
			Double[] samplePlot=PlotOut.getPlotPoints();
			Thread.sleep(10);
			for(int i=0; i<plotPointSum.length; i++){
				plotPointSum[i]=plotPointSum[i]+samplePlot[i];
			}
			PlotOut.reset();
		}
		
		for(int i=0; i<plotPointSum.length; i++)plotPointSum[i]=plotPointSum[i]/SAMPLES;
		PlotOut.writeTo(plotPointSum, "pongslR.json");*/
	}
	
	static void loadIcon(){
		try {
			icon=ImageIO.read(new File("Icon.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static void manualAuto(){
		saveBrain();
		plotAppend();
	}
	
	static Runnable autoSave=new Runnable() {
		final int STEP_SAVEINTERVAL=30*60*1000;//ms = 1/2h
		long timeSinceLastSave=System.currentTimeMillis();
		@Override
		public void run() {
			if(System.currentTimeMillis()>timeSinceLastSave+STEP_SAVEINTERVAL){
//			if(TaskMaster.steps%STEP_SAVEINTERVAL==0&&TaskMaster.steps!=0){
				if(TaskMaster.agent instanceof ConvolutionalQNeuralNet && TaskMaster.steps>ConvolutionalQNeuralNet.BATCHSIZE){
					saveBrain();
					timeSinceLastSave=System.currentTimeMillis();
				}
			}
		}
	};
	
	final static String saveBasePath="D:\\Schule\\Maturarbeit\\nets\\";
	static void saveBrain(){
		ConvolutionalQNeuralNet agent=(ConvolutionalQNeuralNet)TaskMaster.agent;
		File saveFile=new File(saveBasePath+NNNAME+System.currentTimeMillis()+".brain");
		agent.brain.saveNet(saveFile);
		System.out.println(TaskMaster.steps+": Brain auto-saved to: "+saveFile);
	}
	
	static Runnable autoPlotOut=new Runnable() {
		final int STEP_SAVEINTERVAL=30*60*1000;//ms = 1/2h
		long timeSinceLastSave=System.currentTimeMillis();
		@Override
		public void run() {
			if(System.currentTimeMillis()>timeSinceLastSave+STEP_SAVEINTERVAL){
//			if(TaskMaster.steps%STEP_SAVEINTERVAL==0&&TaskMaster.steps!=0){
				plotAppend();
				timeSinceLastSave=System.currentTimeMillis();
			}
		}
	};
	
	static void plotAppend(){
		PlotOut.appendTo(PlotOut.getPlotPoints(), NNNAME+".json");
		PlotOut.reset();
		System.out.println("Wrote Plot to "+NNNAME+".json");
		System.out.println("Step: "+TaskMaster.steps);
	}
	
	void checkGradients(){
//		ModularNetwork convNet=new ModularNetwork(true, new ConvolutionLayer(3,3,1,1), new ReLULayer(), new PoolingLayer(), new ConvolutionLayer(1,3,1,1), new ReLULayer(), new PoolingLayer(), new FullyConnectedLayer(3), new ReLULayer(), new FullyConnectedLayer(1));
//		RealMatrix input=new Array2DRowRealMatrix(new double[][]{{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}});
//		RealMatrix out=convNet.feedForward(input);
//		RealMatrix ans=out.scalarAdd(100);
////		System.out.println(convNet.backpropagate(ans)[1].nablaWeights.getEntry(0, 0));
//		System.out.println(convNet.backpropagate(ans)[1].nablaBiases.getEntry(0));
//		
//		double eps=0.00001;
//		ConvolutionLayer layer=(ConvolutionLayer)convNet.layers[3];
////		layer.weights.setEntry(0, 0, layer.weights.getEntry(0, 0)+eps);
//		layer.biases.setEntry(0, layer.biases.getEntry(0)+eps);
//		layer.initBiasMatrix();
//		RealMatrix out1=convNet.feedForward(input);
////		layer.weights.setEntry(0, 0, layer.weights.getEntry(0, 0)-2*eps);
//		layer.biases.setEntry(0, layer.biases.getEntry(0)-2*eps);
//		layer.initBiasMatrix();
//		RealMatrix out2=convNet.feedForward(input);
//		
//		double e1=0;
//		double[][] sq1=Util.componentwiseOperation(Operation.squareOperation, out1.subtract(ans).getData());
//		double e2=0;
//		double[][] sq2=Util.componentwiseOperation(Operation.squareOperation, out2.subtract(ans).getData());
//		for(int i=0; i<sq2.length; i++){
//			for(int j=0; j<sq2[0].length; j++){
//				e1+=sq1[i][j]/2;
//				e2+=sq2[i][j]/2;
//			}
//		}
//		System.out.println((e1-e2)/(2*eps));
	}
}