package core;

import java.util.ArrayList;

import interfaces.Agent;
import interfaces.Destructable;
import interfaces.Environment;

public class TaskMaster {
	public static Agent agent;
	public static Environment environment;
	public static int maxSteps=-1;
	public static long finishTime=-1;
	
	private static ArrayList<Runnable> stepwiseTasks=new ArrayList<>(), toAdd=new ArrayList<>(), toRemove=new ArrayList<>();
	
	public static boolean paused=true;
	static boolean stop=false;
	static boolean running=false;
	static boolean exit=false;
	
	public static void setTask(Agent agent, Environment environment){
		TaskMaster.agent=agent;
		TaskMaster.environment=environment;
		maxSteps=-1;
	}
	
	public static void setAgent(Agent agent){
		TaskMaster.agent=agent;
	}
	
	public static void setEnvironment(Environment environment){
		TaskMaster.environment=environment;
	}
	
	public static void setMaxSteps(int maxSteps){
		TaskMaster.maxSteps=maxSteps;
	}
	
	public static void setFinishTime(long finishTime){
		TaskMaster.finishTime=finishTime;
	}
	
	public static void setTask(Agent agent, Environment environment, int maxSteps){
		TaskMaster.agent=agent;
		TaskMaster.environment=environment;
		TaskMaster.maxSteps=maxSteps;
	}
	
	public static int steps=0;
	public static void execute(){//Blocks
		running=true;
		steps=0;
		try{
			mainLoop:while(!stop && (steps<maxSteps || maxSteps==-1) && (System.currentTimeMillis()<finishTime || finishTime==-1)){
				while(paused){
					Thread.sleep(10);
					if(stop)break mainLoop;
				}
				long startTime=System.currentTimeMillis();
				
				Main.performanceWatcher.startWatching(6);
				int selectedAction=agent.step(environment.getCurrentState(), environment.getLastReward(), environment.isTerminal());
				Main.performanceWatcher.endWatching(6);
				Main.performanceWatcher.startWatching(7);
				environment.update(selectedAction);
				Main.performanceWatcher.endWatching(7);
				
				for(Runnable nT:toAdd)stepwiseTasks.add(nT);
				if(toAdd.size()>0)toAdd=new ArrayList<>();
				for(Runnable nT:toRemove)stepwiseTasks.remove(nT);
				if(toRemove.size()>0)toRemove=new ArrayList<>();
				for(Runnable task:stepwiseTasks){
					try{
						task.run();
					}catch(Exception e){
						System.err.println("Error while executing Stepwisetask "+task.getClass().getName());
						e.printStackTrace();
					}
				}
				
				if(environment.isDrawable()&&Main.panel.chckbxDraw.isSelected())Main.panel.screen.repaint();
				if(Main.panel.chckbxPlot.isSelected())Main.panel.addReward(environment.getLastReward(), environment.isTerminal());
					
				long deltaTime=System.currentTimeMillis()-startTime;
				long sleepTime=(long) (1000/Main.SPS-deltaTime);
				if(Main.SPS!=0&&sleepTime>0)Thread.sleep(sleepTime);
				steps++;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		running=false;
		if(exit){
			System.exit(0);
			destroy();
		}
	}
	
	public static void addStepwiseTask(Runnable task){
		toAdd.add(task);
	}
	
	public static void removeStepwiseTask(Runnable task){
		toRemove.add(task);
	}
	
	public static void destroy(){
		if(agent instanceof Destructable)((Destructable)agent).destroy();
		if(environment instanceof Destructable)((Destructable)environment).destroy();
	}
	
	public static void stop(){
		stop=true;
	}
	
	public static void stop(boolean exit){
		if(!running && exit){
			destroy();
			System.exit(0);
		}else if(exit && stop){
			destroy();
			System.exit(0);
		}else{
			stop=true;
			TaskMaster.exit=exit;
		}
	}
}
