package utils;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Random;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class Util {
	
	public static Random r=new Random();
	public static Robot wallE;//=new Robot();
	
	static {
		try {
			wallE=new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
	
	public static void safeSleep(long millis){//Fuck you java
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void lClick(int x, int y){
		wallE.mouseMove(x, y);
		wallE.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		wallE.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
	}
	
	public static double randGaussian(){
		return r.nextGaussian();
	}
	
	public static double[] colorToGrayscale(double[] colorArr){
		int imgVol=colorArr.length/3;
		double[] outArr=new double[imgVol];
		for(int i=0; i<outArr.length; i++){
			outArr[i]=0.21*colorArr[i]+0.72*colorArr[i+imgVol]+0.07*colorArr[i+imgVol*2];//Lumosity grayscaling
		}
		return outArr;
	}
	
	public static double[] imgToRGBArray(BufferedImage img){
		int imgVol=img.getHeight()*img.getWidth();
		int outSize=imgVol*3;
		int[] imgData=((DataBufferInt)img.getRaster().getDataBuffer()).getData();
		double[] outArr=new double[outSize];
		for(int i=0; i<imgVol; i++){
			//int x=i%img.getWidth(), y=i/img.getWidth();
			Color c=new Color(imgData[i]);
			outArr[i]=c.getRed()/256d;
			outArr[i+imgVol]=c.getGreen()/256d;
			outArr[i+imgVol*2]=c.getBlue()/256d;
		}
		return outArr;
	}
	
	public static BufferedImage scale(BufferedImage image, double widthScalar, double heightScalar){
		int scaledWidth=(int) (image.getWidth()*widthScalar);
		int scaledHeight=(int) (image.getHeight()*heightScalar);
		Image tmpImg=image.getScaledInstance(scaledWidth, scaledHeight, BufferedImage.SCALE_SMOOTH);
		BufferedImage end=new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
		Graphics g2=end.getGraphics();
		g2.drawImage(tmpImg, 0, 0, null);
		g2.dispose();
		return end;
	}
	
	public static RealMatrix randGaussianMatrix(double scale, int rowDimension, int columnDimension){
		RealMatrix out=new Array2DRowRealMatrix(rowDimension, columnDimension);
		for(int r=0; r<rowDimension; r++){
			for(int c=0; c<columnDimension; c++){
				out.setEntry(r, c, randGaussian()*scale);
			}
		}
		return out;
	}
	
	public static RealVector randGaussianVector(double scale, int dimension){
		RealVector out=new ArrayRealVector(dimension);
		for(int i=0; i<dimension; i++)out.setEntry(i, randGaussian()*scale);
		return out;
	}
	
	public static RealVector elementPow(RealVector vec, double p){
		RealVector outVec=new ArrayRealVector(vec.getDimension());
		for(int i=0; i<vec.getDimension(); i++)outVec.setEntry(i, Math.pow(vec.getEntry(i),p));
		return outVec;
	}
	
	public static RealMatrix elementPow(RealMatrix matr, double p){//Element-wise power
		RealMatrix outMatr=new Array2DRowRealMatrix(matr.getRowDimension(), matr.getColumnDimension());
		for(int i=0; i<outMatr.getColumnDimension(); i++)outMatr.setColumnVector(i, elementPow(matr.getColumnVector(i), p));
		return outMatr;
	}
	
	public static double[][] componentwiseOperation(Operation operator, double[][]... inputs){
		int maxWidth=-1, maxHeight=-1, minWidth=Integer.MAX_VALUE, minHeight=Integer.MAX_VALUE;
		for(double[][] in:inputs){
			if(in.length>maxWidth)maxWidth=in.length;
			if(in[0].length>maxHeight)maxHeight=in[0].length;
			if(in.length<minWidth)minWidth=in.length;
			if(in[0].length<minHeight)minHeight=in[0].length;
		}
		if(maxWidth!=minWidth||maxHeight!=minHeight)throw new IllegalArgumentException("Input lenghts dont match");
		double[][] out=new double[maxWidth][maxHeight];
		for(int x=0; x<maxWidth; x++){
			for(int y=0; y<maxHeight; y++){
				double[] args=new double[inputs.length];
				for(int i=0; i<inputs.length; i++){
					args[i]=inputs[i][x][y];
				}
				out[x][y]=operator.operate(args);
			}
		}
		return out;
	}
	
	public static double elementSum(RealMatrix inp){
		double sum=0;
		for(int r=0; r<inp.getRowDimension(); r++){
			for(int c=0; c<inp.getColumnDimension(); c++){
				sum+=inp.getEntry(r, c);
			}
		}
		return sum;
	}
	
	public static RealMatrix componentwiseOperation(Operation operator, RealMatrix... inputs){
		double[][][] nIn=new double[inputs.length][][];
		for(int i=0; i<inputs.length; i++)nIn[i]=inputs[i].getData();
		return new Array2DRowRealMatrix(componentwiseOperation(operator, nIn));
	}
	
	public static double[] componentwiseVectorOperation(Operation operator, double[]... inputs){
		int maxLenght=-1, minLenght=Integer.MAX_VALUE;
		for(double[] in:inputs){
			if(in.length>maxLenght)maxLenght=in.length;
			if(in.length<minLenght)minLenght=in.length;
		}
		if(maxLenght!=minLenght)throw new IllegalArgumentException("Input lenghts dont match");
		double[] out=new double[maxLenght];
		for(int i=0; i<maxLenght; i++){
			double[] args=new double[inputs.length];
			for(int j=0; j<inputs.length; j++)args[j]=inputs[j][i];
			out[i]=operator.operate(args);
		}
		return out;
	}
	
	public static double[] unpackArray(Double[] p){
		double[] out=new double[p.length];
		for(int i=0; i<out.length; i++)out[i]=p[i];
		return out;
	}
}
