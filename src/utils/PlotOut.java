package utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

public class PlotOut {
	//static StringBuilder plotString=new StringBuilder();
	static ArrayList<Double> plotPoints=new ArrayList<>();
	static final String basePath="D:\\Schule\\Maturarbeit\\plots\\ImageMario\\";
	public static String folder=null;
	public static boolean enabled=false;
	
	public static void setEnabled(boolean enabled){
		PlotOut.enabled=enabled;
	}
	
	public static void plotOut(double plotVal){
		if(enabled)plotPoints.add(plotVal);
		/*if(plotString.toString().equals(""))plotString.append("[");
		plotString.append(plotVal+",");*/
	}
	
	public static void reset(){
		plotPoints=new ArrayList<>();
	}
	
	public static double[] getPlotPoints(){
		return Util.unpackArray(plotPoints.toArray(new Double[0]));
	}
	
	public static void setFolder(String folder){
		PlotOut.folder=folder;
		File f=new File(basePath+folder);
		f.mkdirs();
	}
	
	public static void appendTo(double[] plotPoints, String fileName){
		File pOut=new File(basePath+folder+"\\"+fileName);
		try {
			StringBuilder plotString;
			if(pOut.exists()){
				plotString=new StringBuilder(new String(Files.readAllBytes(pOut.toPath())));
				plotString.setCharAt(plotString.length()-1, ',');
			}else{
				plotString=new StringBuilder();
				plotString.append("[");
			}
			for(double d: plotPoints) plotString.append(d+",");
			plotString.setCharAt(plotString.length()-1, ']');
			File oFile=new File(basePath+folder+"\\"+fileName);
			FileWriter fOut=new FileWriter(oFile);
			fOut.write(plotString.toString());
			fOut.flush();
			fOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeTo(double[] plotPoints, String fileName){
		StringBuilder plotString=new StringBuilder();
		plotString.append("[");
		for(double d: plotPoints) plotString.append(d+",");
		plotString.setCharAt(plotString.length()-1, ']');
		try{
			File oFile=new File(basePath+folder+"\\"+fileName);
			FileWriter fOut=new FileWriter(oFile);
			fOut.write(plotString.toString());
			fOut.flush();
			fOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
