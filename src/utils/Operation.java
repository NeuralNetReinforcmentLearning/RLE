package utils;


public abstract class Operation {
	
	public abstract double operate(double[] arguments);
	
	public static HadamardProductOperation hadamardProductOperation=new HadamardProductOperation();
	public static HadamardDivisionOperation hadamardDivisionOperation=new HadamardDivisionOperation();
	public static AbsoluteOperation absoluteOperation=new AbsoluteOperation();
	public static SquareOperation squareOperation=new SquareOperation(); 
	
}

class HadamardProductOperation extends Operation{

	@Override
	public double operate(double[] arguments) {
		double out=1;
		for(double d:arguments)out*=d;
		return out;
	}
	
}

class HadamardDivisionOperation extends Operation{

	@Override
	public double operate(double[] arguments) {
		return arguments[0]/arguments[1];
	}
	
}

class AbsoluteOperation extends Operation{

	@Override
	public double operate(double[] arguments) {
		return Math.abs(arguments[0]);
	}
	
}

class SquareOperation extends Operation{

	@Override
	public double operate(double[] arguments) {
		return Math.pow(arguments[0], 2);
	}
	
}