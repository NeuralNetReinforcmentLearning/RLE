package utils;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class PerformanceWatcher extends JFrame{
	HashMap<Integer, WatchPoint> watchPoints=new HashMap<>();
	private JTable table;
	boolean disabled=false;
	public PerformanceWatcher() {
		init();
	}
	
	void init(){
		setTitle("PerformanceWatcher");
		setResizable(false);
		
		Dimension size=new Dimension(400, 500);
		setPreferredSize(size);
		setMaximumSize(size);
		setMinimumSize(size);
		
		setLocation(50, 100);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		scrollPane.setViewportView(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		table = new JTable();
		table.setEnabled(false);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"Name", "Time", "Average"},
			},
			new String[] {
				"Name", "Time", "Average"
			}
		));
		panel.add(table, BorderLayout.CENTER);
	}
	
	public PerformanceWatcher(boolean disabled){
		if(disabled){
			this.disabled=true;
			dispose();
		}
		else init();
	}
	
	public void activate(){
		if(!disabled)setVisible(true);
	}
	
	public void addWatchPoint(String name, int id){
		if(!disabled){
			if(!watchPoints.containsKey(id)){
				int rowIndex=addRow(name);
				watchPoints.put(id, new WatchPoint(name, id, rowIndex));
			}else System.err.println("Already WatchPoint with same id");
		}
	}
	
	int addRow(String name){//Returns rowIndex of new entry
		TableModel oldModel=table.getModel();
		Object[][] newData=new Object[oldModel.getRowCount()+1][oldModel.getColumnCount()];
		for(int r=0; r<newData.length-1; r++){
			newData[r][0]=oldModel.getValueAt(r, 0);
			newData[r][1]=oldModel.getValueAt(r, 1);
			newData[r][2]=oldModel.getValueAt(r, 2);
		}
		newData[newData.length-1][0]=name;
		newData[newData.length-1][1]=0+"ns";
		newData[newData.length-1][2]=0+"ns";
		TableModel newModel=new DefaultTableModel(newData, new String[]{"Name", "Time", "Average"});
		table.setModel(newModel);
		return newData.length-1;
	}
	
	public void startWatching(int id){
		if(!disabled)watchPoints.get(id).start();
	}
	
	public void endWatching(int id){
		if(!disabled){
			WatchPoint wp=watchPoints.get(id);
			wp.end(System.nanoTime());
			table.getModel().setValueAt(wp.elapsedTime+"ns", wp.rowIndex, 1);
			table.getModel().setValueAt(wp.getAverage()+"ns", wp.rowIndex, 2);
		}
	}
}

class WatchPoint{
	String name;
	int id;
	int rowIndex;
	WatchPoint(String name, int id, int rowIndex){
		this.name=name;
		this.id=id;
		this.rowIndex=rowIndex;
	}
	int samples;
	long epsTimeSum;
	
	long startedAt;
	int elapsedTime;
	
	void start(){
		startedAt=System.nanoTime();
	}
	
	void end(long currentTime){
		elapsedTime=(int)(currentTime-startedAt);
		samples++;
		epsTimeSum+=elapsedTime;
	}
	
	int getAverage(){
		return (int) (epsTimeSum/samples);
	}
}