package agents;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import core.Main;
import core.TaskMaster;
import gui.NNControl;
import interfaces.Agent;
import interfaces.Destructable;
import interfaces.ImageEnvironment;
import layers.ConvolutionLayer;
import layers.FullyConnectedLayer;
import layers.ReLULayer;
import layers.TrainableLayer.LayerGradient;
import networks.ModularNetwork;
import optimizers.Adam;
import utils.PlotOut;

public class ConvolutionalQNeuralNet implements Agent, Destructable{
	public static final int PLOT_AVERAGING=1000, WORKER_THREADS=7;//ImageSize: SideLength
	public final static double LEARNINGRATE=0.2, DISCOUNT=0.9, EPS_MIN=0.02, EPS_MAX=0.1, EPS_STEPS=35000, EPS_STEPRATE=(EPS_MAX-EPS_MIN)/EPS_STEPS;
	public final static double BRAINLEARNINGRATE=1;
	public double SELECTION_INEQUALITY=1;
	public static final double MAX_EXP=10000;
	public final static int BATCHSIZE=32, REPLAYSIZE=30000, EXPERIENCE_HISTBUNDLES=2;
	public final static boolean SOFTMAX_EXPREPLAY=false, TRAIN=true;

	public int inRD, inCD, inputChannels;
	
	public ModularNetwork brain;
	ExecutorService executor=Executors.newFixedThreadPool(WORKER_THREADS);
	
	NNControl controlPanel;
	
	ArrayList<ConvolutionalExperience> experienceReplay=new ArrayList<>();
	
	int steps=0;
	
	ConvolutionalExperience curExp;
	RealVector oldQVals;
	
	double minVal, scale;//QVal<->BrainVal conversion
	
	public ConvolutionalQNeuralNet() {
//		double qLim=1/(1-DISCOUNT);
//		double minQ=Main.environment.getRewardRange()[0]*qLim;
//		double maxQ=Main.environment.getRewardRange()[1]*qLim;
//		minVal=minQ;
//		scale=maxQ-minQ;
		
		calcInDimensions();
		
		defaultInit();
	}
	
	public ConvolutionalQNeuralNet(boolean customInit){
		calcInDimensions();
		if(!customInit)defaultInit();
	}
	
	void defaultInit(){
		brain=new ModularNetwork(inRD, inCD, true, new Adam(), 
				//new PoolingLayer(), 
				new ConvolutionLayer(16, 8, 4, 4), new ReLULayer(), new ConvolutionLayer(16, 4, 2, 2), new ReLULayer(),// new PoolingLayer(), 
				new ConvolutionLayer(4, 3, 2, 1), new ReLULayer(), new ConvolutionLayer(4, 3, 2, 1), new ReLULayer(), //new PoolingLayer(), 
				//new ConvolutionLayer(4, 3, 1, 1), new ReLULayer(), new ConvolutionLayer(4, 3, 1, 1), new ReLULayer(), new PoolingLayer(), 
				//new FullyConnectedLayer(64), new ReLULayer(),
				new FullyConnectedLayer(32), new ReLULayer(),
				new FullyConnectedLayer(32), new ReLULayer(),
//				new FullyConnectedLayer(64), new ReLULayer(),
				//new FullyConnectedLayer(128), new ReLULayer(), 
				//new FullyConnectedLayer(512), new ReLULayer(), 
				//new FullyConnectedLayer(20), new ReLULayer(), 
				new FullyConnectedLayer(TaskMaster.environment.getPossibleActions()));
		
		controlPanel=new NNControl(brain, REPLAYSIZE, 0, 1);
	}
	
	public void setBrain(ModularNetwork brain){
		this.brain=brain;
		controlPanel=new NNControl(brain, REPLAYSIZE, 0, 1);
	}
	
	public void calcInDimensions(){
		if(TaskMaster.environment instanceof ImageEnvironment){
			ImageEnvironment imgEnv=(ImageEnvironment)TaskMaster.environment;
			inputChannels=imgEnv.getOutputChannels();
			inCD=imgEnv.getOutputSize();
		}else{
			inputChannels=1;
			inCD=TaskMaster.environment.getStateSize();
		}
		inRD=inputChannels*(1+EXPERIENCE_HISTBUNDLES);//Account for histbundles
	}
	
	@Override
	public int step(double[] rawState, double reward, boolean terminal) {//terminal if transition was terminal
		double[][] state=addPastStates(toConvolutionalState(rawState), terminal);
		
		if(steps>BATCHSIZE && TRAIN)train();
		
		//Calculate qVals for current state
		RealVector qVals=brain.feedForward(state).getColumnVector(0);
		
		//Finalize curExp
		if(curExp!=null){
			curExp.afterState=state;
			curExp.reward=reward;
			curExp.terminal=terminal;
			
			BrainWorker.updateExperience(curExp, oldQVals.getEntry(curExp.action), qVals.getEntry(selectBestLegalAction(qVals)), this);
			
			//Add plot information
			double costSum=curExp.cost, rewardSum=curExp.reward;
			int samples=Math.min(experienceReplay.size(), PLOT_AVERAGING);
			for(int i=experienceReplay.size()-1; i>=experienceReplay.size()-samples; i--){
				costSum+=experienceReplay.get(i).cost;
				rewardSum+=experienceReplay.get(i).reward;
			}
			curExp.costAvg=costSum/(samples+1);
			curExp.rewardAvg=rewardSum/(samples+1);
			
			experienceReplay.add(curExp);
			if(experienceReplay.size()>REPLAYSIZE){
				experienceReplay.remove(0);
			}
			
			//Plot
			if(controlPanel.plot()){
				controlPanel.setQVals(qVals.toArray());
				plot();
			}
			PlotOut.plotOut(curExp.rewardAvg);
		}
		
		//New curExp
		curExp=new ConvolutionalExperience();
		curExp.preState=state;
		if(Math.random()<(getEps(steps))){//Explore
			curExp.action=selectRandomLegalAction();
		}else{//Exploit
			curExp.action=selectBestLegalAction(qVals);
		}
		
		oldQVals=qVals;
		steps++;
		
		while(controlPanel.block){
			Thread.yield();
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		return curExp.action;
	}
	
	void plot(){
		double[] plotPoints=new double[experienceReplay.size()];
		for(int i=0; i<plotPoints.length; i++){
			if(controlPanel.modeChooser.getSelectedIndex()==0){
				plotPoints[i]=experienceReplay.get(i).cost;
			}else if(controlPanel.modeChooser.getSelectedIndex()==1){
				plotPoints[i]=experienceReplay.get(i).costAvg;
			}else if(controlPanel.modeChooser.getSelectedIndex()==2){
				plotPoints[i]=experienceReplay.get(i).rewardAvg;
			}
		}
		controlPanel.setPlotArray(plotPoints);
	}
	
	void train(){
		ConvolutionalExperience[] batch=new ConvolutionalExperience[Math.min(BATCHSIZE, experienceReplay.size()+1)];
		
		//Select from expReplay with softmax
//		long start=System.currentTimeMillis();
		if(SOFTMAX_EXPREPLAY){
			double[] probs=new double[experienceReplay.size()];
			double expSum=0;
			for(int i=0; i<probs.length; i++)expSum+=experienceReplay.get(i).costExp;
			
			for(int i=0; i<probs.length; i++){
				probs[i]=experienceReplay.get(i).costExp/expSum;
			}
			
			for(int i=0; i<batch.length; i++){
				double r=Math.random();
				double sum=0;
				int expIndx=-1;
				randPicker:for(int j=0; j<experienceReplay.size(); j++){
					sum+=probs[j];
					if(sum>=r){
						expIndx=j;
						break randPicker;
					}
				}
				if(expIndx==-1){
					System.out.println("fail");
				}
				batch[i]=experienceReplay.get(expIndx);
			}
		}else{
			for(int i=0; i<batch.length; i++)batch[i]=experienceReplay.get((int)(Math.random()*experienceReplay.size()));
		}
//		System.out.println("Selection Time: "+(System.currentTimeMillis()-start)+"ms");
//		for(int i=1; i<batch.length; i++)batch[i]=experienceReplay.get((int)(Math.random()*experienceReplay.size()));
		
		Main.performanceWatcher.startWatching(5);
		//Distribute work onto BrainWorkers
		ConvolutionalExperience[][] batchParts=new ConvolutionalExperience[WORKER_THREADS][];
		int remainingExperiences=batch.length;
		int bIndex=0;
		BrainWorker[] workers=new BrainWorker[WORKER_THREADS];
		for(int i=0; i<WORKER_THREADS; i++){
			int partLenght=remainingExperiences/(WORKER_THREADS-i);
			batchParts[i]=new ConvolutionalExperience[partLenght];
			remainingExperiences-=partLenght;
			for(int b=0; b<partLenght; b++){
				batchParts[i][b]=batch[bIndex+b];
			}
			bIndex+=partLenght;
			
			//Reflection about Experiences
			workers[i]=new BrainWorker(this, batchParts[i]);
			executor.execute(workers[i]);
		}
		
		executor.shutdown();
		while(!executor.isTerminated());
		Main.performanceWatcher.endWatching(5);
		executor=Executors.newFixedThreadPool(WORKER_THREADS);
		
		LayerGradient[][] layerGradients=new LayerGradient[batch.length][];
		int lgIndex=0;
		for(BrainWorker bw:workers){
			for(int g=0; g<bw.gradientParts.length; g++){
				layerGradients[lgIndex+g]=bw.gradientParts[g];
			}
			lgIndex+=bw.gradientParts.length;
		}
		
		//Summing Gradients
		LayerGradient[] layerGradientSum=layerGradients[0];
		for(int i=1; i<batch.length; i++){
			for(int l=0; l<layerGradientSum.length; l++){
				layerGradientSum[l]=layerGradientSum[l].add(layerGradients[i][l]);
			}
		}
		
		//Averaging Gradients
		for(int l=0; l<layerGradientSum.length; l++){
			layerGradientSum[l]=layerGradientSum[l].scale(1d/batch.length);
		}
		
		brain.train(layerGradientSum);
	}
	
	double getEps(int steps){
		return Math.max(EPS_MAX-EPS_STEPRATE*steps, EPS_MIN);
	}
	
	double[][] addPastStates(double[][] state, boolean terminal){// State: t+1 curExp: t newest in ExpReplay: t-1
		double[][] outState=new double[inRD][inCD];
		int expReplayIndex=experienceReplay.size();
		boolean pastTerminalState=false;
		for(int c=0; c<outState.length; c++){
			int pastNr=c/inputChannels;
			if(pastNr==0){
				outState[c]=state[c];
				pastTerminalState=terminal;//True if t+1 is terminal
			}else{
				if(expReplayIndex-pastNr>=0 && !pastTerminalState){
					ConvolutionalExperience pastExp=experienceReplay.get(expReplayIndex-pastNr);
					outState[c]=pastExp.afterState[c%inputChannels];
					if(pastExp.terminal)pastTerminalState=true;
				}
				else outState[c]=outState[c-inputChannels];//Copy previous state
			}
		}
		return outState;
	}
	
	double[][] toConvolutionalState(double[] inState){
		if(inRD==1)return new double[][]{inState};
		double[][] convState=new double[inRD][inCD];
		for(int i=0; i<inState.length; i++)convState[i/(inCD)][i%(inCD)]=inState[i];
		return convState;
	}
	
	static int selectRandomLegalAction(){
		int selectedAction=-1;
		while(selectedAction==-1){
			int rAction=(int)(TaskMaster.environment.getPossibleActions()*Math.random());
			if(TaskMaster.environment.isLegalAction(rAction))selectedAction=rAction;
		}
		return selectedAction;
	}
	
	static int selectBestLegalAction(RealVector qVals){
		RealVector qValsCopy=qVals.copy();
		int selectedAction=-1;
		while(selectedAction==-1){
			if(qValsCopy.getMaxValue()==-Double.MAX_VALUE)break;
			int maxAc=qValsCopy.getMaxIndex();
			if(TaskMaster.environment.isLegalAction(maxAc))selectedAction=maxAc;
			else qValsCopy.setEntry(maxAc, -Double.MAX_VALUE);
		}
		return selectedAction;
	}

	@Override
	public void destroy() {
		controlPanel.dispose();
	}
	
}

class BrainWorker implements Runnable{
	ConvolutionalExperience[] batchPart;
	LayerGradient[][] gradientParts;
	ModularNetwork brain;
	ConvolutionalQNeuralNet master;
	
	public BrainWorker(ConvolutionalQNeuralNet master, ConvolutionalExperience[] batchPart) {
		this.batchPart=batchPart;
		this.brain=master.brain.copy();//Thread safe
		this.master=master;
		gradientParts=new LayerGradient[batchPart.length][];
	}

	@Override
	public void run() {
		for(int i=0; i<batchPart.length; i++){
			gradientParts[i]=reflectExperience(batchPart[i]);
		}
	}
	
	static double updateExperience(ConvolutionalExperience exp, double oldVal, double bestNextVal, ConvolutionalQNeuralNet master){
		//Calculate correct cost
		double corrQVal;
		if(exp.terminal){
			corrQVal=oldVal+ConvolutionalQNeuralNet.LEARNINGRATE*(exp.reward-oldVal);//This is the end my friend
		}else{
			corrQVal=oldVal+ConvolutionalQNeuralNet.LEARNINGRATE*(exp.reward+ConvolutionalQNeuralNet.DISCOUNT*bestNextVal-oldVal);
		}
		//System.out.println(oldVal+":"+corrQVal);
		
		//Adjust Cost-Values
		double newCost=master.brain.cost(oldVal, corrQVal);//Adjusting cost
		//System.out.println("Cost: "+newCost);
		
		exp.cost=newCost;//Math.min(newCost, ConvolutionalQNeuralNet.MAX_COST);
		if(ConvolutionalQNeuralNet.SOFTMAX_EXPREPLAY)exp.costExp=Math.min(ConvolutionalQNeuralNet.MAX_EXP, Math.exp(newCost*master.SELECTION_INEQUALITY));
		
		return corrQVal;
	}
	
	LayerGradient[] reflectExperience(ConvolutionalExperience exp){
		//Calculate value of best action in the next state
		RealVector nextActOutput=brain.feedForward(exp.afterState).getColumnVector(0);
		double bestNextQVal=nextActOutput.getEntry(ConvolutionalQNeuralNet.selectBestLegalAction(nextActOutput));
		
		//Calculate "old" value
		RealMatrix preQOutput=brain.feedForward(exp.preState);
		double oldVal=preQOutput.getEntry(exp.action, 0);
		
		double corrQVal=updateExperience(exp, oldVal, bestNextQVal, master);
		
		//Backpropagate
		RealMatrix bpInput=preQOutput.copy();
		bpInput.setEntry(exp.action, 0, corrQVal);
		LayerGradient[] bpOutput=brain.backpropagate(bpInput);
		
		return bpOutput;
	}
		
	public LayerGradient[][] getGradientParts(){
		return gradientParts;
	}
	
}

class ConvolutionalExperience{
	double[][] preState;
	double[][] afterState;
	int action;
	boolean terminal;
	double reward;
	double rewardAvg;
	double cost;
	double costExp;
	double costAvg;
}
