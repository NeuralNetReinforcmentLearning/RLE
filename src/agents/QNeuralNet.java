package agents;

import org.apache.commons.math3.linear.ArrayRealVector;

import core.TaskMaster;
import interfaces.Agent;
import interfaces.BackPropagationOutput;

public class QNeuralNet implements Agent{
	final double LEARNINGRATE=1, DISCOUNT=0, EPS=0.01, BRAINLEARNINGRATE=1;
	Network brain;
	double minVal, scale;//(minQ,maxQ) zu (0,1) f�r NN
	
	double[] lastState;
	
	double[] Qs;
	int bestAct;
	
	double[] oldQs;
	int lastAct=-1;
	
	
	public QNeuralNet() {
		brain=new Network(new int[]{TaskMaster.environment.getStateSize(),5,TaskMaster.environment.getPossibleActions()});
		double qLim=1/(1-DISCOUNT);
		double minQ=TaskMaster.environment.getRewardRange()[0]*qLim;
		double maxQ=TaskMaster.environment.getRewardRange()[1]*qLim;
		minVal=minQ;
		scale=maxQ-minQ;
	}
	
	@Override
	public int step(double[] state, double reward) {
		
		Qs=brainValToQVal(brain.feedForward(state).toArray());
		
		//Find best action for current state
		bestAct=getBestAction(state);
		
		//Update
		if(lastAct!=-1){
			brain.feedForward(lastState).getEntry(lastAct);//Restore brain
			double correctionQVal=oldQs[lastAct]+LEARNINGRATE*(reward+DISCOUNT*Qs[bestAct]-oldQs[lastAct]);
			System.out.println("["+oldQs[lastAct]+":"+correctionQVal+"]");
			double[] corrQs=oldQs;
			corrQs[lastAct]=correctionQVal;
			
			//System.out.println("["+(lastAction.qVal-correctionQVal)+"]");
			BackPropagationOutput bpO=brain.backpropagate(new ArrayRealVector(qValToBrainVal(corrQs)));
			brain.gradDescent(BRAINLEARNINGRATE, bpO.nablaBiases[0], bpO.nablaWeights[0]);
		}
		
		//Explore
		if(Math.random()<EPS){
			lastAct=(int)(TaskMaster.environment.getPossibleActions()*Math.random());
			
		}else{//Exploit
			lastAct=bestAct;
		}
		
		lastState=state;
		oldQs=Qs;
		return lastAct;
	}
	
	class Action{
		int id;
		double qVal;
		public Action(int id, double qVal) {
			this.id=id;
			this.qVal=qVal;
		}
	}
	
	double[] brainValToQVal(double[] b){
		double[] out=new double[b.length];
		for(int i=0; i<out.length; i++)out[i]=brainValToQVal(b[i]);
		return out;
	}
	
	double[] qValToBrainVal(double[] q){
		double[] out=new double[q.length];
		for(int i=0; i<out.length; i++)out[i]=qValToBrainVal(q[i]);
		return out;
	}
	
	double brainValToQVal(double b){
		return b*scale+minVal;
	}
	
	double qValToBrainVal(double q){
		return (q-minVal)/scale;
	}
	
	int getBestAction(double[] state){
		double bestAct=-Double.MAX_VALUE;
		int id=-1;
		for(int i=0; i<Qs.length; i++){
			if(Qs[i]>bestAct){
				bestAct=Qs[i];
				id=i;
			}else if(Qs[i]==bestAct&&Math.random()<0.5){//Maybe Baby
				bestAct=Qs[i];
				id=i;
			}
		}
		//for(Action a:actions)System.out.println(bestAct.qVal-a.qVal);
		return id;
	}
}
