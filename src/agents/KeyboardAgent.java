package agents;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;

import interfaces.Agent;

public class KeyboardAgent implements Agent{
	JFrame inputFrame;
	boolean[] keys;
	
	public KeyboardAgent() {
		keys=new boolean[6];
		inputFrame=new JFrame("InputFrame");
		Dimension size=new Dimension(300, 100);
		inputFrame.setSize(size.width, size.height);
		inputFrame.setResizable(false);
		inputFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		inputFrame.getContentPane().add(new JLabel("Input here"));
		inputFrame.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD0)keys[0]=false;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD1)keys[1]=false;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD2)keys[2]=false;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD3)keys[3]=false;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD4)keys[4]=false;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD5)keys[5]=false;
			}
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD0)keys[0]=true;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD1)keys[1]=true;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD2)keys[2]=true;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD3)keys[3]=true;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD4)keys[4]=true;
				if(e.getKeyCode()==KeyEvent.VK_NUMPAD5)keys[5]=true;
			}
			@Override
			public void keyTyped(KeyEvent e) {}
		});
		inputFrame.setVisible(true);
	}
	
	@Override
	public int step(double[] state, double reward, boolean terminal) {
		while(true){
			for(int i=0; i<keys.length; i++){
				if(keys[i])return i;
			}
		}
	}

}
