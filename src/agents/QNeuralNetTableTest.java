package agents;

import java.util.HashMap;

import core.TaskMaster;
import interfaces.Agent;

public class QNeuralNetTableTest implements Agent{
	final double LEARNINGRATE=1, DISCOUNT=0.5, EPS=0.01, BRAINLEARNINGRATE=1;
	HashMap<String, double[]> qTable=new HashMap<>();
	double minVal, scale;//(minQ,maxQ) zu (0,1) f�r NN
	
	String lastState;
	String stateKey;
	Action lastAction=null;
	
	
	@Override
	public int step(double[] state, double reward) {
		
		/*Debug out
		 * for(String s:qTable.keySet()){
		 * System.out.println(qTable.entrySet().size()*Main.environment.getPossibleActions());
			System.out.print(s+"->");
			for(double d:qTable.get(s))System.out.print(d+":");
			System.out.println();
		}*/
		
		stateKey="";
		for(double d:state)stateKey+=d;
		
		double[] qVals;
		if(qTable.containsKey(stateKey)){
			qVals=qTable.get(stateKey);
		}else{
			qTable.put(stateKey, new double[TaskMaster.environment.getPossibleActions()]);
			qVals=qTable.get(stateKey);
		}
		
		//Find best action for current state
		Action bestAction=getBestAction(stateKey);
		//Update
		if(lastAction!=null){
			double correctionQVal=lastAction.qVal+LEARNINGRATE*(reward+DISCOUNT*bestAction.qVal-lastAction.qVal);
			qTable.get(lastState)[lastAction.id]=correctionQVal;
		}
		
		//Explore
		if(Math.random()<EPS){
			int rand=(int)(TaskMaster.environment.getPossibleActions()*Math.random());
			lastAction=new Action(rand,qVals[rand]);
			lastState=stateKey;
			return lastAction.id;
		}
		
		//Exploit
		lastAction=bestAction;
		lastState=stateKey;
		return lastAction.id;
	}
	
	class Action{
		int id;
		double qVal;
		public Action(int id, double qVal) {
			this.id=id;
			this.qVal=qVal;
		}
	}
	
	double brainValToQVal(double b){
		return b*scale+minVal;
	}
	
	double qValToBrainVal(double q){
		return (q-minVal)/scale;
	}
	
	Action getBestAction(String stateKey){
		Action[] actions=new Action[TaskMaster.environment.getPossibleActions()];
		int c=0;
		for(double d:qTable.get(stateKey)){
			actions[c]=new Action(c, d);
			c++;
		}
		
		Action bestAct=new Action(-1, -Double.MAX_VALUE);
		for(int i=0; i<actions.length; i++){
			
			if(actions[i].qVal>bestAct.qVal){
				bestAct=actions[i];
			}else if(actions[i].qVal==bestAct.qVal&&Math.random()<0.5){//Maybe Baby
				bestAct=actions[i];
			}
		}
		//for(Action a:actions)System.out.println(bestAct.qVal-a.qVal);
		return bestAct;
	}
}
