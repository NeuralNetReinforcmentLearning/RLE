package agents;

import interfaces.Agent;

import java.util.HashMap;

import core.TaskMaster;

public class QTable implements Agent{
	final double LEARNINGRATE=0.8, DISCOUNT=0.6, EPS=0.11;
	HashMap<String, double[]> qTable=new HashMap<>();
	
	String lastState;
	int lastAction=-1;
	
	
	String stateKey;//TODO: Better Hashing Method
	
	int steps=0;
	
	double[] curState;
	public QTable() {
	}
	
	@Override
	public int step(double[] state, double reward, boolean terminal) {
		curState=state;
		stateKey="";
		for(double d:state)stateKey+=d;
		
		double[] qVals;
		if(qTable.containsKey(stateKey)){
			qVals=qTable.get(stateKey);
		}else{
			qTable.put(stateKey, new double[TaskMaster.environment.getPossibleActions()]);
			qVals=qTable.get(stateKey);
		}
		
		//for(double d:(double[])qTable.get(stateKey))System.out.println(d);
		
		
		/*Debug out
		 * for(String s:qTable.keySet()){
		 * System.out.println(qTable.entrySet().size()*TaskMaster.environment.getPossibleActions());
			System.out.print(s+"->");
			for(double d:qTable.get(s))System.out.print(d+":");
			System.out.println();
		}*/
		
		steps++;
		
		//Update
		if(lastAction!=-1){
			double oldVal=qTable.get(lastState)[lastAction];
			qTable.get(lastState)[lastAction]=oldVal+LEARNINGRATE*(reward+DISCOUNT*qVals[getBestAction(stateKey)]-oldVal);
		}
		
		//Explore
		if(Math.random()<EPS){
			lastState=stateKey;
			lastAction=(int)(TaskMaster.environment.getPossibleActions()*Math.random());
			return lastAction;
		}
		
		//Exploit
		lastState=stateKey;
		lastAction=getBestAction(stateKey);
		return lastAction;
	}
	
	int getBestAction(String state){
		double[] actions=qTable.get(state);
		double max=-Double.MAX_VALUE;
		int bestAct=-1;
		boolean random=false;
		for(int i=0; i<actions.length; i++){
			if(actions[i]>max){
				max=actions[i];
				bestAct=i;
				random=false;
			}else if(actions[i]==max&&Math.random()<0.5){//Maybe Baby
				max=actions[i];
				bestAct=i;
				random=true;
			}
		}
		/*if(steps>10000){
			if(steps==10001){
				System.out.println("Switch");
				FeedForwardNeuralNet.saveNet(new File("A:\\Schule\\Maturarbeit\\net.txt"), brain);
			}
			bestAct=brain.feedForward(curState).getMaxIndex();
		}
		else if(!random){
			double[] brans=brain.feedForward(curState).toArray();
			double[] ans=new double[TaskMaster.environment.getPossibleActions()];
			for(int i=0; i<ans.length; i++)if(i==bestAct)ans[i]=1;
			//for(int i=0; i<brans.length; i++)System.out.println(brans[i]+":"+ans[i]);
			brain.gradDescent(1, brain.backpropagate(new ArrayRealVector(ans)));
		}*/
		return bestAct;
	}

}
