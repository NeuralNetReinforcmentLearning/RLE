package agents;

import java.util.ArrayList;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import core.TaskMaster;
import gui.NNControl;
import interfaces.Agent;
import interfaces.BackPropagationOutput;

public class AdvancedQNeuralNet implements Agent{
	public static final int PLOT_AVERAGING=1000;
	final double LEARNINGRATE=1, DISCOUNT=0.5, EPS=0.1, BRAINLEARNINGRATE=1, SELECTION_RANDOMNESS=1;
	final int BATCHSIZE=64, REPLAYSIZE=3000;
	
	ParallelNetwork brain=new ParallelNetwork(TaskMaster.environment.getStateSize(),10,10,TaskMaster.environment.getPossibleActions());
	
	NNControl controlPanel=new NNControl(brain, REPLAYSIZE, 0, 2);
	
	ArrayList<Experience> experienceReplay=new ArrayList<>();
	
	int steps=0;
	
	Experience curExp;
	
	double minVal, scale;//QVal<->BrainVal conversion
	
	public AdvancedQNeuralNet() {
		double qLim=1/(1-DISCOUNT);
		double minQ=TaskMaster.environment.getRewardRange()[0]*qLim;
		double maxQ=TaskMaster.environment.getRewardRange()[1]*qLim;
		minVal=minQ;
		scale=maxQ-minQ;
	}
	
	@Override
	public int step(double[] state, double reward) {
		
		while(controlPanel.block){
			Thread.yield();
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(curExp!=null){
			curExp.afterState=state;
			curExp.reward=reward;
			
			//====================Train==========================
			Experience[] batch=new Experience[Math.min(BATCHSIZE, experienceReplay.size()+1)];
			//Select from expReplay with softmax
			double[] probs=new double[experienceReplay.size()];
			double expSum=0;
			for(int i=0; i<probs.length; i++)expSum+=experienceReplay.get(i).costExp;
			
			for(int i=0; i<probs.length; i++){
				probs[i]=Math.exp(experienceReplay.get(i).cost*SELECTION_RANDOMNESS)/expSum;
			}
			
			batch[0]=curExp;
			for(int i=1; i<batch.length; i++){
				double r=Math.random();
				double sum=0;
				int expIndx=-1;
				randPicker:for(int j=0; j<experienceReplay.size(); j++){
					sum+=probs[j];
					if(sum>=r){
						expIndx=j;
						break randPicker;
					}
				}
				batch[i]=experienceReplay.get(expIndx);
			}
//			if(experienceReplay.size()<REPLAYSIZE){
//				batch[0]=curExp;
//			}else batch[0]=experienceReplay.get((int)(Math.random()*experienceReplay.size()));
//			for(int i=1; i<batch.length; i++)batch[i]=experienceReplay.get((int)(Math.random()*experienceReplay.size()));
			
			//Calculate bestAction values for correctionQVals
			RealMatrix bestActInput=new Array2DRowRealMatrix(TaskMaster.environment.getStateSize(), batch.length);
			for(int i=0; i<batch.length; i++)bestActInput.setColumnVector(i, new ArrayRealVector(batch[i].afterState));
			
			RealMatrix bestActOutput=brain.feedForward(bestActInput);
			double[] bestQVals=new double[batch.length];
			for(int i=0; i<batch.length; i++){
				bestQVals[i]=brainValToQVal(bestActOutput.getColumnVector(i).getMaxValue());
			}
			
			//Calculate corrQVals
			RealMatrix preQInput=new Array2DRowRealMatrix(TaskMaster.environment.getStateSize(), batch.length);
			for(int i=0; i<batch.length; i++)preQInput.setColumnVector(i, new ArrayRealVector(batch[i].preState));
			RealMatrix preQOutput=brain.feedForward(preQInput);
			
			double[] corrQVals=new double[batch.length];
			for(int i=0; i<batch.length; i++){
				double oldVal=brainValToQVal(preQOutput.getEntry(batch[i].action, i));
				corrQVals[i]=oldVal+LEARNINGRATE*(batch[i].reward+DISCOUNT*bestQVals[i]-oldVal);
				
				double newCost=brain.calcCost(qValToBrainVal(oldVal), qValToBrainVal(corrQVals[i]));//Adjusting cost
				batch[i].cost=newCost;
				batch[i].costExp=Math.exp(newCost*SELECTION_RANDOMNESS);
				//System.out.println(newCost);
				//if(i==0).System.out.println(newCost);
			}
			
			//Backpropagate
			RealMatrix bpInput=preQOutput.copy();
			for(int i=0; i<batch.length; i++){
				bpInput.setEntry(batch[i].action, i, qValToBrainVal(corrQVals[i]));
			}
			BackPropagationOutput bpOutput=brain.backpropagate(bpInput);
			
			//Gradient descent
			RealVector[] biasGradients=bpOutput.nablaBiases[0];
			RealMatrix[] weightGradients=bpOutput.nablaWeights[0];
			for(int i=1; i<batch.length; i++){//Adding batchgradients together
				for(int l=0; l<biasGradients.length; l++){
					biasGradients[l].add(bpOutput.nablaBiases[i][l]);
					weightGradients[l].add(bpOutput.nablaWeights[i][l]);
				}
			}
			for(int l=0; l<biasGradients.length; l++){//Averaging
				biasGradients[l]=biasGradients[l].mapMultiply(1d/BATCHSIZE);
				weightGradients[l]=weightGradients[l].scalarMultiply(1d/BATCHSIZE);
			}
			
			brain.gradDescent(BRAINLEARNINGRATE, biasGradients, weightGradients);
			
			//Add curExp
			/*if(experienceReplay.size()<REPLAYSIZE){
				experienceReplay.add(curExp);
			}*/
			double costSum=curExp.cost, rewardSum=curExp.reward;
			int samples=Math.min(experienceReplay.size(), PLOT_AVERAGING);
			for(int i=experienceReplay.size()-1; i>=experienceReplay.size()-samples; i--){
				costSum+=experienceReplay.get(i).cost;
				rewardSum+=experienceReplay.get(i).reward;
			}
			curExp.costAvg=costSum/(samples+1);
			curExp.rewardAvg=rewardSum/(samples+1);
			
			experienceReplay.add(curExp);
			if(experienceReplay.size()>REPLAYSIZE){
				experienceReplay.remove(0);
			}
			
			if(controlPanel.plot()){
				double[] plotPoints=new double[experienceReplay.size()];
				for(int i=0; i<plotPoints.length; i++){
					if(controlPanel.modeChooser.getSelectedIndex()==0){
						plotPoints[i]=experienceReplay.get(i).cost;
					}else if(controlPanel.modeChooser.getSelectedIndex()==1){
						plotPoints[i]=experienceReplay.get(i).costAvg;
					}else if(controlPanel.modeChooser.getSelectedIndex()==2){
						plotPoints[i]=experienceReplay.get(i).rewardAvg;
					}
				}
				controlPanel.setPlotArray(plotPoints);
			}
		}
		
		//New curExp
		curExp=new Experience();
		curExp.preState=state;
		//Select action
		RealMatrix ffO=brain.feedForward(state);
		//System.out.println(ffO);
		if(Math.random()<EPS){//Explore
			curExp.action=selectRandomLegalAction();
		}else{//Exploit
			curExp.action=selectBestLegalAction(ffO.getColumnVector(0));
		}
		
		steps++;
		
		return curExp.action;
	}
	
	int selectRandomLegalAction(){
		int selectedAction=-1;
		while(selectedAction==-1){
			int rAction=(int)(TaskMaster.environment.getPossibleActions()*Math.random());
			if(TaskMaster.environment.isLegalAction(rAction))selectedAction=rAction;
		}
		return selectedAction;
	}
	
	int selectBestLegalAction(RealVector qVals){
		RealVector qValsCopy=qVals.copy();
		int selectedAction=-1;
		while(selectedAction==-1){
			if(qValsCopy.getMaxValue()==-Double.MAX_VALUE)break;
			int maxAc=qValsCopy.getMaxIndex();
			if(TaskMaster.environment.isLegalAction(maxAc))selectedAction=maxAc;
			else qValsCopy.setEntry(maxAc, -Double.MAX_VALUE);
		}
		return selectedAction;
	}
	
	double[] brainValToQVal(double[] b){
		double[] out=new double[b.length];
		for(int i=0; i<out.length; i++)out[i]=brainValToQVal(b[i]);
		return out;
	}

	double[] qValToBrainVal(double[] q){
		double[] out=new double[q.length];
		for(int i=0; i<out.length; i++)out[i]=qValToBrainVal(q[i]);
		return out;
	}

	double brainValToQVal(double b){
		//return b*scale+minVal;
		return b;
	}

	double qValToBrainVal(double q){
		//return (q-minVal)/scale;
		return q;
	}
}


class Experience{
	double[] preState;
	double[] afterState;
	int action;
	double reward;
	double rewardAvg;
	double cost;
	double costExp;
	double costAvg;
}