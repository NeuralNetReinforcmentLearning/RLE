package environments;

import java.awt.Color;
import java.awt.Graphics;

import core.Main;
import interfaces.Environment;

public class DodgeEnvironment implements Environment{

	class Player{
		double x,y;
		double height=4,width=4;
		Player(){
			x=Math.random()*(100-width);
			y=Math.random()*(100-height);
		}
		
		boolean collidesWith(Apple a){
			if(isInside(a.x, a.y)||isInside(a.x+a.width, a.y)||isInside(a.x, a.y+a.height)||isInside(a.x+a.width, a.y+a.height))return true;
			return false;
		}
		
		boolean isInside(double px, double py){
			if(px>x&&px<x+width&&py>y&&py<y+height)return true;
			return false;
		}
	}
	
	class Apple{
		double x,y;
		double height=3, width=3;
		public Apple() {
			x=Math.random()*(100-width);
			y=Math.random()*(100-height);
		}
	}
	
	Apple[] apls;
	Player pl;
	
	double lastReward;
	
	public DodgeEnvironment() {
		pl=new Player();
		apls=new Apple[10];
		for(int i=0; i<apls.length; i++)apls[i]=new Apple();
	}
	
	@Override
	public void update(int action) {
		if(action==0);//Nothing
		else if(action==1){
			pl.y--;//Up
			if(pl.y<0)pl.y=100-pl.y;
		}
		else if(action==2){
			pl.x++;//Right
			if(pl.x>100)pl.x=pl.x-100;
		}
		else if(action==3){
			pl.y++;//Down
			if(pl.y>100)pl.y=pl.y-100;
		}
		else if(action==4){
			pl.x--;//Left
			if(pl.x<0)pl.x=100-pl.x;
		}
		lastReward=0;
		for(int i=0; i<apls.length; i++){
			if(pl.collidesWith(apls[i])){
				lastReward++;
				apls[i]=new Apple();
			}
		}
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		double[] state=new double[2+apls.length*2];
		state[0]=pl.x;
		state[1]=pl.y;
		for(int i=0; i<apls.length; i++){
			state[(i+1)*2]=apls[i].x;
			state[(i+1)*2+1]=apls[i].y;
		}
		return state;
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.blue);
		g.fillRect(Main.panel.percToX(pl.x), Main.panel.percToY(pl.y), Main.panel.percToX(pl.width), Main.panel.percToY(pl.height));
		g.setColor(Color.green);
		for(int i=0; i<apls.length; i++)g.fillRect(Main.panel.percToX(apls[i].x), Main.panel.percToY(apls[i].y), Main.panel.percToX(apls[i].width), Main.panel.percToY(apls[i].height));
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 5;
	}

	@Override
	public int getStateSize() {
		return 2+apls.length*2;
	}

	@Override
	public double[] getRewardRange() {
		return new double[]{0,1};
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}

}
