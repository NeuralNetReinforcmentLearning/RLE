package environments;

import java.awt.Color;
import java.awt.Graphics;

import interfaces.Environment;

public class AdditionEnvironment implements Environment{

	int a, b;
	double lastReward=0;
	
	public AdditionEnvironment() {
		setABT();
	}
	
	void setABT(){
		a=(int)(Math.random()*20);
		b=(int)(Math.random()*20);
	}
	
	@Override
	public void update(int action) {
		//HH88 lololol//
		if(action==a+b)lastReward=1;
		else lastReward=0;
		setABT();
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		return new double[]{a,b};
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		int val=a+b;
		g.drawString(a+" + "+b+" = "+val, 10, 10);
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 38;
	}

	@Override
	public int getStateSize() {
		return 2;
	}
	
	@Override
	public double[] getRewardRange() {
		return new double[]{0,1};
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}
}
