package environments;

import java.awt.Color;
import java.awt.Graphics;

import core.Main;
import interfaces.Environment;

public class MoveEnvironment implements Environment{

	class Player{
		double x,y;
		double height=4,width=4;
		Player(int x, int y) {
			this.x=x;
			this.y=y;
		}
		
		double distLeft(){
			return x-(walls[0].x+walls[0].width);
		}
		
		double distRight(){
			return walls[1].x-(x+width);
		}
	}	
	
	class Wall{
		double x,y;
		double height=100, width=100;
		public Wall(int x, int y) {
			this.x=x;
			this.y=y;
		}
		
		boolean collidesWith(Player p){
			if(isInside(p.x, p.y)||isInside(p.x+p.width, p.y)||isInside(p.x, p.y+p.height)||isInside(p.x+p.width, p.y+p.height))return true;
			return false;
		}
		
		boolean isInside(double px, double py){
			if(px>x&&px<x+width&&py>y&&py<y+height)return true;
			return false;
		}
	}
	
	Wall[] walls;
	Player pl;
	
	int step=20;
	
	double lastReward;
	
	public MoveEnvironment() {
		pl=new Player(50,50);
		walls=new Wall[2];
		walls[0]=new Wall(-80, 0);
		walls[1]=new Wall(60,0);
	}
	
	@Override
	public void update(int action) {
		if(action==0);//Nothing
		else if(action==1){
			pl.y--;//Up
			if(pl.y<0)pl.y=0;//pl.y=100-pl.y;
		}
		else if(action==2){
			pl.x++;//Right
			if(pl.x+pl.width>100)pl.x=100-pl.width;//pl.x=pl.x-100;
		}
		else if(action==3){
			pl.y++;//Down
			if(pl.y+pl.height>100)pl.y=100-pl.height;//pl.y=pl.y-100;
		}
		else if(action==4){
			pl.x--;//Left
			if(pl.x<0)pl.x=0;//pl.x=100-pl.x;
		}
		lastReward=0;
		for(int i=0; i<walls.length; i++){
			if(step%80>=40){
				walls[i].x--;
			}else{
				walls[i].x++;
			}
			if(walls[i].collidesWith(pl)){
				lastReward--;
			}
		}
		step++;
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		double[] state=new double[2];
		state[0]=pl.distLeft();
		state[1]=pl.distRight();
		return state;
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.blue);
		g.fillRect(Main.panel.percToX(pl.x), Main.panel.percToY(pl.y), Main.panel.percToX(pl.width), Main.panel.percToY(pl.height));
		g.setColor(Color.black);
		for(int i=0; i<walls.length; i++)g.drawRect(Main.panel.percToX(walls[i].x), Main.panel.percToY(walls[i].y), Main.panel.percToX(walls[i].width), Main.panel.percToY(walls[i].height));
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 5;
	}

	@Override
	public int getStateSize() {
		return 2;
	}

	@Override
	public double[] getRewardRange() {
		return new double[]{0,-1};
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}

}
