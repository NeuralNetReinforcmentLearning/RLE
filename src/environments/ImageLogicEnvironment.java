package environments;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import interfaces.Environment;
import interfaces.ImageEnvironment;

public class ImageLogicEnvironment implements Environment, ImageEnvironment{
	
	public static final int OUTPUT_SIDELENGHT=32, OUTPUT_SIZE=OUTPUT_SIDELENGHT*OUTPUT_SIDELENGHT, COLOR_CHANNELS=1;

	int a, b, t;
	double lastReward=0;
	
	public ImageLogicEnvironment() {
		setABT();
	}
	
	void setABT(){
		a=(int)(Math.random()*2);
		b=(int)(Math.random()*2);
		t=(int)(Math.random()*3);
	}
	
	@Override
	public void update(int action) {
		if((t==0)&&(action==1)==(and(a,b)==1))lastReward=1;//AND
		else if((t==1)&&(action==1)==(or(a,b)==1))lastReward=1;//OR
		else if((t==2)&&(action==1)==(xor(a,b)==1))lastReward=1;//XOR
		else lastReward=0;
		setABT();
	}
	
	int and(int a, int b){
		return (a==1&&b==1)?1:0;
	}
	
	int or(int a, int b){
		return (a==1||b==1)?1:0;
	}
	
	int xor(int a, int b){
		return (a==1||b==1)&&!(a==1&&b==1)?1:0;
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		BufferedImage img=new BufferedImage(OUTPUT_SIDELENGHT, OUTPUT_SIDELENGHT, BufferedImage.TYPE_INT_RGB);
		Graphics2D g=img.createGraphics();
		g.setFont(new Font("Arial", Font.PLAIN, 10));
		g.drawString(a+" "+getTString(t)+" "+b, 1, 20);
		g.dispose();
		double[] state=new double[getStateSize()];
		for(int i=0; i<OUTPUT_SIZE; i++){
			int x=i%OUTPUT_SIDELENGHT, y=i/OUTPUT_SIDELENGHT;
			Color c=new Color(img.getRGB(x, y));
			state[i]=c.getRed()/255d;//Img is either black or white so we can just take red/green/blue values
		}
		return state;
	}
	
	String getTString(int t){
		if(t==0)return "and";
		if(t==1)return "or";
		if(t==2)return "xor";
		else return "fail";
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		String sign = null;
		int val=0;
		if(t==0){
			sign="AND";
			val=and(a,b);
		}
		else if(t==1){
			sign="OR";
			val=or(a,b);
		}
		else if(t==2){
			sign="XOR";
			val=xor(a,b);
		}
		g.setFont(new Font("Arial", Font.PLAIN, 50));
		g.drawString(a+" "+sign+" "+b+" = "+val, 70, 70);
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 2;
	}

	@Override
	public int getStateSize() {
		return OUTPUT_SIZE*COLOR_CHANNELS;
	}
	
	@Override
	public double[] getRewardRange() {
		return new double[]{0,1};
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public int getOutputSidelenght() {
		return OUTPUT_SIDELENGHT;
	}

	@Override
	public int getOutputSize() {
		return OUTPUT_SIZE;
	}

	@Override
	public int getOutputChannels() {
		return COLOR_CHANNELS;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}
}
