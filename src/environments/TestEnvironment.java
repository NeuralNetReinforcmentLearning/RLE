package environments;

import java.awt.Graphics;

import interfaces.Environment;

public class TestEnvironment implements Environment{
	int key=0;
	double reward=-1;
	
	@Override
	public void update(int action) {
		reward=action==key?1:-1;
		key=(int) (Math.random()*3);
	}

	@Override
	public double getLastReward() {
		return reward;
	}

	@Override
	public double[] getCurrentState() {
		return new double[]{key};
	}

	@Override
	public void draw(Graphics g) {
	}

	@Override
	public boolean isDrawable() {
		return false;
	}

	@Override
	public int getPossibleActions() {
		return 3;
	}

	@Override
	public int getStateSize() {
		return 1;
	}

	@Override
	public double[] getRewardRange() {
		return new double[]{-1,1};
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}

}
