package environments;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import bridges.NESBridge;
import interfaces.Destructable;
import interfaces.Environment;
import interfaces.ImageEnvironment;
import utils.Util;

public class NESEnvironment implements Environment, ImageEnvironment, Destructable{

	double lastReward;
	BufferedImage stateImg;
	double[] state;
	
	public static final int TRIM_SIDELENGHT=224, OUTPUT_SIDELENGHT=64, OUTPUT_SIZE=OUTPUT_SIDELENGHT*OUTPUT_SIDELENGHT, COLOR_CHANNELS=3;
	public static final boolean GRAYSCALE=true, EASY_MODE=true, EMU_AUTOSTART=true;
	
	public NESEnvironment() {
		if(EMU_AUTOSTART)NESBridge.restartEmulator();
		NESBridge.initialize();
		updateState();
	}
	
	@Override
	public void update(int action) {
		if(EASY_MODE)action=transformEasyAction(action);
		NESBridge.writeAction(action);//Starts NES Cycle
		lastReward=NESBridge.readLastReward();//Blocks until NES Cycle is complete
		updateState();//Gets&tranforms Screenshot
	}
	
	int transformEasyAction(int action){
		if(action==0)return 0;//Nothing
		if(action==1)return 1;//Right
		if(action==2)return 2;//Left
		if(action==3)return 33;//RightJump
		if(action==4)return 34;//LeftJump
		System.out.println("Invalid action");
		return -1;
	}
	
	public void updateState(){
		stateImg=transformState(NESBridge.getScreen());
		state=Util.imgToRGBArray(stateImg);
		if(GRAYSCALE)state=Util.colorToGrayscale(state);
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}
	
	BufferedImage transformState(BufferedImage in){
		BufferedImage newImg=new BufferedImage(TRIM_SIDELENGHT, TRIM_SIDELENGHT, BufferedImage.TYPE_INT_RGB);
		Graphics g=newImg.getGraphics();
		g.drawImage(in, 0, 0, null);
		g.dispose();
		Image tmpImg=newImg.getScaledInstance(OUTPUT_SIDELENGHT, OUTPUT_SIDELENGHT, BufferedImage.SCALE_AREA_AVERAGING);
		BufferedImage end=new BufferedImage(OUTPUT_SIDELENGHT, OUTPUT_SIDELENGHT, BufferedImage.TYPE_INT_RGB);
		Graphics g2=end.getGraphics();
		g2.drawImage(tmpImg, 0, 0, null);
		g2.dispose();
		return end;
	}
	
	@Override
	public double[] getCurrentState() {
		return state;
	}

	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public void draw(Graphics g) {
		BufferedImage bigStateImg;
		if(GRAYSCALE){
			BufferedImage grayImg=new BufferedImage(OUTPUT_SIDELENGHT, OUTPUT_SIDELENGHT, BufferedImage.TYPE_INT_RGB);
			int[] gImgData=((DataBufferInt)grayImg.getRaster().getDataBuffer()).getData();
			int[] stateInt=new int[state.length];
			for(int i=0; i<state.length; i++)stateInt[i]=new Color((int)(state[i]*256), (int)(state[i]*256), (int)(state[i]*256)).getRGB();
			System.arraycopy(stateInt, 0, gImgData, 0, state.length);
			bigStateImg=Util.scale(grayImg, 10, 10);
		}else{
			bigStateImg=Util.scale(stateImg, 10, 10);
		}
		g.drawImage(bigStateImg, 0, 0, null);
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		if (EASY_MODE) return 5;
		else return 64;
	}

	@Override
	public int getStateSize() {
		return OUTPUT_SIDELENGHT*OUTPUT_SIDELENGHT*getOutputChannels();
	}

	@Override
	public double[] getRewardRange() {
		return null;
	}

	@Override
	public void destroy() {
		NESBridge.close();
	}

	@Override
	public int getOutputSidelenght() {
		return OUTPUT_SIDELENGHT;
	}

	@Override
	public int getOutputSize() {
		return OUTPUT_SIZE;
	}

	@Override
	public int getOutputChannels() {
		if(GRAYSCALE)return 1;
		else return COLOR_CHANNELS;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}

}
