package environments;

import java.awt.Color;
import java.awt.Graphics;

import core.Main;
import interfaces.Environment;

public class BoxEnvironment implements Environment{
	class Box{
		public Box(double posX, double posY, double width, double height) {
			this.posX=posX; this.posY=posY; this.width=width; this.height=height;
		}
		double posX, posY, width, height;
	}
	
	Box box; //Boxbox FTW!
	
	public BoxEnvironment() {
		box=new Box(5, 5, 5, 5);
	}
	
	@Override
	public void update(int action) {
		if(action>=8){
			action-=8;
			if(box.posX+box.width<100)box.posX++;
		}
		if(action>=4){
			action-=4;
			if(box.posY+box.height<100)box.posY++;
		}
		if(action>=2){
			action-=2;
			if(box.posX>0)box.posX--;
		}
		if(action>=1){
			action-=1;
			if(box.posY>0)box.posY--;
		}
	}

	@Override
	public double getLastReward() {
		return 0;
	}

	@Override
	public double[] getCurrentState() {
		return new double[]{box.posX, box.posY, box.width, box.height};
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.blue);
		g.drawLine((int)Main.panel.percToX(box.posX), (int)Main.panel.percToY(box.posY), (int)Main.panel.percToX(box.posX+box.width), (int)Main.panel.percToY(box.posY));
		g.drawLine((int)Main.panel.percToX(box.posX), (int)Main.panel.percToY(box.posY), (int)Main.panel.percToX(box.posX), (int)Main.panel.percToY(box.posY+box.height));
		g.drawLine((int)Main.panel.percToX(box.posX+box.width), (int)Main.panel.percToY(box.posY), (int)Main.panel.percToX(box.posX+box.width), (int)Main.panel.percToY(box.posY+box.height));
		g.drawLine((int)Main.panel.percToX(box.posX), (int)Main.panel.percToY(box.posY+box.height), (int)Main.panel.percToX(box.posX+box.width), (int)Main.panel.percToY(box.posY+box.height));
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 16;
	}

	@Override
	public int getStateSize() {
		return 4;
	}

	@Override
	public double[] getRewardRange() {
		return null;
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}

}
