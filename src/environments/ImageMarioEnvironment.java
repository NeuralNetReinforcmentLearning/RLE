package environments;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import bridges.ImageMarioNESBridge;
import interfaces.Destructable;
import interfaces.Environment;
import utils.Util;

public class ImageMarioEnvironment implements Environment, Destructable{
	double lastReward;
	BufferedImage stateImg;
	double[] state;
	
	public static final int TRIM_SIDELENGHT=224, OUTPUT_SIDELENGHT=64, COLOR_CHANNELS=3, OUTPUT_SIZE=OUTPUT_SIDELENGHT*OUTPUT_SIDELENGHT*COLOR_CHANNELS;
	public static final boolean GRAYSCALE=false;
	
	public ImageMarioEnvironment() {
		ImageMarioNESBridge.initialize();
		updateState();
	}
	
	@Override
	public void update(int action) {
		action=transformEasyAction(action);
		ImageMarioNESBridge.writeAction(action);//Starts NES Cycle
		lastReward=ImageMarioNESBridge.readLastReward();//Blocks until NES Cycle is complete
		updateState();//Gets&tranforms Screenshot	}
	}
	
	public void updateState(){
		stateImg=transformState(ImageMarioNESBridge.getScreen());
		state=Util.imgToRGBArray(stateImg);
		if(GRAYSCALE)state=Util.colorToGrayscale(state);
	}
	
	BufferedImage transformState(BufferedImage in){
		BufferedImage newImg=new BufferedImage(TRIM_SIDELENGHT, TRIM_SIDELENGHT, BufferedImage.TYPE_INT_RGB);
		Graphics g=newImg.getGraphics();
		g.drawImage(in, 0, 0, null);
		g.dispose();
		Image tmpImg=newImg.getScaledInstance(OUTPUT_SIDELENGHT, OUTPUT_SIDELENGHT, BufferedImage.SCALE_AREA_AVERAGING);
		BufferedImage end=new BufferedImage(OUTPUT_SIDELENGHT, OUTPUT_SIDELENGHT, BufferedImage.TYPE_INT_RGB);
		Graphics g2=end.getGraphics();
		g2.drawImage(tmpImg, 0, 0, null);
		g2.dispose();
		return end;
	}
	
	boolean jumped=false;
	int transformEasyAction(int action){
		int ac=0;
		if(action==0)ac=0;//Nothing
		else if(action==1)ac=1;//Right
		else if(action==2)ac=2;//Left
		else if(action==3||action==4){
			if(jumped&&onGround()){
				if(action==3)ac=1;
				else if(action==4)ac=2;
			}else{
				if(action==3)ac=33;
				if(action==4)ac=34;
			}
		}else{
			System.out.println("Invalid action");
		}
		jumped=ac>32;
		return ac;
	}

	boolean onGround(){
		return state[110]==1;
	}
	
	@Override
	public double getLastReward() {
		return lastReward;
	}
	
	@Override
	public double[] getCurrentState() {
		return state;
	}

	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public void draw(Graphics g) {
		BufferedImage bigStateImg;
		if(GRAYSCALE){
			BufferedImage grayImg=new BufferedImage(OUTPUT_SIDELENGHT, OUTPUT_SIDELENGHT, BufferedImage.TYPE_INT_RGB);
			int[] gImgData=((DataBufferInt)grayImg.getRaster().getDataBuffer()).getData();
			int[] stateInt=new int[state.length];
			for(int i=0; i<state.length; i++)stateInt[i]=new Color((int)(state[i]*256), (int)(state[i]*256), (int)(state[i]*256)).getRGB();
			System.arraycopy(stateInt, 0, gImgData, 0, state.length);
			bigStateImg=Util.scale(grayImg, 10, 10);
		}else{
			bigStateImg=Util.scale(stateImg, 5, 5);
		}
		g.drawImage(bigStateImg, 0, 0, null);
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 5;
	}

	@Override
	public int getStateSize() {
		return OUTPUT_SIZE;
	}

	@Override
	public double[] getRewardRange() {
		return null;
	}

	@Override
	public void destroy() {
		ImageMarioNESBridge.close();
	}

	@Override
	public boolean isTerminal() {
		return ImageMarioNESBridge.isTerminal();
	}
}
