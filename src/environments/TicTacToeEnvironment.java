package environments;

import java.awt.Graphics;

import core.Main;
import interfaces.Environment;

public class TicTacToeEnvironment implements Environment{

	int[] board;//0=Empty, 1=Circle=Agent, 2=Square=K.I.
	double lastReward=0;
	boolean terminal=false;
	
	public TicTacToeEnvironment() {
		board=new int[9];
	}
	
	@Override
	public void update(int action) {
		terminal=false;
		lastReward=0;
		if(action!=-1){
			if(isLegalMove(action)){
				board[action]=1;
			}else{
				lastReward=-1;
				return;
			}
		}else return;
		int ww=whoWon();
		if(ww!=0){
			if(ww==1)lastReward=10;
			else if(ww==2){
				lastReward=-10;
				terminal=true;
			}
			board=new int[board.length];
		}
		move();
		ww=whoWon();
		if(ww!=0){
			if(ww==1)lastReward=10;
			else if(ww==2){
				lastReward=-10;
				terminal=true;
			}
			board=new int[board.length];
		}
		lastReward*=0.1;
	}
	
	@Override
	public boolean isTerminal() {
		return terminal;
	}

	boolean isLegalMove(int move){
		if(move>=0&&move<board.length){
			return board[move]==0;
		}else return false;
	}
	
	void move(){
		int move=-1;
		while(!isLegalMove(move)){
			move=(int)(Math.random()*board.length);
		}
		board[move]=2;
	}
	
	int whoWon(){
		if(board[0]==board[1]&&board[1]==board[2]){
			return board[0];
		}else if(board[0]==board[3]&&board[3]==board[6]){
			return board[0];
		}else if(board[0]==board[4]&&board[4]==board[8]){
			return board[0];
		}else if(board[2]==board[5]&&board[5]==board[8]){
			return board[2];
		}else if(board[6]==board[7]&&board[7]==board[8]){
			return board[6];
		}else if(board[2]==board[4]&&board[4]==board[6]){
			return board[2];
		}else if(board[1]==board[4]&&board[4]==board[7]){
			return board[1];
		}else if(board[3]==board[4]&&board[4]==board[5]){
			return board[3];
		}else{
			boolean full=true;
			for(int i:board)if(i==0)full=false;
			if(full)return -1;
			else return 0;
		}
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return isLegalMove(action);
	}
	
	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		double[] state=new double[board.length];
		for(int i=0; i<state.length; i++)state[i]=board[i];
		return state;
	}

	@Override
	public void draw(Graphics g) {
		g.drawLine(Main.panel.percToX(0), Main.panel.percToY(33), Main.panel.percToX(100), Main.panel.percToY(33));
		g.drawLine(Main.panel.percToX(0), Main.panel.percToY(66), Main.panel.percToX(100), Main.panel.percToY(66));
		g.drawLine(Main.panel.percToX(33), Main.panel.percToY(0), Main.panel.percToX(33), Main.panel.percToY(100));
		g.drawLine(Main.panel.percToX(66), Main.panel.percToY(0), Main.panel.percToX(66), Main.panel.percToY(100));
		for(int i=0; i<board.length; i++){
			int x=i%3;
			int y=(int)i/3;
			if(board[i]==1)g.fillOval(Main.panel.percToX(x*33+1), Main.panel.percToY(y*33+1), Main.panel.percToX(30), Main.panel.percToY(30));
			else if(board[i]==2)g.fillRect(Main.panel.percToX(x*33+1), Main.panel.percToY(y*33+1), Main.panel.percToX(30), Main.panel.percToY(30));
		}
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 9;
	}

	@Override
	public int getStateSize() {
		return 9;
	}

	@Override
	public double[] getRewardRange() {
		return new double[]{-10,10};
	}

}
