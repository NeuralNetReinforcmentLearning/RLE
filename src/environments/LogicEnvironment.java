package environments;

import java.awt.Color;
import java.awt.Graphics;

import interfaces.Environment;

public class LogicEnvironment implements Environment{

	int a, b, t;
	double lastReward=0;
	
	public LogicEnvironment() {
		setABT();
	}
	
	void setABT(){
		a=(int)(Math.random()*2);
		b=(int)(Math.random()*2);
		t=(int)(Math.random()*3);
	}
	
	@Override
	public void update(int action) {
		if((t==0)&&action==and(a,b))lastReward=1;//AND
		else if((t==1)&&action==or(a,b))lastReward=1;//OR
		else if((t==2)&&action==xor(a,b))lastReward=1;//XOR
		else lastReward=0;
		setABT();
	}
	
	int and(int a, int b){
		return (a==1&&b==1)?1:0;
	}
	
	int or(int a, int b){
		return (a==1||b==1)?1:0;
	}
	
	int xor(int a, int b){
		return (a==1||b==1)&&!(a==1&&b==1)?1:0;
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		return new double[]{a,b,t};
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		String sign = null;
		int val=0;;
		if(t==0){
			sign="AND";
			val=and(a,b);
		}
		else if(t==1){
			sign="OR";
			val=or(a,b);
		}
		else if(t==2){
			sign="XOR";
			val=xor(a,b);
		}
		g.drawString(a+" "+sign+" "+b+" = "+val, 10, 10);
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 2;
	}

	@Override
	public int getStateSize() {
		return 3;
	}
	
	@Override
	public double[] getRewardRange() {
		return new double[]{0,1};
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}
}
