package environments;

import java.awt.Graphics;

import interfaces.Environment;

public class RandomReward implements Environment{

	int s=0;
	double lastReward=0;
	
	@Override
	public void update(int action) {
		s++;
		if(s==10){
			s=0;
			lastReward=1;
		}else{
			lastReward=0;
		}
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		return new double[]{s};
	}

	@Override
	public void draw(Graphics g) {
	}

	@Override
	public boolean isDrawable() {
		return false;
	}
	
	@Override
	public int getStateSize() {
		return 1;
	}

	@Override
	public int getPossibleActions() {
		return 1;
	}

	@Override
	public double[] getRewardRange() {
		return null;
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}

}
