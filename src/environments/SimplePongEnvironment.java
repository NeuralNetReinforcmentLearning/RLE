package environments;

import java.awt.Color;
import java.awt.Graphics;

import core.Main;
import interfaces.Environment;

public class SimplePongEnvironment implements Environment{

	class Ball{
		int xPos, yPos;
	}
	
	class Player{
		int yPos;
		final int size=1;
	}
	
	Ball ball;
	Player player;
	
	double lastReward=0;
	boolean terminal=false;
	
	public SimplePongEnvironment() {
		spawnBall();
		player=new Player();
		player.yPos=5;
	}
	
	@Override
	public void update(int action) {
		terminal=false;
		if(action==1&&player.yPos>0)player.yPos--;
		if(action==2&&player.yPos+player.size<10)player.yPos++;
		ball.xPos--;
		if(ball.xPos<=0){
			if(player.yPos+player.size>ball.yPos&&player.yPos<=ball.yPos){
				lastReward=1;
			}else{
				lastReward=0;
				terminal=true;
			}
			spawnBall();
			return;
		}
		lastReward=0.5;
	}
	
	void spawnBall(){
		ball=new Ball();
		ball.xPos=13;
		ball.yPos=(int)(Math.random()*10);
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		return new double[]{player.yPos/9d, ball.xPos/19d, ball.yPos/10d};
		//return new double[]{player.yPos, ball.xPos, ball.yPos};
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, Main.panel.percToY(player.yPos*10), Main.panel.percToX(5), Main.panel.percToY(player.size*10));
		g.fillRect(Main.panel.percToX(ball.xPos*10), Main.panel.percToY(ball.yPos*10), Main.panel.percToX(5), Main.panel.percToY(5));
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 3;
	}

	@Override
	public int getStateSize() {
		return 3;
	}

	@Override
	public double[] getRewardRange() {
		return new double[]{-1,1};
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public boolean isTerminal() {
		return terminal;
	}

}
