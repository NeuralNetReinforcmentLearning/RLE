package environments;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import core.Main;
import interfaces.Environment;
import interfaces.ImageEnvironment;
import utils.Util;

public class ImagePongEnvironment implements Environment, ImageEnvironment{

	public static final int OUTPUT_SIDELENGHT=32, OUTPUT_SIZE=OUTPUT_SIDELENGHT*OUTPUT_SIDELENGHT, COLOR_CHANNELS=1;

	class Ball{
		int xPos, yPos;
	}
	
	class Player{
		int yPos;
		final int size=1;
	}
	
	Ball ball;
	Player player;
	
	double lastReward=0;
	
	public ImagePongEnvironment() {
		spawnBall();
		player=new Player();
		player.yPos=5;
	}
	
	@Override
	public void update(int action) {
		if(action==1&&player.yPos>0)player.yPos--;
		if(action==2&&player.yPos+player.size<10)player.yPos++;
		ball.xPos--;
		if(ball.xPos<=0){
			if(player.yPos+player.size>ball.yPos&&player.yPos<=ball.yPos){
				lastReward=2;
			}else{
				lastReward=0;
			}
			spawnBall();
			return;
		}
		lastReward=1;
	}
	
	void spawnBall(){
		ball=new Ball();
		ball.xPos=13;
		ball.yPos=(int)(Math.random()*10);
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		BufferedImage img=new BufferedImage(OUTPUT_SIDELENGHT, OUTPUT_SIDELENGHT, BufferedImage.TYPE_INT_RGB);
		Graphics2D g=img.createGraphics();
		g.setColor(Color.white);
		g.fillRect(0, percToY(player.yPos*10), percToX(5), percToY(player.size*10));
		g.fillRect(percToX(ball.xPos*10), percToY(ball.yPos*10), percToX(5), percToY(5));
		g.dispose();
		/*try {
			ImageIO.write(img, "png", new File("img.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		return Util.colorToGrayscale(Util.imgToRGBArray(img));
		//return new double[]{player.yPos, ball.xPos, ball.yPos};
	}
	
	public int percToX(double x){
		return (int) (x*(OUTPUT_SIDELENGHT/100d));
	}
	
	public int percToY(double y){
		return (int) (y*(OUTPUT_SIDELENGHT/100d));
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, Main.panel.percToY(player.yPos*10), Main.panel.percToX(5), Main.panel.percToY(player.size*10));
		g.fillRect(Main.panel.percToX(ball.xPos*10), Main.panel.percToY(ball.yPos*10), Main.panel.percToX(5), Main.panel.percToY(5));
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 3;
	}

	@Override
	public int getStateSize() {
		return OUTPUT_SIZE*COLOR_CHANNELS;
	}

	@Override
	public double[] getRewardRange() {
		return new double[]{-1,1};
	}
	
	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public int getOutputSidelenght() {
		return OUTPUT_SIDELENGHT;
	}

	@Override
	public int getOutputSize() {
		return OUTPUT_SIZE;
	}

	@Override
	public int getOutputChannels() {
		return COLOR_CHANNELS;
	}

	@Override
	public boolean isTerminal() {
		return false;
	}

}
