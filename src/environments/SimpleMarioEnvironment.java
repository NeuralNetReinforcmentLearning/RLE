package environments;

import interfaces.Destructable;
import interfaces.Environment;

import java.awt.Color;
import java.awt.Graphics;

import bridges.MarioNESBridge;

public class SimpleMarioEnvironment implements Environment, Destructable{
	double lastReward;
	double[] state=new double[OUTPUT_SIZE];
	
	public static final int OUTPUT_SIDELENGHT=13, OUTPUT_SIZE=OUTPUT_SIDELENGHT*OUTPUT_SIDELENGHT;
	
	public SimpleMarioEnvironment() {
		MarioNESBridge.initialize();
	}
	
	@Override
	public void update(int action) {
		action=transformEasyAction(action);
		MarioNESBridge.writeAction(action);//Starts NES Cycle
		MarioNESBridge.readUpdate();//Blocks until NES Cycle is complete
		state=MarioNESBridge.state;
		lastReward=MarioNESBridge.lastReward;
	}
	
	boolean jumped=false;
	int transformEasyAction(int action){
		int ac=0;
		if(action==0)ac=0;//Nothing
		else if(action==1)ac=1;//Right
		else if(action==2)ac=2;//Left
		else if(action==3||action==4){
			if(jumped&&onGround()){
				if(action==3)ac=1;
				else if(action==4)ac=2;
			}else{
				if(action==3)ac=33;
				if(action==4)ac=34;
			}
		}else{
			System.out.println("Invalid action");
		}
		jumped=ac>32;
		return ac;
	}

	boolean onGround(){
		return state[110]==1;
	}
	
	@Override
	public double getLastReward() {
		return lastReward;
	}
	
	@Override
	public double[] getCurrentState() {
		return state;
	}

	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	@Override
	public void draw(Graphics g) {
		for(int y=0; y<OUTPUT_SIDELENGHT; y++){
			for(int x=0; x<OUTPUT_SIDELENGHT; x++){
				double t=state[y*OUTPUT_SIDELENGHT+x];
				if(t==0)g.setColor(Color.CYAN);
				else if(t==1)g.setColor(Color.GRAY);
				else if(t==-1)g.setColor(Color.RED);
				//if(y*OUTPUT_SIDELENGHT+x==8*13+6)g.setColor(Color.MAGENTA);
				g.fillRect(x*10, y*10, 10, 10);
			}
		}
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 5;
	}

	@Override
	public int getStateSize() {
		return OUTPUT_SIZE;
	}

	@Override
	public double[] getRewardRange() {
		return null;
	}

	@Override
	public void destroy() {
		MarioNESBridge.close();
	}

	@Override
	public boolean isTerminal() {
		return MarioNESBridge.isTerminal();
	}
}
