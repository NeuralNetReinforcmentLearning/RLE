package environments;

import java.awt.Color;
import java.awt.Graphics;

import interfaces.Environment;
import utils.Util;

public class SnakeEnvironment implements Environment{

	public static final int FIELD_SIZE=10;
	
	double lastReward;
	boolean terminal;
	int[][] state;//0:air, 1:snake_head, 2:snake_body, 3:candy
	
	int cX, cY;
	
	static class Part{
		int x, y;
		Part after;
		public Part(int x, int y, Part after) {
			this.x=x;
			this.y=y;
			this.after=after;
		}
	}
	
	Part head;
	
	public SnakeEnvironment() {
		init();
	}
	
	void init(){
		state=new int[FIELD_SIZE][FIELD_SIZE];
		head=new Part(FIELD_SIZE/2, FIELD_SIZE/2, null);
		state[FIELD_SIZE/2][FIELD_SIZE/2]=1;
		spawnCandy();
	}
	
	void spawnCandy(){
		while(true){
			int nX=(int)(Util.r.nextDouble()*FIELD_SIZE);
			int nY=(int)(Util.r.nextDouble()*FIELD_SIZE);
			if(state[nY][nX]==0){
				cX=nX;
				cY=nY;
				state[nY][nX]=3;
				return;
			}
		}
	}
	
	//0:Up, 1:Right, 2:Down, 3:Left
	@Override
	public void update(int action) {
		lastReward=0;
		terminal=false;
		int dx=0;
		int dy=0;
		if(action==0)dy=-1;
		else if(action==1)dx=1;
		else if(action==2)dy=1;
		else if(action==3)dx=-1;
		int nX=head.x+dx;
		int nY=head.y+dy;
		if(inBounds(nX, nY)&&state[nY][nX]!=2){
			int bState=state[nY][nX];
			Part nHead=new Part(nX, nY, head);
			state[nY][nX]=1;
			state[head.y][head.x]=2;
			head=nHead;
			if(bState==3){
				lastReward=1;
				spawnCandy();
			}else{
				Part cPart=nHead;
				while(cPart.after.after!=null){
					cPart=cPart.after;
				}
				state[cPart.after.y][cPart.after.x]=0;
				cPart.after=null;
			}
		}else{
			die();
		}
	}
	
	void die(){
		lastReward=-1;
		terminal=true;
		init();
	}
	
	boolean inBounds(int x, int y){
		if(x>=0&&x<FIELD_SIZE&&y>=0&&y<FIELD_SIZE)return true;
		else return false;
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		double[] outState=new double[FIELD_SIZE*FIELD_SIZE];
		for(int y=0; y<FIELD_SIZE; y++){
			for(int x=0; x<FIELD_SIZE; x++){
				outState[y*FIELD_SIZE+x]=state[y][x];
			}
		}
		return outState;
	}

	@Override
	public boolean isTerminal() {
		return terminal;
	}

	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	static final int CELL_SIZE=15; 
	@Override
	public void draw(Graphics g) {
		for(int y=0; y<FIELD_SIZE; y++){
			for(int x=0; x<FIELD_SIZE; x++){
				if(state[y][x]==0)g.setColor(Color.WHITE);
				else if(state[y][x]==1)g.setColor(Color.RED);
				else if(state[y][x]==2)g.setColor(Color.ORANGE);
				else if(state[y][x]==3)g.setColor(Color.GREEN);
				g.fillRect(x*CELL_SIZE, y*CELL_SIZE, CELL_SIZE, CELL_SIZE);
			}
		}
		g.setColor(Color.BLACK);
		g.drawLine(0, FIELD_SIZE*CELL_SIZE, FIELD_SIZE*CELL_SIZE, FIELD_SIZE*CELL_SIZE);
		g.drawLine(FIELD_SIZE*CELL_SIZE, 0, FIELD_SIZE*CELL_SIZE, FIELD_SIZE*CELL_SIZE);
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 4;
	}

	@Override
	public int getStateSize() {
		return FIELD_SIZE*FIELD_SIZE;
	}

	@Override
	public double[] getRewardRange() {
		return new double[]{-1,1};
	}

}
