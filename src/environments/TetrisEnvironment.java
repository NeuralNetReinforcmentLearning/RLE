package environments;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import interfaces.Environment;

public class TetrisEnvironment implements Environment{
	static final int WIDTH=8, HEIGHT=24;
	
	public TetrisEnvironment() {
		spawnNewTile();
	}
	
	double lastReward;
	boolean terminal;
	int[][] state=new int[HEIGHT][WIDTH];//0: Empty, 1:Solid, 2:Falling
	Point middle;
	
	@Override
	public void update(int action) {//0:Nothing, 1:Move Left, 2: Move Right, 3: Rotate CCW, 4: Rotate CW
		lastReward=0;
		terminal=false;
		
		move:if(action!=0){
			int[][] newState=new int[HEIGHT][WIDTH];
			if(action==1){
				for(int y=0; y<HEIGHT; y++){
					for(int x=0; x<WIDTH; x++){
						if(state[y][x]==1)newState[y][x]=1;
						else if(state[y][x]==2){
							if(x==0||state[y][x-1]==1){
								break move;
							}else{
								newState[y][x-1]=2;
							}
						}
					}
				}
				middle=new Point(middle.x-1, middle.y);
			}else if(action==2){
				for(int y=0; y<HEIGHT; y++){
					for(int x=0; x<WIDTH; x++){
						if(state[y][x]==1)newState[y][x]=1;
						else if(state[y][x]==2){
							if(x==WIDTH-1||state[y][x+1]==1){
								break move;
							}else{
								newState[y][x+1]=2;
							}
						}
					}
				}
				middle=new Point(middle.x+1, middle.y);
			}else if(action==3){
				for(int y=0; y<HEIGHT; y++){
					for(int x=0; x<WIDTH; x++){
						if(state[y][x]==1)newState[y][x]=1;
						else if(state[y][x]==2){
							int dx=x-middle.x, dy=y-middle.y;
							int nx=middle.x-dy, ny=middle.y+dx;
							if(empty(nx, ny)){
								newState[ny][nx]=2;
							}else{
								break move;
							}
						}
					}
				}
			}else if(action==4){
				for(int y=0; y<HEIGHT; y++){
					for(int x=0; x<WIDTH; x++){
						if(state[y][x]==1)newState[y][x]=1;
						else if(state[y][x]==2){
							int dx=x-middle.x, dy=y-middle.y;
							int nx=middle.x+dy, ny=middle.y-dx;
							if(empty(nx, ny)){
								newState[ny][nx]=2;
							}else{
								break move;
							}
						}
					}
				}
			}
			state=newState;
		}
		
		int[][] newState=new int[HEIGHT][WIDTH];
		boolean lock=false;
		outerCopy:{
			for(int y=0; y<HEIGHT; y++){
				for(int x=0; x<WIDTH; x++){
					if(state[y][x]==1)newState[y][x]=1;
					else if(state[y][x]==2){
						if(y==0||state[y-1][x]==1){
							lock=true;
							break outerCopy;
						}else{
							newState[y][x]=0;
							newState[y-1][x]=2;
						}
					}
				}
			}
			state=newState;
			middle=new Point(middle.x, middle.y-1);
		}
		lastReward=0.2;
		if(lock){
			newState=new int[HEIGHT][WIDTH];
			for(int y=0; y<HEIGHT; y++){
				for(int x=0; x<WIDTH; x++){
					if(state[y][x]>0)newState[y][x]=1;
				}
			}
			state=newState;
			if(isGameOver())restart();
			checkFullLine();
			spawnNewTile();
		}
	}
	
	boolean empty(int x, int y){
		if(x>0 && x<WIDTH && y>0 && y<HEIGHT){
			return state[y][x]!=1;
		}
		return false;
	}
	
	boolean isGameOver(){
		for(int x=0; x<WIDTH; x++)if(state[HEIGHT-1][x]==1)return true;
		return false;
	}
	
	void checkFullLine(){
		outer:for(int y=0; y<HEIGHT; y++){
			for(int x=0; x<WIDTH; x++){
				if(state[y][x]!=1)continue outer;
			}
			vaporizeLine(y);
			//lastReward+=1;
		}
	}
	
	void vaporizeLine(int line){
		int[][] newState=new int[HEIGHT][WIDTH];
		for(int y=0; y<HEIGHT; y++){
			if(y!=line){
				for(int x=0; x<WIDTH; x++){
					if(y<line){
						newState[y][x]=state[y][x];
					}else{
						newState[y-1][x]=state[y][x];
					}
				}
			}
		}
		state=newState;
	}
	
	void restart(){
		lastReward=-4;
		terminal=true;
		state=new int[HEIGHT][WIDTH];
	}

	@Override
	public double getLastReward() {
		return lastReward;
	}

	@Override
	public double[] getCurrentState() {
		double[] out=new double[HEIGHT*WIDTH];
		for(int i=0; i<out.length; i++){
			out[i]=state[i/WIDTH][i%WIDTH];
		}
		return out;
	}

	@Override
	public boolean isTerminal() {
		return terminal;
	}

	@Override
	public boolean isLegalAction(int action) {
		return true;
	}

	final static int BLOCK_SIZE=10;
	@Override
	public void draw(Graphics g) {
		for(int y=0; y<HEIGHT; y++){
			for(int x=0; x<WIDTH; x++){
				if(y==HEIGHT-1){
					g.setColor(Color.RED);
					g.fillRect(x*BLOCK_SIZE, HEIGHT*BLOCK_SIZE-y*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
				}
				if(state[y][x]>0){
					if(state[y][x]==1)g.setColor(Color.BLUE);
					else if(state[y][x]==2)g.setColor(Color.MAGENTA);
					g.fillRect(x*BLOCK_SIZE, HEIGHT*BLOCK_SIZE-y*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
				}
			}
		}
		g.setColor(Color.BLACK);
		g.drawLine(0, (HEIGHT+1)*BLOCK_SIZE, WIDTH*BLOCK_SIZE, (HEIGHT+1)*BLOCK_SIZE);
		g.drawLine(WIDTH*BLOCK_SIZE, (HEIGHT+1)*BLOCK_SIZE, WIDTH*BLOCK_SIZE, 0);
	}
	
	void spawnNewTile(){
		int tileType=(int)(Math.random()*7);
		tileType=0;
		if(tileType==0){//Line
			state[HEIGHT-1][WIDTH/2-2]=2;
			state[HEIGHT-1][WIDTH/2-1]=2;
			state[HEIGHT-1][WIDTH/2]=2;
			state[HEIGHT-1][WIDTH/2+1]=2;
			middle=new Point(WIDTH/2-1, HEIGHT-1);
		}else if(tileType==1){//LLeft
			state[HEIGHT-1][WIDTH/2-1]=2;
			state[HEIGHT-2][WIDTH/2-1]=2;
			state[HEIGHT-2][WIDTH/2]=2;
			state[HEIGHT-2][WIDTH/2+1]=2;
			middle=new Point(WIDTH/2, HEIGHT-2);
		}else if(tileType==2){//LRight
			state[HEIGHT-2][WIDTH/2-1]=2;
			state[HEIGHT-2][WIDTH/2]=2;
			state[HEIGHT-2][WIDTH/2+1]=2;
			state[HEIGHT-1][WIDTH/2+1]=2;
			middle=new Point(WIDTH/2, HEIGHT-2);
		}else if(tileType==3){//Block
			state[HEIGHT-1][WIDTH/2-1]=2;
			state[HEIGHT-1][WIDTH/2]=2;
			state[HEIGHT-2][WIDTH/2-1]=2;
			state[HEIGHT-2][WIDTH/2]=2;
			middle=new Point(WIDTH/2-1, HEIGHT-1);
		}else if(tileType==4){//SwiggleRight
			state[HEIGHT-2][WIDTH/2-1]=2;
			state[HEIGHT-2][WIDTH/2]=2;
			state[HEIGHT-1][WIDTH/2]=2;
			state[HEIGHT-1][WIDTH/2+1]=2;
			middle=new Point(WIDTH/2, HEIGHT-1);
		}else if(tileType==5){//T
			state[HEIGHT-1][WIDTH/2]=2;
			state[HEIGHT-2][WIDTH/2]=2;
			state[HEIGHT-2][WIDTH/2-1]=2;
			state[HEIGHT-2][WIDTH/2+1]=2;
			middle=new Point(WIDTH/2, HEIGHT-2);
		}else if(tileType==6){//SwiggleLeft
			state[HEIGHT-1][WIDTH/2-1]=2;
			state[HEIGHT-1][WIDTH/2]=2;
			state[HEIGHT-2][WIDTH/2]=2;
			state[HEIGHT-2][WIDTH/2+1]=2;
			middle=new Point(WIDTH/2, HEIGHT-1);
		}
	}

	@Override
	public boolean isDrawable() {
		return true;
	}

	@Override
	public int getPossibleActions() {
		return 5;
	}

	@Override
	public int getStateSize() {
		return WIDTH*HEIGHT;
	}

	@Override
	public double[] getRewardRange() {
		return new double[]{-4,4};
	}
	
}
