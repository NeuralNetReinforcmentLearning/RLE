package interfaces;

import java.awt.Graphics;

public interface Environment {
	void update(int action);
	double getLastReward();
	double[] getCurrentState();
	boolean isTerminal();
	
	boolean isLegalAction(int action);
	
	void draw(Graphics g);
	boolean isDrawable();
	
	int getPossibleActions();
	int getStateSize();
	double[] getRewardRange();
}
