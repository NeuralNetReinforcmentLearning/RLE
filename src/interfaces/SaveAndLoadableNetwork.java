package interfaces;

import java.io.File;

public interface SaveAndLoadableNetwork {
	public void loadNet(File f);
	public void saveNet(File f);
}
