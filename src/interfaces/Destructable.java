package interfaces;

public interface Destructable {
	public void destroy();
}
