package interfaces;

public interface StringSaveAndLoadable {
	public abstract String getSaveString();
	public abstract void constructFromSaveString(String str);
}
