package interfaces;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public abstract class FeedForwardNeuralNet implements SaveAndLoadableNetwork{
	volatile protected RealVector[] biases; //z->N->a
	volatile protected RealMatrix[] weights;
	
	public int[] layerSizes;
	
	public FeedForwardNeuralNet(int[] layerSizes){
		this.layerSizes=layerSizes;
		initNet(layerSizes);
	}
	
	protected abstract void initNet(int[] layerSizes);
	
	public abstract double cost(RealVector answer);
	
	/*public abstract BackPropagationOutput backpropagate(RealVector answer);
	
	public abstract void gradDescent(double learningRate, RealVector[] nablaBiases, RealMatrix[] nablaWeights);*/
	
	public synchronized void setWeights(RealMatrix[] weights){
		this.weights=weights;
	}
	
	public synchronized void setBiases(RealVector[] biases){
		this.biases=biases;
	}
	
	public synchronized RealMatrix[] getWeights(){
		return weights;
	}
	
	public synchronized RealVector[] getBiases(){
		return biases;
	}
	
	public void loadNet(File f){
		Scanner fin=null;
		try {
			fin=new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if(fin.hasNextLine()){
			String structure=fin.nextLine();
			structure=structure.substring(1, structure.length()-1);
			String[] sizes=structure.split(",");
			int[] layerSizes=new int[sizes.length];
			for(int i=0; i<sizes.length; i++)layerSizes[i]=Integer.parseInt(sizes[i]);
			
			for(int i=0; fin.hasNextLine(); i++){
				String biasString=fin.nextLine();
				String weightString=fin.nextLine();
				
				biasString=biasString.substring(1, biasString.length()-1);
				String[] biasSplit=biasString.split(",");
				double[] biases=new double[biasSplit.length];
				for(int j=0; j<biases.length; j++)biases[j]=Double.parseDouble(biasSplit[j]);
				this.biases[i]=new ArrayRealVector(biases);
				
				weightString=weightString.substring(2, weightString.length()-2);
				String[] weightRowSplit=weightString.split("\\},\\{");
				String[][] weightSplit=new String[weightRowSplit.length][];
				for(int j=0; j<weightRowSplit.length; j++)weightSplit[j]=weightRowSplit[j].split(",");
				double[][] weights=new double[weightSplit.length][weightSplit[0].length];
				for(int j=0; j<weightSplit.length; j++){
					for(int k=0; k<weightSplit[j].length; k++){
						weights[j][k]=Double.parseDouble(weightSplit[j][k]);
					}
				}
				this.weights[i]=new Array2DRowRealMatrix(weights);
			}
			fin.close();
		}else{
			fin.close();
		}
	}
	
	
	public void saveNet(File f){
		String out="";
		out+="{";
		for(int i=0; i<layerSizes.length; i++){
			if(i<layerSizes.length-1)out+=layerSizes[i]+",";
			else out+=layerSizes[i]+"}\n";
		}
		
		for(int i=0; i<layerSizes.length-1; i++){
			out+="{";
			for(int j=0; j<biases[i].getDimension(); j++){
				if(j<biases[i].getDimension()-1)out+=biases[i].getEntry(j)+",";
				else out+=biases[i].getEntry(j)+"}\n";
			}
			out+="{";
			for(int j=0; j<weights[i].getRowDimension(); j++){
				out+="{";
				for(int k=0; k<weights[i].getColumnDimension(); k++){
					if(k<weights[i].getColumnDimension()-1)out+=weights[i].getEntry(j,k)+",";
					else out+=weights[i].getEntry(j,k)+"}";
				}
				if(j<weights[i].getRowDimension()-1)out+=",";
				else out+="}\n";
			}
		}
		
		try {
			FileWriter outWriter=new FileWriter(f);
			outWriter.write(out);
			outWriter.close();
			System.out.println("Network saved");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
