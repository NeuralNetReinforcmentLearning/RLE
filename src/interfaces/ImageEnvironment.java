package interfaces;

public interface ImageEnvironment {
	public int getOutputSidelenght();
	public int getOutputSize();
	public int getOutputChannels();
}
