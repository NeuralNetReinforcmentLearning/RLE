package interfaces;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class BackPropagationOutput{
	public RealVector[][] nablaBiases;
	public RealMatrix[][] nablaWeights;
	public BackPropagationOutput(RealVector[][] nablaBiases, RealMatrix[][] nablaWeights) {
		this.nablaBiases=nablaBiases;
		this.nablaWeights=nablaWeights;
	}
}
