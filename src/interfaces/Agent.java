package interfaces;

public interface Agent {
	int step(double[] state, double reward, boolean terminal);
}
