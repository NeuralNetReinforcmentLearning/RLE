package gui;

import interfaces.SaveAndLoadableNetwork;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;

import networks.ModularNetwork;
import core.Main;
import core.TaskMaster;

public class NNControl extends JFrame{
	private static final long serialVersionUID = 1L;
	public double[] plotPoints;
	public double scaleX, yMin, yMax, scaleY;
	public int xMax;
	Dimension graphSize;//=new Dimension(500, 500);
	public CostGraphPanel graphPanel;
	JPanel contentPane;
	
	QValDisplay qValDisp;
	
	public boolean block;
	
	public static final File NETSAVEDIR=new File("A:\\Schule\\Maturarbeit\\nets");
	
	JFileChooser chooser;
	
	SaveAndLoadableNetwork net;
	
	public JCheckBox chckbxPlotCost;
	public JComboBox<String> modeChooser;
	
	public NNControl(SaveAndLoadableNetwork net, int xMax, double yMin, double yMax){
		this.net=net;
		this.xMax=xMax;
		this.yMin=yMin;
		this.yMax=yMax;
		plotPoints=new double[(int)xMax];
		setTitle("NN ControlPanel");
		
		chooser=new JFileChooser(NETSAVEDIR);
		chooser.setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return "Neural Network Savefiles";
			}
			
			@Override
			public boolean accept(File f) {
				if(f.isDirectory()) return true;
				String[] name=f.getName().split("\\.");
				if(name.length>1){
					return name[name.length-1].equals("brain");
				}else return false;
			}
		});
		
		setIconImage(Main.icon);
		contentPane=new JPanel();
		Dimension cpSize=new Dimension(600, 600);
		contentPane.setPreferredSize(cpSize);
		getContentPane().add(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		graphPanel=new CostGraphPanel();
		contentPane.add(graphPanel, BorderLayout.CENTER);
		
		qValDisp=new QValDisplay();
		contentPane.add(qValDisp, BorderLayout.EAST); 
		
		graphSize=graphPanel.getSize();
		adjustScales();
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(block) btnStop.setText("Stop");
				else btnStop.setText("Start");
				block=!block;
			}
		});
		
		JButton btnBrainsight = new JButton("BraInsight");
		btnBrainsight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(net instanceof ModularNetwork){
					new BraInsight((ModularNetwork)net, NNControl.this);
				}else{
					JOptionPane.showMessageDialog(NNControl.this,
						    "Action not avilable for this type of Network",
						    "Action not avilable",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel.add(btnBrainsight);
		panel.add(btnStop);
		
		JButton btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chooser.setDialogTitle("Load Neural Net");
				if(chooser.showOpenDialog(NNControl.this)==JFileChooser.APPROVE_OPTION){
					net.loadNet(chooser.getSelectedFile());
					System.out.println(chooser.getSelectedFile().getName()+" loaded");
				}
			}
		});
		panel.add(btnLoad);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chooser.setDialogTitle("Save Neural Net");
				if(chooser.showSaveDialog(NNControl.this)==JFileChooser.APPROVE_OPTION){
					File saveFile=chooser.getSelectedFile();
					if(saveFile.getName().indexOf(".")==-1)saveFile=new File(saveFile.getAbsolutePath()+".brain");
					net.saveNet(saveFile);
					System.out.println(saveFile.getName()+" saved");
				}
			}
		});
		panel.add(btnSave);
		
		chckbxPlotCost = new JCheckBox("Plot", false);
		panel.add(chckbxPlotCost);
		
		modeChooser = new JComboBox<>();
		modeChooser.setModel(new DefaultComboBoxModel<String>(new String[] {"Absolute Cost", "Average Cost History", "Average Reward History"}));
		panel.add(modeChooser);
		
		pack();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				graphSize=graphPanel.getSize();
				adjustScales();
				qValDisp.adjustScales();
			}
		});
	}
	
	public boolean plot(){
		return chckbxPlotCost.isSelected();
	}
	
	public void setQVals(double[] actionVals){
		qValDisp.qVals=actionVals;
		qValDisp.adjustScales();
	}
	
	public void setPlotArray(double[] plotArray){
		this.plotPoints=plotArray;
		repaint();
	}
	
	public void addPlotPoint(double entry){
		double[] newGraph;
		if(plotPoints.length==xMax){
			newGraph=new double[xMax];
			for(int i=1; i<xMax; i++)newGraph[i-1]=plotPoints[i];
			newGraph[xMax-1]=entry;
		}else{
			newGraph=new double[plotPoints.length+1];
			for(int i=0; i<plotPoints.length; i++)newGraph[i]=plotPoints[i];
			newGraph[newGraph.length-1]=entry;
		}
		plotPoints=newGraph;
		repaint();
	}
	
	void adjustScales(){
		scaleX=xMax/(double)graphSize.width;
		scaleY=Math.abs(yMax-yMin)/graphSize.height;
	}
	
	public void changexMax(int xMax){
		this.xMax=xMax;
		scaleX=xMax/(double)graphSize.width;
	}
	
	class CostGraphPanel extends JPanel{
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			for(int i=0; i<=xMax; i+=100){
				int absX=(int)(i/scaleX);
				g.drawLine(absX, graphSize.height, absX, graphSize.height-3);
			}
			for(double i=yMin; i<yMax; i+=0.25){
				int absY=(int)(graphSize.height-(i)/scaleY);
				g.drawLine(0, absY, 3, absY);
			}
			
			if(modeChooser.getSelectedIndex()==0)g.setColor(Color.BLACK);
			else if(modeChooser.getSelectedIndex()==1)g.setColor(Color.BLUE);
			else if(modeChooser.getSelectedIndex()==2)g.setColor(Color.MAGENTA);
			double lastX=0,lastY=0;
			for(int x=0; x<plotPoints.length; x++){
				double absX=x/scaleX;
				double absY=graphSize.height-((plotPoints[x])/scaleY);
				if(modeChooser.getSelectedIndex()==0){
					g.drawLine((int)absX, graphSize.height-1, (int)absX, (int)(absY));//Columns
				}else{
					if(x!=0)g.drawLine((int)lastX, (int)lastY, (int)absX, (int)absY);//Line / Curve
				}
				lastX=absX;
				lastY=absY;
			}
			
		}
		public CostGraphPanel() {
			setBackground(Color.WHITE);
		}
	}
	class QValDisplay extends JPanel{
		double[] qVals=new double[TaskMaster.environment.getPossibleActions()];
		public static final int QBAR_WIDTH=13;
		double maxVal, minVal, toYScale, toYOffset;
		public final Font VAL_FONT=new Font("Arial", Font.PLAIN, 9);
		public final Font IDX_FONT=new Font("Arial", Font.PLAIN, 11);
		public final Color VAL_COLOR=new Color(51, 153, 255);
		
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			for(int i=0; i<qVals.length; i++){
				int xOffset=i*QBAR_WIDTH;
				int height=qValToY(qVals[i]);
				g.setColor(getColorForAction(i));
				g.drawRect(xOffset, getHeight(), QBAR_WIDTH, -height);
				g.setColor(Color.BLACK);
				g.setFont(IDX_FONT);
				g.drawString(""+i, xOffset+3, getHeight()-5);
				g.setFont(VAL_FONT);
				g.setColor(VAL_COLOR);
				g.drawString(""+Math.round(qVals[i]*10)/10d, xOffset, Math.min(getHeight()-30, Math.max(getHeight()-height, 10)));
			}
			g.setColor(Color.BLACK);
			g.drawLine(0, 0, 0, getHeight());
		}
		
		Color getColorForAction(int act){
			return new Color((act*75)%256, 100+(act*100)%156, 100+(act*50)%156);
		}
		
		int qValToY(double qVal){
			return (int)((qVal+toYOffset)*toYScale);
		}
		
		void adjustScales(){
			maxVal=-Double.MAX_VALUE;
			minVal=Double.MAX_VALUE;
			for(double d:qVals){
				if(d>maxVal)maxVal=d;
				if(d<minVal)minVal=d;
			}
			toYScale=getHeight()/(maxVal-minVal);
			toYOffset=-minVal;
		}
		
		Dimension minSize=new Dimension(QBAR_WIDTH*qVals.length, 100);
		public QValDisplay() {
			//setBackground(Color.WHITE);
			setMinimumSize(minSize);
			setPreferredSize(minSize);
		}
	}
}
