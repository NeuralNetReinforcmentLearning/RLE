package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.linear.RealMatrix;

import utils.Util;
import layers.TrainableLayer;
import networks.ModularNetwork;
import core.Main;
import core.TaskMaster;

public class BraInsight extends JFrame{
	ModularNetwork net;
	NNControl brainControler;
	Dimension defaultSize=new Dimension(500,500);
	
	Point oldOrig=new Point(-1, -1);
	
	ArrayList<Node> nodes=new ArrayList<>();
	ArrayList<Connection> connections=new ArrayList<>();
	
	boolean open=true;
	
	public static final int UPS=10;
	
	String overrideString="";
	
	Runnable stepwiseUpdate=new Runnable() {
		@Override
		public void run() {
			try{
				readValuesFromNet(net);
			}catch(Exception e){
				System.err.println("Concerr in BraInsight");
			}
		}
	};
	
	final static int initOffsetX=100, initOffsetY=300;
	
	int offsetX=initOffsetX, offsetY=initOffsetY;
	double scale=1, sExp=0;
	public BraInsight(ModularNetwork net, NNControl brainControler) {
		this.net=net;
		this.brainControler=brainControler;
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("BraInsight");
		setIconImage(Main.icon);
		setLocationRelativeTo(brainControler);
		add(new Canvas());
		KeyAdapter kHandler=new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_SPACE){
					reset();
					BraInsight.this.repaint();
				}else if(Character.isDigit(e.getKeyChar())||e.getKeyChar()=='.'||e.getKeyChar()=='-'||e.getKeyCode()==KeyEvent.VK_BACK_SPACE){
					ArrayList<Node> sInputs=new ArrayList<>();
					for(Node n:nodes)if(n.focused&&n.startNode)sInputs.add(n);
					if(sInputs.size()>0){
						if(e.getKeyCode()==KeyEvent.VK_BACK_SPACE){
							if(overrideString.length()>1)overrideString=overrideString.substring(0, overrideString.length()-1);
							else overrideString="0";
						}else{
							if(overrideString.equals("0"))overrideString="";
							overrideString+=e.getKeyChar();
						}
						try{
							double val=Double.parseDouble(overrideString);
							RealMatrix craftedInput=net.layers[0].in;
							for(Node n:sInputs){
								craftedInput.setEntry(0, n.idx, val);
							}
							net.feedForward(craftedInput);
							readValuesFromNet(net);
						}catch(NumberFormatException ex){}
					}
				}
			}
		};
		addKeyListener(kHandler);
		setSize(defaultSize);
		
		constructNetRepresentation();
		readValuesFromNet(net);
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(open){
					long startT=System.currentTimeMillis();
					//readValuesFromNet();
					repaint();
					Util.safeSleep(startT+1000/UPS-System.currentTimeMillis());
				}
			}
		}).start();
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				TaskMaster.removeStepwiseTask(stepwiseUpdate);
				open=false;
			}
		});
		
		TaskMaster.addStepwiseTask(stepwiseUpdate);
		
		setVisible(true);
	}
	
	void readValuesFromNet(ModularNetwork net){
		for(int l=0; l<net.layers.length; l++){
			if(net.layers[l] instanceof TrainableLayer){
				TrainableLayer cLayer=(TrainableLayer)net.layers[l];
				for(int ti=0; ti<cLayer.weights.getRowDimension(); ti++){
					Node to=null;
					for(Node n:nodes){
						if(n.layer==l+1&&n.idx==ti)to=n;
					}
					if(cLayer.out!=null)to.inp=cLayer.out.getEntry(ti, 0);
					to.bias=cLayer.biases.getEntry(ti);
					for(int fi=0; fi<cLayer.weights.getColumnDimension(); fi++){
						Node from=null;
						for(Node n:nodes){
							if(n.layer==l-1&&n.idx==fi)from=n;
						}
						if(cLayer.in!=null){
							if(l==0){
								int x=fi%cLayer.inCD, y=fi/cLayer.inCD;
								from.oup=cLayer.in.getEntry(y, x);
							}else{
								from.oup=cLayer.in.getEntry(fi, 0);
							}
						}
						for(Connection c:connections){
							if(c.from==from && c.to==to){
								c.weight=cLayer.weights.getEntry(ti, fi);
							}
						}
					}
				}
			}
		}
	}
	
	void constructNetRepresentation(){
		int nYOffset=-net.layers[0].inRD*net.layers[0].inCD*40/2;
		for(int i=0; i<net.layers[0].inRD*net.layers[0].inCD; i++){//Input Layer
			nodes.add(new Node(-1, i, -50, i*40+nYOffset, 0, true));
		}
		
		nYOffset=-net.layers[net.layers.length-1].getOutRowDimension()*40/2;
		for(int i=0; i<net.layers[net.layers.length-1].getOutRowDimension(); i++){//Output Layer
			nodes.add(new Node(net.layers.length, i, net.layers.length*50, i*40+nYOffset, 0));
		}
		for(int l=0; l<net.layers.length; l++){
			if(!(net.layers[l] instanceof TrainableLayer)){
				nYOffset=-net.layers[l].inRD*40/2;
				for(int i=0; i<net.layers[l].inRD; i++){
					nodes.add(new Node(l, i, l*50, i*40+nYOffset, 0));
				}
			}
		}
		for(int l=0; l<net.layers.length; l++){
			if(net.layers[l] instanceof TrainableLayer){
				TrainableLayer cLayer=(TrainableLayer)net.layers[l];
				for(int ti=0; ti<cLayer.weights.getRowDimension(); ti++){
					Node to=null;
					for(Node n:nodes){
						if(n.layer==l+1&&n.idx==ti)to=n;
					}
					to.bias=cLayer.biases.getEntry(ti);
					for(int fi=0; fi<cLayer.weights.getColumnDimension(); fi++){
						Node from=null;
						for(Node n:nodes){
							if(n.layer==l-1&&n.idx==fi)from=n;
						}
						//System.out.println(from+":"+(l-1)+":"+fi+"|"+to+":"+(l+1)+":"+ti);
						connections.add(new Connection(from, to, cLayer.weights.getEntry(ti, fi)));
					}
				}
			}
		}
	}
	
	void updateScale(){
		scale=Math.exp(sExp*0.1);
	}
	
	void reset(){
		offsetX=initOffsetX;
		offsetY=initOffsetY;
		sExp=0;
		updateScale();
		nodes=new ArrayList<>();
		connections=new ArrayList<>();
		constructNetRepresentation();
		readValuesFromNet(net);
	}
	
	public void render(Graphics g){
		Graphics2D g2d=(Graphics2D)g;
		AffineTransform trans=new AffineTransform();
		trans.translate(offsetX, offsetY);
		trans.scale(scale, scale);
		g2d.transform(trans);
		RenderingHints rh = new RenderingHints(
	             RenderingHints.KEY_ANTIALIASING,
	             RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setRenderingHints(rh);
		
	    for(Connection c:connections)c.render(g2d);
		for(Node n:nodes)n.render(g2d);
		
		//g2d.scale(1/scale, 1/scale);
		
		for(Node n:nodes){
			if(n.focused){
				n.renderDetails(g2d);
			}
		}
	}
	
	abstract class GuiObject{
		boolean focused=false;
		
		abstract boolean isInside(Vector2D pos);
		
		void setFocused(boolean focused){
			this.focused=focused;
		}
		
		abstract void render(Graphics2D g);
		
		abstract void renderDetails(Graphics2D g);
	}
	
	Color getColorForValue(double val){
		float col=(float) (2/(Math.exp(-val)+1)-1);
		double proxB=1-Math.min(1, Math.abs(col+1)), proxG=1-Math.min(1, Math.abs(col)), proxR=1-Math.min(1, Math.abs(col-1));
		double r=1*proxR+0.6*proxG+0*proxB;
		double g=0*proxR+0.6*proxG+0*proxB;
		double b=0*proxR+0.6*proxG+1*proxB;
		return new Color((float)r,(float)g,(float)b);
	}
	
	class Node extends GuiObject{
		double bias, inp, oup;
		double xPos, yPos;
		int layer, idx;
		final static int DIAMETER=30;
		final static double THICKNESS=3;
		boolean startNode=false;
		
		public Node(int layer, int idx, int xPos, int yPos, double bias) {
			this.bias=bias;
			this.layer=layer;
			this.idx=idx;
			this.xPos=xPos;
			this.yPos=yPos;
		}
		
		public Node(int layer, int idx, int xPos, int yPos, double bias, boolean startNode) {
			this.bias=bias;
			this.layer=layer;
			this.idx=idx;
			this.xPos=xPos;
			this.yPos=yPos;
			this.startNode=startNode;
		}

		@Override
		boolean isInside(Vector2D pos) {
			double dist=Math.sqrt(Math.pow((pos.getX()-xPos-DIAMETER/2),2)+Math.pow((pos.getY()-yPos-DIAMETER/2),2));
			return dist<DIAMETER/2;
		}

		@Override
		void render(Graphics2D g) {
			
			g.setColor(getColorForValue(inp));
			Arc2D larc=new Arc2D.Double(xPos, yPos, DIAMETER, DIAMETER, 90, 180, Arc2D.OPEN);
			g.fill(larc);
			
			g.setColor(getColorForValue(oup));
			Arc2D rarc=new Arc2D.Double(xPos, yPos, DIAMETER, DIAMETER, 90, -180, Arc2D.OPEN);
			g.fill(rarc);
			
			g.setColor(Color.BLACK);
			Line2D boundry=new Line2D.Double((xPos+DIAMETER/2), (yPos+1), (xPos+DIAMETER/2), yPos+DIAMETER);
			g.draw(boundry);
			
			g.scale(THICKNESS, THICKNESS);
			g.setColor(getColorForValue(bias));
			Ellipse2D border=new Ellipse2D.Double(xPos/THICKNESS, yPos/THICKNESS, DIAMETER/THICKNESS, DIAMETER/THICKNESS);
			g.draw(border);
			g.scale(1d/THICKNESS, 1d/THICKNESS);
		}

		int boxWidth=80, boxHeight=42;
		@Override
		void renderDetails(Graphics2D g) {
			double boxXPos=xPos+DIAMETER+10, boxYPos=yPos+DIAMETER/2-boxHeight/2;
			Rectangle2D box=new Rectangle2D.Double(boxXPos, boxYPos, boxWidth, boxHeight);
			g.setColor(new Color(220, 220, 220));
			g.fill(box);
			g.setColor(Color.BLACK);
			g.draw(box);
			g.drawString("Input: "+Math.round(inp*1000)/1000d, (int)boxXPos+3, (int)boxYPos+12);
			g.drawString("Output: "+Math.round(oup*1000)/1000d, (int)boxXPos+3, (int)boxYPos+12+13);
			g.drawString("Bias: "+Math.round(bias*1000)/1000d, (int)boxXPos+3, (int)boxYPos+12+26);
		}
		
	}
	
	class Connection extends GuiObject{
		Node from;
		Node to;
		double weight;
		final static int THICKNESS=2;

		Connection(Node from, Node to, double weight) {
			this.from=from;
			this.to=to;
			this.weight=weight;
		}

		@Override
		boolean isInside(Vector2D pos) {
			return false;
		}

		@Override
		void render(Graphics2D g) {
			g.setColor(getColorForValue(weight));
			double startX=from.xPos+Node.DIAMETER;
			double startY=from.yPos+Node.DIAMETER/2;
			double endX=to.xPos;
			double endY=to.yPos+Node.DIAMETER/2;
			g.scale(THICKNESS, THICKNESS);
			Line2D connection=new Line2D.Double(startX/THICKNESS, startY/THICKNESS, endX/THICKNESS, endY/THICKNESS);
			g.draw(connection);
			g.scale(1d/THICKNESS, 1d/THICKNESS);
		}

		@Override
		void renderDetails(Graphics2D g) {
		}
		
	}
	
	Point nodeOrig=new Point(-1, -1);
	Point nDragStart=new Point(-1,-1);
	Node draggedNode=null;
	class Canvas extends JPanel{
		public Canvas() {
			setBackground(Color.WHITE);
			MouseAdapter mHandler=new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					if(e.getButton()==MouseEvent.BUTTON3){
						oldOrig=e.getPoint();
						BraInsight.this.setCursor(new Cursor(Cursor.MOVE_CURSOR));
					}else if(e.getButton()==MouseEvent.BUTTON1){
						Vector2D vPoint=new Vector2D((e.getX()-offsetX)/scale, (e.getY()-offsetY)/scale);
						for(Node n:nodes){
							if(n.isInside(vPoint)){
								draggedNode=n;
							}
						}
						if(draggedNode!=null){
							nodeOrig=e.getPoint();
							nDragStart=e.getPoint();
							BraInsight.this.setCursor(new Cursor(Cursor.MOVE_CURSOR));
						}
					}
				}
				
				@Override
				public void mouseReleased(MouseEvent e) {
					if(e.getButton()==MouseEvent.BUTTON3){
						oldOrig=new Point(-1, -1);
					}else if(e.getButton()==MouseEvent.BUTTON1){
						if(e.getPoint().equals(nDragStart)){
							draggedNode.setFocused(!draggedNode.focused);
							if(draggedNode.startNode&&draggedNode.focused)overrideString="";
						}
						nodeOrig=new Point(-1, -1);
						nDragStart=new Point(-1, -1);
						draggedNode=null;
					}
					BraInsight.this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}
				
				@Override
				public void mouseDragged(MouseEvent e) {
					if(!oldOrig.equals(new Point(-1, -1))){
						int deltaX=(int) (e.getX()-oldOrig.getX());
						int deltaY=(int) (e.getY()-oldOrig.getY());
						offsetX+=deltaX;
						offsetY+=deltaY;
						oldOrig=e.getPoint();
						BraInsight.this.repaint();
					}
					if(!nodeOrig.equals(new Point(-1, -1))){
						double deltaX=(e.getX()-nodeOrig.getX())/scale;
						double deltaY=(e.getY()-nodeOrig.getY())/scale;
						draggedNode.xPos+=deltaX;
						draggedNode.yPos+=deltaY;
						nodeOrig=e.getPoint();
						BraInsight.this.repaint();
					}
				}
				
				@Override
				public void mouseWheelMoved(MouseWheelEvent e) {
					double vX=(e.getPoint().getX()-offsetX)/scale;
					double vY=(e.getPoint().getY()-offsetY)/scale;//Old virtual point
					sExp-=e.getWheelRotation();
					updateScale();
					offsetX=(int) (e.getPoint().getX()-vX*scale);
					offsetY=(int) (e.getPoint().getY()-vY*scale);//Set offset so that oldPoint matches newPoint
					BraInsight.this.repaint();
				}
			};
			addMouseListener(mHandler);
			addMouseMotionListener(mHandler);
			addMouseWheelListener(mHandler);
		}
		
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			render(g);
		}
	}
	
}
