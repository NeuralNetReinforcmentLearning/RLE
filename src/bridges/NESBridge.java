package bridges;

import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import javax.imageio.ImageIO;

import utils.Util;

public class NESBridge {//BizHawk NES Bridge
	public static final int PORT=4242, READ_TIMEOUT=5000;
	public static final String EMU_DIR="A:\\Schule\\Maturarbeit\\BizHawk\\";
	public static final File SCREENSHOT_FILE=new File(EMU_DIR+"RLEBridge\\ipc\\screenshot.png");
	
	public static ServerSocket server;
	public static Socket service;
	public static PrintWriter out;
	public static BufferedReader in;
	static boolean closed=false;
	
	public static void initialize(){
		try {
			server=new ServerSocket(PORT);
			System.out.println("Waiting for client on port "+PORT);
			service=server.accept();
			service.setSoTimeout(5000);
			out=new PrintWriter(service.getOutputStream());
			in=new BufferedReader(new InputStreamReader(service.getInputStream()));
			closed=false;
		} catch (IOException e) {
			System.out.println("Already a server running on port "+PORT);
			e.printStackTrace();
		}
	}
	
	public static void close(){
		System.out.println("Closing");
		try {
			server.close();
			service.close();
			out.close();
			in.close();
			closed=true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeAction(int action){//Writes action to pipe
		if(!closed){
			out.write(action+"\n");	
			out.flush();
		}
	}
	
	public static double readLastReward(){//Reads lastReward double from stream, BLOCKS!
		int lastReward=-1;
		if(!closed){
			try {
				lastReward=Integer.parseInt(in.readLine());
			} catch(Exception e){
				System.err.println("Emulator crashed - Lets revive him!");
				restart();
				return readLastReward();
			}
		}
		return lastReward;
	}
	
	public static void restart(){
		close();
		restartEmulator();
		initialize();
		writeAction(0);
	}
	
	public static void restartEmulator(){
		/*try {
			Runtime.getRuntime().exec("taskkill /f /im EmuHawk.exe");
			Thread.sleep(1000);
			Runtime.getRuntime().exec("cmd /c start "+EMU_DIR+"EmuHawk.exe");
			Thread.sleep(7000);
			Util.wallE.keyPress(KeyEvent.VK_ALT);
			Util.wallE.keyPress(KeyEvent.VK_F);
			Thread.sleep(100);
			Util.wallE.keyRelease(KeyEvent.VK_ALT);
			Util.wallE.keyRelease(KeyEvent.VK_F);
			
			Util.wallE.keyPress(KeyEvent.VK_DOWN);
			Thread.sleep(100);
			Util.wallE.keyRelease(KeyEvent.VK_DOWN);
			Util.wallE.keyPress(KeyEvent.VK_RIGHT);
			Thread.sleep(100);
			Util.wallE.keyRelease(KeyEvent.VK_RIGHT);
			Thread.sleep(1000);
			Util.wallE.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(100);
			Util.wallE.keyRelease(KeyEvent.VK_ENTER);

			Util.wallE.keyPress(KeyEvent.VK_ALT);
			Util.wallE.keyPress(KeyEvent.VK_T);
			Thread.sleep(100);
			Util.wallE.keyRelease(KeyEvent.VK_ALT);
			Util.wallE.keyRelease(KeyEvent.VK_T);
			Thread.sleep(1000);
			
			Util.wallE.keyPress(KeyEvent.VK_UP);
			Thread.sleep(200);
			Util.wallE.keyRelease(KeyEvent.VK_UP);
			Thread.sleep(200);
			
			Util.wallE.keyPress(KeyEvent.VK_UP);
			Thread.sleep(200);
			Util.wallE.keyRelease(KeyEvent.VK_UP);
			Thread.sleep(200);
			
			Util.wallE.keyPress(KeyEvent.VK_UP);
			Thread.sleep(200);
			Util.wallE.keyRelease(KeyEvent.VK_UP);
			Thread.sleep(200);
			
			Util.wallE.keyPress(KeyEvent.VK_UP);
			Thread.sleep(200);
			Util.wallE.keyRelease(KeyEvent.VK_UP);
			Thread.sleep(200);
			
			Util.wallE.keyPress(KeyEvent.VK_UP);
			Thread.sleep(200);
			Util.wallE.keyRelease(KeyEvent.VK_UP);
			Thread.sleep(200);
			
			Util.wallE.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(200);
			Util.wallE.keyRelease(KeyEvent.VK_ENTER);
			
			Thread.sleep(2000);
			Util.wallE.keyPress(KeyEvent.VK_ALT);
			Util.wallE.keyPress(KeyEvent.VK_F);
			Thread.sleep(200);
			Util.wallE.keyRelease(KeyEvent.VK_ALT);
			Util.wallE.keyRelease(KeyEvent.VK_F);
			
			Util.wallE.keyPress(KeyEvent.VK_UP);
			Thread.sleep(200);
			Util.wallE.keyRelease(KeyEvent.VK_UP);
			Util.wallE.keyPress(KeyEvent.VK_UP);
			Thread.sleep(200);
			Util.wallE.keyRelease(KeyEvent.VK_UP);
			Thread.sleep(200);
			Util.wallE.keyPress(KeyEvent.VK_RIGHT);
			Thread.sleep(100);
			Util.wallE.keyRelease(KeyEvent.VK_RIGHT);
			
			Thread.sleep(1000);
			Util.wallE.keyPress(KeyEvent.VK_ENTER);
			Util.wallE.keyRelease(KeyEvent.VK_ENTER);
			
			enlargeEmulator();
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		try {
			Runtime.getRuntime().exec("taskkill /f /im EmuHawk.exe");
			Thread.sleep(100);
//			Runtime.getRuntime().exec("python "+System.getProperty("user.dir")+"\\BizHawk\\BridgeStart.py");
			BufferedReader in = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("python "+System.getProperty("user.dir")+"\\BizHawk\\BridgeStart.py").getErrorStream()));
			String s=null;
			while((s = in.readLine()) != null){
				System.out.println(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static void enlargeEmulator(){
		Util.wallE.keyPress(KeyEvent.VK_ALT);
		Util.wallE.keyPress(KeyEvent.VK_TAB);
		Util.wallE.keyRelease(KeyEvent.VK_ALT);
		Util.wallE.keyRelease(KeyEvent.VK_TAB);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Util.wallE.keyPress(KeyEvent.VK_WINDOWS);
		Util.wallE.keyPress(KeyEvent.VK_UP);
		Util.wallE.keyRelease(KeyEvent.VK_WINDOWS);
		Util.wallE.keyRelease(KeyEvent.VK_UP);
	}
	
	public static BufferedImage getScreen(){
		try {
			return ImageIO.read(SCREENSHOT_FILE);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
