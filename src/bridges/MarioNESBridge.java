package bridges;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import environments.SimpleMarioEnvironment;

public class MarioNESBridge {//BizHawk NES Bridge
	public static final int PORT=4242, READ_TIMEOUT=5000;
	
	public static ServerSocket server;
	public static Socket service;
	public static PrintWriter out;
	public static BufferedReader in;
	static boolean closed=false;
	
	public static double lastReward;
	public static double[] state=new double[SimpleMarioEnvironment.OUTPUT_SIZE];
	
	public static void initialize(){
		try {
			server=new ServerSocket(PORT);
			System.out.println("Waiting for client on port "+PORT);
			service=server.accept();
			service.setSoTimeout(5000);
			out=new PrintWriter(service.getOutputStream());
			in=new BufferedReader(new InputStreamReader(service.getInputStream()));
			closed=false;
			System.out.println("Client connected!");
		} catch (IOException e) {
			System.out.println("Already a server running on port "+PORT);
			e.printStackTrace();
		}
		/*writeAction(0);
		readUpdate();*/
	}
	
	public static void close(){
		System.out.println("Closing");
		try {
			server.close();
			service.close();
			out.close();
			in.close();
			closed=true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeAction(int action){//Writes action to pipe
		if(!closed){
			out.write(action+"\n");	
			out.flush();
		}
	}
	
	public static void readUpdate(){//Reads state array & lastReward double from stream, BLOCKS!
		if(!closed){
			try {
				String stateStr=in.readLine();
				for(int i=0; i<SimpleMarioEnvironment.OUTPUT_SIZE; i++){
					int s=Character.getNumericValue(stateStr.charAt(i));
					if (s==2)s=-1;
					state[i]=s;
				}
				lastReward=Integer.parseInt(in.readLine());
				//if(lastReward==0)lastReward=-2;//Mooove bitch!
			} catch(Exception e){
				System.err.println("Emulator crashed - Lets revive him!");
				restart();
				readUpdate();
			}
		}
	}
	
	public static boolean isTerminal(){
		//System.out.println(lastReward+":"+(lastReward==-10));
		return lastReward==-10;
	}
	
	public static void restart(){
		close();
		//NESBridge.restartEmulator();
		initialize();
		writeAction(0);
	}
	
}
