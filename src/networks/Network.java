package networks;

import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import gui.NNControl;
import interfaces.BackPropagationOutput;
import interfaces.FeedForwardNeuralNet;

public class Network extends FeedForwardNeuralNet{
	RealVector[] zs, as;
	int layers;
	Random r;
	
	NNControl costGraph;
	
	ArrayList<Double>costHistory=new ArrayList<>();
	ArrayList<Double>avgHistory=new ArrayList<>();
	
	public boolean block=false;
	
	public Network(int[] layerSizes) {
		super(layerSizes);
	}
	
	@Override
	protected void initNet(int[] layerSizes) {
		layers=layerSizes.length;
		r=new Random();
		biases=new ArrayRealVector[layers-1];
		weights=new Array2DRowRealMatrix[layers-1];
		zs=new ArrayRealVector[layers-1];
		as=new ArrayRealVector[layers];
		
		
		//Random initialisation
		for(int i=0; i<layers-1; i++){
			double[] bRand=new double[layerSizes[i+1]];
			for(int j=0; j<bRand.length; j++)bRand[j]=r.nextDouble()*10-5;
			biases[i]=new ArrayRealVector(bRand);
			
			double[][] mRand=new double[layerSizes[i]][layerSizes[i+1]];
			for(int j=0; j<mRand.length; j++){
				for(int k=0; k<mRand[j].length; k++){
					mRand[j][k]=r.nextDouble()*10-5;
				}
			}
			weights[i]=new Array2DRowRealMatrix(mRand);
		}
		
		costGraph=new NNControl(this, 6000, 0, 1);
	}
	
	public RealVector feedForward(RealVector input){
		while(block){
			Thread.yield();
			try {Thread.sleep(10);} catch(InterruptedException e){e.printStackTrace();}
		}
		as[0]=input;
		for(int i=0; i<layers-1; i++){
			zs[i]=weights[i].preMultiply(as[i]).add(biases[i]);
			as[i+1]=activation(zs[i]);
		}
		return as[layers-1];
	}
	
	public RealVector feedForward(double... inputs){
		return feedForward(new ArrayRealVector(inputs));
	}
	
	/*RealVector feedForward(double action, double[] stateinfo){
		double[] inputs=new double[layerSizes[0]];
		for(int i=0; i<inputs.length; i++){
			if(i==action)inputs[i]=1;
			else if(i>=inputs.length-stateinfo.length)inputs[i]=stateinfo[i+stateinfo.length-inputs.length];
			else inputs[i]=0;
		}
		return feedForward(new ArrayRealVector(inputs));
	}*/
	
	public RealVector feedForward(double action, double[] stateinfo){
		double[] inputs=new double[stateinfo.length+1];
		for(int i=0; i<inputs.length; i++){
			if(i==0)inputs[i]=action;
			else inputs[i]=stateinfo[i-1];
		}
		return feedForward(new ArrayRealVector(inputs));
	}
	
	public double cost(RealVector answer){
		double cost=0;
		RealVector diff=answer.subtract(as[layers-1]);
		for(int i=0; i<diff.getDimension(); i++)cost+=Math.pow(diff.getEntry(i),2);
		cost=cost/2;
		return cost;
	}
	
	
	public BackPropagationOutput backpropagate(RealVector answer){
		RealVector[] nablaBiases=new ArrayRealVector[layers-1];
		RealMatrix[] nablaWeights=new Array2DRowRealMatrix[layers-1];
		RealVector[] errors=new ArrayRealVector[layers-1];
		
		if(costGraph.chckbxPlotCost.isSelected()){
			int avg=1;
			double[] ng=new double[(int) costGraph.xMax];
			costHistory.add(cost(answer));
			//costHistory.add(reward+1);
			if(costHistory.size()>avg){
				double newAvg=0;
				for(int i=-avg; i<0; i++)newAvg+=costHistory.get(costHistory.size()+i);
				newAvg/=avg;
				avgHistory.add(newAvg);
			}else avgHistory.add(costHistory.get(costHistory.size()-1));
		
			if(costHistory.size()>ng.length){
				costHistory.remove(0);
				avgHistory.remove(0);
			}
			
			for(int i=0; i<avgHistory.size(); i++)ng[i]=avgHistory.get(i);
			/*for(int i=0; i<ng.length; i++){
				if(i<ng.length-1)ng[i]=costGraph.graphs[0][i];
				else ng[i]=cost(answer);
			}*/
			costGraph.setPlotArray(ng);
			//costGraph.changexMax(costGraph.xMax+1);
			costGraph.repaint();
			//System.out.println(cost(answer));
		}
		
		//RealVector outputError=hadamardProduct(as[layers-1].subtract(answer),activationPrime(zs[zs.length-1]));
		RealVector outputError=as[layers-1].subtract(answer);
		//for(int i=0; i<outputError.getDimension(); i++)outputError.setEntry(i, outputError.getEntry(i)*Math.exp(-reward));
		errors[errors.length-1]=outputError;
		for(int i=layers-2; i>=0; i--){
			//Calculating Bias derivatives
			nablaBiases[i]=errors[i];
			
			//Calculating Weight derivatives
			double[][] tWeights=new double[as[i].getDimension()][errors[i].getDimension()];
			for(int j=0; j<as[i].getDimension(); j++){
				for(int k=0; k<errors[i].getDimension(); k++){
					tWeights[j][k]=as[i].getEntry(j)*errors[i].getEntry(k);
				}
			}
			nablaWeights[i]=new Array2DRowRealMatrix(tWeights);
			
			//Packpropagating errors further
			if(i>0){
				errors[i-1]=hadamardProduct(weights[i].transpose().preMultiply(errors[i]),activationPrime(zs[i-1]));
			}
		}
		return new BackPropagationOutput(new RealVector[][]{nablaBiases}, new RealMatrix[][]{nablaWeights});
	}
	
	public void gradDescent(double learningRate, RealVector[] nablaBiases, RealMatrix[] nablaWeights){
		for(int i=0; i<layers-1; i++){
			for(int j=0; j<layerSizes[i+1]; j++){
				double newVal=biases[i].getEntry(j)-learningRate*nablaBiases[i].getEntry(j);
				biases[i].setEntry(j, newVal);
			}
			for(int j=0; j<layerSizes[i]; j++){
				for(int k=0; k<layerSizes[i+1]; k++){
					double newVal=weights[i].getEntry(j,k)-learningRate*nablaWeights[i].getEntry(j,k);
					weights[i].setEntry(j, k, newVal);
				}
			}
		}
	}
	
	double activation(double z){
		return 1/(1+Math.exp(-z));
	}
	
	/*double activation(double z){
		//return Math.log(1+Math.exp(z)); Java cant handle
		return Math.max(z, 0);
	}*/
	
	RealVector activation(RealVector z){
		double[] os=new double[z.getDimension()];
		for(int i=0; i<os.length; i++)os[i]=activation(z.getEntry(i));
		return new ArrayRealVector(os);
	}
	
	double activationPrime(double z){
		if(z>40)return 0.0;
		else if(z<-40)return 0.0;
		else {
			double ehz=Math.exp(-z);
			return ehz/Math.pow((1+ehz),2);
		}
	}
	
	/*double activationPrime(double z){
		//return 1/(1+Math.exp(-z));
		if(z>=0)return 1;
		else return 0;
	}*/
	
	RealVector activationPrime(RealVector z){
		double[] os=new double[z.getDimension()];
		for(int i=0; i<os.length; i++)os[i]=activationPrime(z.getEntry(i));
		return new ArrayRealVector(os);
	}
	
	static RealVector hadamardProduct(RealVector v1, RealVector v2){
		if(v1.getDimension()!=v2.getDimension())throw new IllegalArgumentException();
		double[] os=new double[v1.getDimension()];
		for(int i=0; i<v1.getDimension(); i++)os[i]=v1.getEntry(i)*v2.getEntry(i);
		return new ArrayRealVector(os);
	}
}
