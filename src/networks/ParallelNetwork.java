package networks;

import java.util.Random;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import core.TaskMaster;
import interfaces.BackPropagationOutput;
import interfaces.FeedForwardNeuralNet;
import utils.Util;

public class ParallelNetwork extends FeedForwardNeuralNet{
	final double LEAKSCALAR=0.01, L2DECAY=0.00, MOMENTUM=0.7, ADAMDECAY1=0.9, ADAMDECAY2=0.999, ADAMSTEPSIZE=0.001, EPSILON=Math.pow(10, -8);
	
	int[] layerSizes;
	int layers;
	
	RealMatrix[] lIn; //Input for layer i+1
	RealMatrix[] lOut; //Output of layer i
	
	RealVector[] biasM;
	RealMatrix[] weightM;
	RealVector[] biasV;
	RealMatrix[] weightV;
	
	Random rand;

	public ParallelNetwork(int... layerSizes){
		super(layerSizes);
	}

	protected void initNet(int[] layerSizes) {
		layers=layerSizes.length;
		rand=new Random();
		biases=new ArrayRealVector[layers-1];
		weights=new Array2DRowRealMatrix[layers-1];
		lIn=new Array2DRowRealMatrix[layers-1];
		lOut=new Array2DRowRealMatrix[layers];
		biasM=new ArrayRealVector[layers-1];
		weightM=new Array2DRowRealMatrix[layers-1];
		biasV=new ArrayRealVector[layers-1];
		weightV=new Array2DRowRealMatrix[layers-1];
		
		//Random initialisation
		for(int i=0; i<layers-1; i++){
			double[] bRand=new double[layerSizes[i+1]];
			for(int j=0; j<bRand.length; j++)bRand[j]=0;
			biases[i]=new ArrayRealVector(bRand);
			
			double[][] mRand=new double[layerSizes[i+1]][layerSizes[i]];//Width: l Height: l+1
			for(int j=0; j<mRand.length; j++){
				for(int k=0; k<mRand[j].length; k++){
					mRand[j][k]=rand.nextDouble()-0.5;
				}
			}
			weights[i]=new Array2DRowRealMatrix(mRand);
			
			biasM[i]=new ArrayRealVector(layerSizes[i+1]);
			weightM[i]=new Array2DRowRealMatrix(layerSizes[i+1], layerSizes[i]);
			biasV[i]=new ArrayRealVector(layerSizes[i+1]);
			weightV[i]=new Array2DRowRealMatrix(layerSizes[i+1], layerSizes[i]);
		}
	}
	
	//================================Feed Forward================================
	public RealMatrix feedForward(RealMatrix inputs) {
		lOut[0]=inputs.copy();
		for(int i=0; i<layers-1; i++){
			/*double[][] biasMatrix=new double[inputs.getColumnDimension()][];
			double[] biasArray=biases[i].toArray();
			for(int j=0; j<biasMatrix.length; j++)biasMatrix[j]=biasArray;*/
			lIn[i]=weights[i].multiply(lOut[i]);
			for(int j=0; j<lIn[i].getColumnDimension(); j++)lIn[i].setColumnVector(j, lIn[i].getColumnVector(j).add(biases[i]));//TODO: Performance?
			lOut[i+1]=activate(i+1,lIn[i]);
		}
		return lOut[layers-1].copy();
	}
	
	public RealMatrix feedForward(double[] state){
		RealMatrix input=new Array2DRowRealMatrix(TaskMaster.environment.getStateSize(), 1);
		input.setColumn(0, state);
		return feedForward(input);
	}
	
	RealMatrix activate(int layer, RealMatrix arg){
		double[][] out=new double[arg.getRowDimension()][arg.getColumnDimension()];
		for(int i=0; i<out.length; i++){
			for(int j=0; j<out[i].length; j++){
				out[i][j]=activation(layer, arg.getEntry(i, j));
			}
		}
		return new Array2DRowRealMatrix(out);
	}
	
	RealMatrix activatePrime(int layer, RealMatrix arg){
		double[][] out=new double[arg.getRowDimension()][arg.getColumnDimension()];
		for(int i=0; i<out.length; i++){
			for(int j=0; j<out[i].length; j++){
				out[i][j]=activationPrime(layer, arg.getEntry(i, j));
			}
		}
		return new Array2DRowRealMatrix(out);
	}
	
	/*double activation(double z){
		return 1/(1+Math.exp(-z));
	}
	
	public double activationPrime(double z){
		double az=activation(z);
		return az*(1-az);
	}*/
	
	double activation(int layer, double z){//ReLU FTW!!
		if(layer<layers-1)return Math.max(LEAKSCALAR*z, z);
		else if(layer==layers-1)return z;
		else{
			System.out.println("Invalid layer");
			return Double.NaN;
		}
	}
	
	public double activationPrime(int layer, double z){
		if(layer<layers-1){
			if(z>0)return 1;
			else return LEAKSCALAR;
		}
		else if(layer==layers-1)return 1;
		else{
			System.out.println("Invalid layer");
			return Double.NaN;
		}
	}
	
	
	//================================Backprop================================
	public double[] cost(RealMatrix answers) {
		double[] out=new double[answers.getColumnDimension()];
		for(int i=0; i<out.length; i++){
			double cSum=0; 
			for(int j=0; j<answers.getRowDimension(); j++){
				cSum+=calcCost(lOut[layers-1].getEntry(j, i), answers.getEntry(j, i));
			}
			out[i]=Math.sqrt(cSum);
		}
		return out;
	}
	
	public double calcCost(double out, double ans){
		return Math.abs(out-ans);
		//return -(ans*Math.log(out)+(1-ans)*Math.log(1-out)); //Cross-Entropy Cost
	}

	public BackPropagationOutput backpropagate(RealMatrix answers) {
		RealVector[][] nablaBiases=new RealVector[answers.getColumnDimension()][layers-1];
		RealMatrix[][] nablaWeights=new RealMatrix[answers.getColumnDimension()][layers-1];
		RealMatrix[] errors=new RealMatrix[layers-1];
		
		errors[errors.length-1]=hadamardProduct(lOut[lOut.length-1].subtract(answers),activatePrime(layers-1,lIn[lIn.length-1]));
		
		for(int l=layers-2; l>=0; l--){//Looping back through layers
			for(int i=0; i<answers.getColumnDimension(); i++){//Looping through samples
				nablaBiases[i][l]=errors[l].getColumnVector(i);//Calculating biases
				nablaWeights[i][l]=lOut[l].getColumnMatrix(i).transpose().preMultiply(errors[l].getColumnMatrix(i));//Calculating weights
			}
			if(l>0){
				errors[l-1]=hadamardProduct(weights[l].transpose().multiply(errors[l]), activatePrime(l,lIn[l-1]));//Backpropagate Error through (l-1) layer
			}
		}
		
		return new BackPropagationOutput(nablaBiases, nablaWeights);
	}
	
	RealMatrix hadamardProduct(RealMatrix m1, RealMatrix m2){//Multiplies matrices element-wise
		if(m1.getColumnDimension()!=m2.getColumnDimension()||m1.getRowDimension()!=m2.getRowDimension())throw new IllegalArgumentException();
		double[][] outArr=new double[m1.getRowDimension()][m1.getColumnDimension()];
		for(int y=0; y<m1.getRowDimension(); y++){
			for(int x=0; x<m1.getColumnDimension(); x++){
				outArr[y][x]=m1.getEntry(y,x)*m2.getEntry(y,x);
			}
		}
		return new Array2DRowRealMatrix(outArr);
	}
	
	RealMatrix hadamardDivide(RealMatrix m1, RealMatrix m2){//Divides matrices element-wise
		if(m1.getColumnDimension()!=m2.getColumnDimension()||m1.getRowDimension()!=m2.getRowDimension())throw new IllegalArgumentException();
		double[][] outArr=new double[m1.getRowDimension()][m1.getColumnDimension()];
		for(int y=0; y<m1.getRowDimension(); y++){
			for(int x=0; x<m1.getColumnDimension(); x++){
				outArr[y][x]=m1.getEntry(y,x)/m2.getEntry(y,x);
			}
		}
		return new Array2DRowRealMatrix(outArr);
	}

	//================================Grad Descent================================
	int steps=0;
	public void gradDescent(double learningRate, RealVector[] nablaBiases, RealMatrix[] nablaWeights) {
		for(int l=0; l<layers-1; l++){//Iterating through layers
			/*	SGD
			biases[l]=biases[l].subtract(nablaBiases[l].mapMultiply(learningRate));//Adjusting Biases
			weights[l]=weights[l].subtract(nablaWeights[l].scalarMultiply(learningRate).add(weights[l].scalarMultiply(L2DECAY)));*////Adjusting Weights with L2 Regularization
			/*	Nadagrad momentum
			RealVector preBiasV=biasV[l];
			biasV[l]=biasV[l].mapMultiply(MOMENTUM).subtract(nablaBiases[l].mapMultiply(learningRate));
			biases[l]=biases[l].add(biasV[l].mapMultiply(1+MOMENTUM).subtract(preBiasV.mapMultiply(MOMENTUM)));
			RealMatrix preWeightV=weightV[l];
			weightV[l]=weightV[l].scalarMultiply(MOMENTUM).subtract(nablaWeights[l].scalarMultiply(learningRate).add(weights[l].scalarMultiply(L2DECAY*learningRate)));
			weights[l]=weights[l].add(weightV[l].scalarMultiply(1+MOMENTUM).subtract(preWeightV.scalarMultiply(MOMENTUM)));*/
			
			//Adam
			steps++;
			biasM[l]=biasM[l].mapMultiply(ADAMDECAY1).add(nablaBiases[l].mapMultiply(1-ADAMDECAY1));
			RealVector corrBiasM=biasM[l].mapMultiply(1d/(1-Math.pow(ADAMDECAY1,steps)));
			biasV[l]=biasV[l].mapMultiply(ADAMDECAY2).add(Util.elementPow(nablaBiases[l], 2).mapMultiply(1-ADAMDECAY2));
			RealVector corrBiasV=biasV[l].mapMultiply(1d/(1-Math.pow(ADAMDECAY2,steps)));
			biases[l]=biases[l].subtract(hadamardDivide(new Array2DRowRealMatrix(corrBiasM.toArray()), new Array2DRowRealMatrix(Util.elementPow(corrBiasV, 0.5).toArray()).scalarAdd(EPSILON)).getColumnVector(0).mapMultiply(ADAMSTEPSIZE));
			
			weightM[l]=weightM[l].scalarMultiply(ADAMDECAY1).add(nablaWeights[l].scalarMultiply(1-ADAMDECAY1));
			RealMatrix corrWeightM=weightM[l].scalarMultiply(1d/(1-Math.pow(ADAMDECAY1,steps)));
			
			weightV[l]=weightV[l].scalarMultiply(ADAMDECAY2).add(Util.elementPow(nablaWeights[l], 2).scalarMultiply(1-ADAMDECAY2));
			RealMatrix corrWeightV=weightV[l].scalarMultiply(1d/(1-Math.pow(ADAMDECAY2,steps)));
			
			weights[l]=weights[l].subtract(hadamardDivide(corrWeightM, Util.elementPow(corrWeightV, 0.5).scalarAdd(EPSILON)).scalarMultiply(ADAMSTEPSIZE));
			
			weights[l]=weights[l].scalarMultiply(1-L2DECAY);//Regularization decay
		}
	}
	
	@Override
	public double cost(RealVector answer) {
		return 0;
	}

	public BackPropagationOutput backpropagate(RealVector answer) {
		return null;
	}

}
