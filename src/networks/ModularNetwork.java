package networks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import core.Main;
import interfaces.SaveAndLoadableNetwork;
import interfaces.StringSaveAndLoadable;
import layers.ConvolutionLayer;
import layers.FullyConnectedLayer;
import layers.Layer;
import layers.PoolingLayer;
import layers.ReLULayer;
import layers.TrainableLayer;
import layers.TrainableLayer.LayerGradient;
import optimizers.Adam;
import optimizers.Optimizer;
import utils.Util;

public class ModularNetwork implements SaveAndLoadableNetwork{
	public static final double GAUSS_SCALE=1;
	public boolean debug=true;
	int inRD, inCD;//Input row & column Dimensions
	public Layer[] layers;
	public TrainableLayer[] trainableLayers;
	
	Optimizer optimizer;
	
	RealMatrix output;
	
	public ModularNetwork(int inRD, int inCD, boolean initNet, Optimizer optimizer, Layer... layers) {
		this.inRD=inRD;
		this.inCD=inCD;
		this.layers=layers;
		this.optimizer=optimizer;
		if(initNet)initNet();
	}

	protected void initNet() {
		ArrayList<TrainableLayer> tLayerList=new ArrayList<>();
		for(int l=0; l<layers.length; l++){
			layers[l].initialize(getLayerOutColumnDimension(l-1), getLayerOutRowDimension(l-1));
			if(layers[l] instanceof TrainableLayer){
				TrainableLayer tl=(TrainableLayer)layers[l];
				tl.initializeTrainableParameters();
				tLayerList.add(tl);
			}
		}
		trainableLayers=tLayerList.toArray(new TrainableLayer[0]);
	}
	
	int getLayerOutColumnDimension(int layer){
		if(layer==-1)return inCD;
		else return layers[layer].getOutColumnDimension();
	}
	
	int getLayerOutRowDimension(int layer){
		if(layer==-1)return inRD;
		else return layers[layer].getOutRowDimension();
	}
	
	public RealMatrix feedForward(RealMatrix input){
		RealMatrix mem=input.copy();
		for(int l=0; l<layers.length; l++){
			if(debug){
				if(l==2)Main.performanceWatcher.startWatching(1);
				if(l==4)Main.performanceWatcher.startWatching(2);
			}
			mem=layers[l].passForward(mem);
			if(debug){
				if(l==2)Main.performanceWatcher.endWatching(1);
				if(l==4)Main.performanceWatcher.endWatching(2);
			}
		}
		output=mem;
		return mem;
	}
	
	public RealMatrix feedForward(double[][] inp){
		return feedForward(new Array2DRowRealMatrix(inp));
	}
	
	public LayerGradient[] backpropagate(RealMatrix answer){
		LayerGradient[] bpOut=new LayerGradient[trainableLayers.length];
		int bpOutIndex=bpOut.length-1;
		RealMatrix error=costPrime(output, answer);
		for(int i=layers.length-1; i>=0; i--){
			if(layers[i] instanceof TrainableLayer){
				bpOut[bpOutIndex]=((TrainableLayer)layers[i]).calcBPOutput(error);
				bpOutIndex--;
			}
			if(i>0)error=layers[i].passBackward(error);
		}
		return bpOut;
	}
	
	public double cost(RealMatrix output, RealMatrix answer) {//(1/2)(out-ans)^2
		RealMatrix procMatr=Util.elementPow(output.subtract(answer),2).scalarMultiply(0.5);
		return Util.elementSum(procMatr);
	}
	
	public double cost(double output, double answer){
		return Math.pow(output-answer, 2)*0.5;
	}
	
	public RealMatrix costPrime(RealMatrix output, RealMatrix answer){
		return output.subtract(answer);
	}

	public void train(LayerGradient[] gradients){
		if(optimizer!=null){
			optimizer.optimize(trainableLayers, gradients);
			optimizer.decay(trainableLayers);
		}
	}

	public ModularNetwork copy(){
		ArrayList<TrainableLayer> tLayerList=new ArrayList<>();
		Layer[] newLayers=new Layer[layers.length];
		for(int l=0; l<newLayers.length; l++){
			Layer newLayer=layers[l].copy();
			newLayers[l]=newLayer;
			if(newLayer instanceof TrainableLayer)tLayerList.add((TrainableLayer)newLayer);
		}
		ModularNetwork copy=new ModularNetwork(inRD, inCD, false, optimizer, newLayers);
		copy.trainableLayers=tLayerList.toArray(new TrainableLayer[0]);
		return copy;
	}

	@Override
	public void loadNet(File f) {
		Scanner fin=null;
		try {
			fin=new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ArrayList<Layer> newLayers=new ArrayList<>();
		ArrayList<TrainableLayer> newTrainableLayers=new ArrayList<>();
		while(fin.hasNextLine()){
			String line=fin.nextLine();
			String[] parts=line.split(":");
			
			if(parts[0].equals("Adam")){//Well thats not nicely solved
				if(optimizer instanceof Adam){
					((Adam) optimizer).constructFromSaveString(parts[1]);
				}
			}else{
				Layer newLayer=null;
				if(parts[0].equals("ConvolutionLayer")){
					newLayer=new ConvolutionLayer(0, 0, 0, 0);
				}else if(parts[0].equals("FullyConnectedLayer")){
					newLayer=new FullyConnectedLayer(0);
				}else if(parts[0].equals("PoolingLayer")){
					newLayer=new PoolingLayer();
				}else if(parts[0].equals("ReLULayer")){
					newLayer=new ReLULayer();
				}
				
				newLayer.constructFromSaveString(parts[1]);
				newLayers.add(newLayer);
				if(newLayer instanceof TrainableLayer)newTrainableLayers.add((TrainableLayer)newLayer);
			}
		}
		fin.close();
		layers=newLayers.toArray(new Layer[0]);
		trainableLayers=newTrainableLayers.toArray(new TrainableLayer[0]);
	}

	@Override
	public void saveNet(File f) {
		StringBuilder out=new StringBuilder();
		for(int l=0; l<layers.length; l++){
			out.append(layers[l].getClass().getSimpleName()+":");
			out.append(layers[l].getSaveString());
			if(l<layers.length-1)out.append("\n");
		}
		if(optimizer instanceof StringSaveAndLoadable){// Can/Should we save the optimizer?
			out.append("\n");
			out.append(optimizer.getClass().getSimpleName()+":");
			out.append(((StringSaveAndLoadable) optimizer).getSaveString());
		}
		
		try {
			FileWriter outWriter=new FileWriter(f);
			outWriter.write(out.toString());
			outWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
