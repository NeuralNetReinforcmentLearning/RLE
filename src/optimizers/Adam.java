package optimizers;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import interfaces.StringSaveAndLoadable;
import layers.TrainableLayer;
import layers.TrainableLayer.LayerGradient;
import utils.Operation;
import utils.Util;

public class Adam extends Optimizer implements StringSaveAndLoadable{
	int steps;
	double ADAMDECAY1=0.9, ADAMDECAY2=0.999, ADAMSTEPSIZE=0.001, EPSILON=Math.pow(10, -8);
	RealVector[] biasM;
	RealMatrix[] weightM;
	RealVector[] biasV;
	RealMatrix[] weightV;
	
	public Adam(){}
	
	public Adam(double ADAMDECAY1, double ADAMDECAY2, double ADAMSTEPSIZE, double EPSILON) {
		steps=0;
		this.ADAMDECAY1=ADAMDECAY1;
		this.ADAMDECAY2=ADAMDECAY2;
		this.ADAMSTEPSIZE=ADAMSTEPSIZE;
		this.EPSILON=EPSILON;
	}
	
	void initializeStorage(TrainableLayer[] layers){
		biasM=new RealVector[layers.length];
		weightM=new RealMatrix[layers.length];
		biasV=new RealVector[layers.length];
		weightV=new RealMatrix[layers.length];
		for(int i=0; i<layers.length; i++){
			biasM[i]=new ArrayRealVector(layers[i].biases.getDimension());
			weightM[i]=new Array2DRowRealMatrix(layers[i].weights.getRowDimension(), layers[i].weights.getColumnDimension());
			biasV[i]=new ArrayRealVector(layers[i].biases.getDimension());
			weightV[i]=new Array2DRowRealMatrix(layers[i].weights.getRowDimension(), layers[i].weights.getColumnDimension());
		}
	}

	@Override
	public void optimize(TrainableLayer[] layers, LayerGradient[] gradients) {
		if(biasM==null)initializeStorage(layers);//Not the nicest way
		for(int l=0; l<layers.length; l++){
			steps++;
			biasM[l]=biasM[l].mapMultiply(ADAMDECAY1).add(gradients[l].nablaBiases.mapMultiply(1-ADAMDECAY1));
			RealVector corrBiasM=biasM[l].mapMultiply(1d/(1-Math.pow(ADAMDECAY1,steps)));
			biasV[l]=biasV[l].mapMultiply(ADAMDECAY2).add(Util.elementPow(gradients[l].nablaBiases, 2).mapMultiply(1-ADAMDECAY2));
			RealVector corrBiasV=biasV[l].mapMultiply(1d/(1-Math.pow(ADAMDECAY2,steps)));
			layers[l].biases=layers[l].biases.subtract(Util.componentwiseOperation(Operation.hadamardDivisionOperation, new Array2DRowRealMatrix(corrBiasM.toArray()), new Array2DRowRealMatrix(Util.elementPow(corrBiasV, 0.5).toArray()).scalarAdd(EPSILON)).getColumnVector(0).mapMultiply(ADAMSTEPSIZE));
			
			weightM[l]=weightM[l].scalarMultiply(ADAMDECAY1).add(gradients[l].nablaWeights.scalarMultiply(1-ADAMDECAY1));
			RealMatrix corrWeightM=weightM[l].scalarMultiply(1d/(1-Math.pow(ADAMDECAY1,steps)));
			
			weightV[l]=weightV[l].scalarMultiply(ADAMDECAY2).add(Util.elementPow(gradients[l].nablaWeights, 2).scalarMultiply(1-ADAMDECAY2));
			RealMatrix corrWeightV=weightV[l].scalarMultiply(1d/(1-Math.pow(ADAMDECAY2,steps)));
			
			layers[l].weights=layers[l].weights.subtract(Util.componentwiseOperation(Operation.hadamardDivisionOperation, corrWeightM, Util.elementPow(corrWeightV, 0.5).scalarAdd(EPSILON)).scalarMultiply(ADAMSTEPSIZE));
		}
	}

	@Override
	public Optimizer copy() {
		Adam newOptimizer=new Adam();
		newOptimizer.steps=steps;
		RealVector[] newBiasM=new RealVector[biasM.length];
		RealMatrix[] newWeightM=new RealMatrix[weightM.length];
		RealVector[] newBiasV=new RealVector[biasV.length];
		RealMatrix[] newWeightV=new RealMatrix[weightV.length];
		for(int i=0; i<newBiasM.length; i++){
			newBiasM[i]=biasM[i].copy();
			newWeightM[i]=weightM[i].copy();
			newBiasV[i]=biasV[i].copy();
			newWeightV[i]=weightV[i].copy();
		}
		newOptimizer.biasM=newBiasM;
		newOptimizer.weightM=newWeightM;
		newOptimizer.biasV=newBiasV;
		newOptimizer.weightV=newWeightV;
		return newOptimizer;
	}

	@Override
	public String getSaveString() {
		StringBuilder out=new StringBuilder();
		out.append(steps+"&"+biasM.length+"&");
		
		for(int l=0; l<biasM.length; l++){
			//M's
			for(int j=0; j<biasM[l].getDimension(); j++){
				if(j<biasM[l].getDimension()-1)out.append(biasM[l].getEntry(j)+",");
				else out.append(biasM[l].getEntry(j)+"&");
			}
			
			for(int j=0; j<weightM[l].getRowDimension(); j++){
				for(int k=0; k<weightM[l].getColumnDimension(); k++){
					if(k<weightM[l].getColumnDimension()-1)out.append(weightM[l].getEntry(j,k)+",");
					else out.append(weightM[l].getEntry(j,k));
				}
				if(j<weightM[l].getRowDimension()-1)out.append(";");
				else out.append("&");
			}
			//V's
			for(int j=0; j<biasV[l].getDimension(); j++){
				if(j<biasV[l].getDimension()-1)out.append(biasV[l].getEntry(j)+",");
				else out.append(biasV[l].getEntry(j)+"&");
			}
			
			for(int j=0; j<weightV[l].getRowDimension(); j++){
				for(int k=0; k<weightV[l].getColumnDimension(); k++){
					if(k<weightV[l].getColumnDimension()-1)out.append(weightV[l].getEntry(j,k)+",");
					else out.append(weightV[l].getEntry(j,k));
				}
				if(j<weightV[l].getRowDimension()-1)out.append(";");
				else out.append("");
			}
			if(l<biasM.length-1)out.append("&");
		}

		return out.toString();
	}

	@Override
	public void constructFromSaveString(String str) {
		String[] parts=str.split("&");
		steps=Integer.parseInt(parts[0]);
		int layers=Integer.parseInt(parts[1]);
		int strIdx=2;
		RealVector[] newBiasM=new RealVector[layers];
		RealMatrix[] newWeightM=new RealMatrix[layers];
		RealVector[] newBiasV=new RealVector[layers];
		RealMatrix[] newWeightV=new RealMatrix[layers];
		
		for(int l=0; l<layers; l++){
			String biasMString=parts[l*4+strIdx];
			String weightMString=parts[l*4+strIdx+1];
			String biasVString=parts[l*4+strIdx+2];
			String weightVString=parts[l*4+strIdx+3];
			
			//M's
			String[] biasMSplit=biasMString.split(",");
			double[] mBiases=new double[biasMSplit.length];
			for(int j=0; j<mBiases.length; j++)mBiases[j]=Double.parseDouble(biasMSplit[j]);
			
			String[] weightMRowSplit=weightMString.split(";");
			String[][] weightMSplit=new String[weightMRowSplit.length][];
			for(int j=0; j<weightMRowSplit.length; j++)weightMSplit[j]=weightMRowSplit[j].split(",");
			double[][] mWeights=new double[weightMSplit.length][weightMSplit[0].length];
			for(int j=0; j<weightMSplit.length; j++){
				for(int k=0; k<weightMSplit[j].length; k++){
					mWeights[j][k]=Double.parseDouble(weightMSplit[j][k]);
				}
			}
			//V's
			String[] biasVSplit=biasVString.split(",");
			double[] vBiases=new double[biasVSplit.length];
			for(int j=0; j<vBiases.length; j++)vBiases[j]=Double.parseDouble(biasVSplit[j]);
			
			String[] weightVRowSplit=weightVString.split(";");
			String[][] weightVSplit=new String[weightVRowSplit.length][];
			for(int j=0; j<weightVRowSplit.length; j++)weightVSplit[j]=weightVRowSplit[j].split(",");
			double[][] vWeights=new double[weightVSplit.length][weightVSplit[0].length];
			for(int j=0; j<weightVSplit.length; j++){
				for(int k=0; k<weightVSplit[j].length; k++){
					vWeights[j][k]=Double.parseDouble(weightVSplit[j][k]);
				}
			}
			newBiasM[l]=new ArrayRealVector(mBiases);
			newWeightM[l]=new Array2DRowRealMatrix(mWeights);
			newBiasV[l]=new ArrayRealVector(vBiases);
			newWeightV[l]=new Array2DRowRealMatrix(vWeights);
		}
	}
	
}
