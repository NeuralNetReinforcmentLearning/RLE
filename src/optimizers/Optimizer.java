package optimizers;

import layers.TrainableLayer;
import layers.TrainableLayer.LayerGradient;

public abstract class Optimizer {
	public static double L2DECAY=0.00;
	
	public abstract void optimize(TrainableLayer[] layers, LayerGradient[] gradients);
	
	public void decay(TrainableLayer[] layers){//Regularization decay
		if(L2DECAY!=0)for(TrainableLayer tl:layers)tl.weights=tl.weights.scalarMultiply(1-L2DECAY);
	}
	
	public abstract Optimizer copy();
}
