package optimizers;

import layers.TrainableLayer;
import layers.TrainableLayer.LayerGradient;

public class SGD extends Optimizer{
	double learningRate;
	
	public SGD(double learningRate) {
		this.learningRate=learningRate;
	}

	@Override
	public void optimize(TrainableLayer[] layers, LayerGradient[] gradients) {
		for(int l=0; l<layers.length; l++){
			layers[l].biases=layers[l].biases.subtract(gradients[l].nablaBiases.mapMultiply(learningRate));//Adjusting Biases
			layers[l].weights=layers[l].weights.subtract(gradients[l].nablaWeights.scalarMultiply(learningRate));
		}
	}

	@Override
	public Optimizer copy() {
		return new SGD(learningRate);
	}
	
}
