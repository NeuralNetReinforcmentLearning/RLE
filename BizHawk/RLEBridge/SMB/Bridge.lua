socket=require('socket')
sClient=socket.tcp()
sClient:settimeout(1)--1 second timeout

ButtonNames = {
	"A",
	"B",
	"Up",
	"Down",
	"Left",
	"Right",
}
Buttons = #ButtonNames

PORT = 4242
SCREENSHOT_FILE = "E:/Schule/Maturarbeit/BizHawk/RLEBridge/ipc/screenshot.png"
FRAME_BUNDLESIZE=3

function resetGame()
	savestate.load("E:/Schule/Maturarbeit/BizHawk/RLEBridge/SMB/StartState.State")
end

oldScreenX = 0

finishX = 3045--100 --Flag is 3045

function stepGame()--Returns deltaX as Reward
	emu.frameadvance()
	--client.pause()
	
	local alive = memory.readbyte(0x07CA)-- 0:Normal, 1:Fucked up
	--local lives = memory.readbyte(0x075A) + 1
	if alive~=0 then --He fucked up
		--console.writeline("He fucked up")
		resetGame()
		--client.pause()
		oldScreenX = 20 -- -20 Reward for dying
		
	elseif oldScreenX >= finishX then --Final Flag
		resetGame()
		
		oldScreenX = -20 -- 20 Reward for finishing
	end
	local newScreenX = math.max(memory.readbyte(0x071B)-1, 0) * 0x100 + memory.readbyte(0x071C)
	local deltaX=newScreenX - oldScreenX
	oldScreenX=newScreenX
	return deltaX
end

function exit()
	sClient:shutdown("both")
	print("Shutting down")
end

doExit=false

resetGame()
client.screenshot(SCREENSHOT_FILE)

connected=false
erMsg=""
while not connected do
	cId,erMsg = sClient:connect("127.0.0.1", PORT)
	connected = cId==1
	if connected then
		break
	end
	if erMsg=="already connected" then
		connected=true
		break
	end
	print("Couldnt connect to port "..PORT..": "..erMsg)
	emu.frameadvance()
end

print("Connected to JavaBridge")

resetGame()
while true do
	local action = -1
	while action == -1 do
		--emu.frameadvance()
		local line, errorMsg = sClient:receive()
		if line ~= nil then
			--console.writeline("Line: " .. line)
			if line ~= "" then
				action = tonumber(line)
				--print(action)
				if action == -42 then 
					doExit=true
					break
				end
			end
		else
			if(errorMsg=="closed") then
				doExit=true
				break
			end
		end
		
		--[[client.pause()
		emu.frameadvance()
		client.unpause()]]--
	end
	if doExit then break end
	controls={}
	--Interpret ActionInput TODO Cleanup Duplicates
	if action >= 32 then
		controls["P1 A"]=true;
		action = action - 32
	else 
		controls["P1 A"]=false;
	end
	if action >= 16 then
		controls["P1 B"]=true;
		action = action - 16
	else 
		controls["P1 B"]=false;
	end
	if action >= 8 then
		controls["P1 Up"]=true;
		action = action - 8
	else 
		controls["P1 Up"]=false;
	end
	if action >= 4 then
		controls["P1 Down"]=true;
		action = action - 4
	else 
		controls["P1 Down"]=false;
	end
	if action >= 2 then
		controls["P1 Left"]=true;
		action = action - 2
	else 
		controls["P1 Left"]=false;
	end
	if action >= 1 then
		controls["P1 Right"]=true;
		action = action - 1
	else 
		controls["P1 Right"]=false;
	end
	--console.writeline(controls)
	
	local reward=0
	local i=0
	while i<FRAME_BUNDLESIZE do
		joypad.set(controls)
		reward=reward+stepGame()
		i=i+1
	end
	client.screenshot(SCREENSHOT_FILE)
	sClient:send(reward .. "\n")
	
	--console.writeline("screenX: " .. screenX)
	--controls={}
	--controls["P1 Right"]=true;
	
end
print("JavaBridge closed")

event.onexit(function()
	exit()
end)
