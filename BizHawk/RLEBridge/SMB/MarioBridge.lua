local igen=require("marioinput")
socket=require('socket')
sClient=socket.tcp()
sClient:settimeout(1)--1 second timeout

ButtonNames = {
	"A",
	"B",
	"Up",
	"Down",
	"Left",
	"Right",
}
Buttons = #ButtonNames

PORT = 4242
SCREENSHOT_FILE = "E:/Schule/Maturarbeit/BizHawk/RLEBridge/ipc/screenshot.png"
FRAME_BUNDLESIZE=3

function resetGame()
	savestate.load("D:/Coding/workspace/RLE/BizHawk/RLEBridge/SMB/StartState.State")
	maxX=0
	lastMaxX=0
end

startX = 991 --Behind pipes:991 Start: 40
oldPlayerX = startX

finishX = 2980--550--100 --Flag is 3045

maxX=0 --Biggest xVal in this run
lastMaxX=0 --Steps since biggest xVal was reached

function stepGame()--Returns deltaX as Reward
	emu.frameadvance()
	--client.pause()
	local time=0
	time=time+memory.readbyte(0x07F8)*100
	time=time+memory.readbyte(0x07F9)*10
	time=time+memory.readbyte(0x07FA)*1
	if time < 2 then
		resetGame()
		oldPlayerX=startX
	end
	
	local alive = memory.readbyte(0x07CA)-- 0:Normal, 1:Fucked up
	
	if lastMaxX>160 then --Aged to death
		alive=0
	end
	--local lives = memory.readbyte(0x075A) + 1
	if alive~=0 then --He fucked up
		--console.writeline("He fucked up")
		resetGame()
		--client.pause()
		oldPlayerX = startX + 20 -- -20 Reward for dying
		
	elseif oldPlayerX >= finishX then --Final Flag
		resetGame()
		
		oldPlayerX = startX - 20 -- 20 Reward for finishing
	end
	sbX=memory.readbyte(0x071B)
	sX=memory.readbyte(0x071C)
	if sX==0 and sbX~=0 then
		sX=0x100 -- AIIIIIIIIIIIDS
	end
	local newScreenX = math.max(sbX-1, 0) * 0x100 + sX
	local newPlayerX = newScreenX + memory.readbyte(0x03AD)
	lastMaxX=lastMaxX+1
	if newPlayerX>maxX then
		maxX=newPlayerX
		lastMaxX=0
	end
	local deltaX=newPlayerX - oldPlayerX
	oldPlayerX=newPlayerX
	return deltaX
end

function exit()
	sClient:shutdown("both")
	print("Shutting down")
end

doExit=false

resetGame()
--client.screenshot(SCREENSHOT_FILE)

connected=false
erMsg=""
while not connected do
	cId,erMsg = sClient:connect("127.0.0.1", PORT)
	connected = cId==1
	if connected then
		break
	end
	if erMsg=="already connected" then
		connected=true
		break
	end
	print("Couldnt connect to port "..PORT..": "..erMsg)
	emu.frameadvance()
end

print("Connected to JavaBridge")

resetGame()
while true do
	local action = -1
	while action == -1 do
		--emu.frameadvance()
		local line, errorMsg = sClient:receive()
		if line ~= nil then
			--console.writeline("Line: " .. line)
			if line ~= "" then
				action = tonumber(line)
				--print(action)
				if action == -42 then 
					doExit=true
					break
				end
			end
		else
			if(errorMsg=="closed") then
				doExit=true
				break
			end
		end
		
		--[[client.pause()
		emu.frameadvance()
		client.unpause()]]--
	end
	if doExit then break end
	
	controls={}
	--Interpret ActionInput TODO Cleanup Duplicates
	if action >= 32 then
		controls["P1 A"]=true;
		action = action - 32
	else 
		controls["P1 A"]=false;
	end
	if action >= 16 then
		controls["P1 B"]=true;
		action = action - 16
	else 
		controls["P1 B"]=false;
	end
	if action >= 8 then
		controls["P1 Up"]=true;
		action = action - 8
	else 
		controls["P1 Up"]=false;
	end
	if action >= 4 then
		controls["P1 Down"]=true;
		action = action - 4
	else 
		controls["P1 Down"]=false;
	end
	if action >= 2 then
		controls["P1 Left"]=true;
		action = action - 2
	else 
		controls["P1 Left"]=false;
	end
	if action >= 1 then
		controls["P1 Right"]=true;
		action = action - 1
	else 
		controls["P1 Right"]=false;
	end
	--console.writeline(controls)
	local reward=0
	local i=0
	while i<FRAME_BUNDLESIZE do
		joypad.set(controls)
		reward=reward+stepGame()
		i=i+1
	end
	if reward>9 then
		reward=5
	elseif reward>0 then
		reward=5
	elseif reward==0 then
		reward=-2
	--[[else
		reward=-1
	end]]--
	elseif reward>-10 then
		reward=-5
	else
		reward=-10
	end
	
	local state = igen.getInputs()
	local sOut=""
	for i=1,#state do
		sOut=sOut..tostring(state[i])
	end
	sOut=sOut.."\n"
	sOut=sOut .. tostring(reward .. "\n")
	sClient:send(sOut)
	--console.writeline("screenX: " .. screenX)
	--controls={}
	--controls["P1 Right"]=true;
	
end
print("JavaBridge closed")

event.onexit(function()
	exit()
end)
