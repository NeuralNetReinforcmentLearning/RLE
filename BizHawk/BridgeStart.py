from pywinauto.application import Application
import time
import os
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
app = Application().start(os.path.join(__location__, "EmuHawk.exe"))
time.sleep(5)
emu = app.BizHawk
emu.DrawOutline()
emu.TypeKeys('%f')
emu.TypeKeys('{DOWN}{RIGHT}~')
time.sleep(2)
emu.TypeKeys('%t')
emu.TypeKeys('{UP}{UP}{UP}{UP}{UP}~')
time.sleep(1)
lua = app.LuaConsole
lua.TypeKeys('%f')
lua.TypeKeys('{UP}{UP}{RIGHT}~')
emu.SetFocus()

