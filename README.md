# About
This is the Repository of Alexander Uhlmanns Reinforcement Learning Algorithm.
# Examples
Examples are located under Repository -> Examples 

A video of the algorithm performing in various environments can be found here:

[![Youtube Demo](https://img.youtube.com/vi/scLTbin8FiQ/0.jpg)](https://www.youtube.com/watch?v=scLTbin8FiQ)

# Download
## Document
To download the main document containig a detailed explanation of my work please
use [this link](https://gitlab.com/NeuralNetReinforcmentLearning/RLE/blob/e27debfafd7aa46e60481bb4efa84ca06cf892d1/Documents/mask.pdf)
## Project
To download all project files please use the "Download zip" Button or [this link](https://gitlab.com/NeuralNetReinforcmentLearning/RLE/repository/archive.zip?ref=master)
